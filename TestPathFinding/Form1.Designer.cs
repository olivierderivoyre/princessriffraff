﻿namespace Test
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dijkstraButton = new System.Windows.Forms.Button();
            this.mapPanel = new System.Windows.Forms.Panel();
            this.InitButton = new System.Windows.Forms.Button();
            this.getDirectionButton = new System.Windows.Forms.Button();
            this.roomButton = new System.Windows.Forms.Button();
            this.spiralButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // dijkstraButton
            // 
            this.dijkstraButton.Location = new System.Drawing.Point(102, 12);
            this.dijkstraButton.Name = "dijkstraButton";
            this.dijkstraButton.Size = new System.Drawing.Size(75, 23);
            this.dijkstraButton.TabIndex = 0;
            this.dijkstraButton.Text = "Dijktsra";
            this.dijkstraButton.UseVisualStyleBackColor = true;
            this.dijkstraButton.Click += new System.EventHandler(this.dijkstraButton_Click);
            // 
            // mapPanel
            // 
            this.mapPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.mapPanel.AutoScroll = true;
            this.mapPanel.Location = new System.Drawing.Point(13, 45);
            this.mapPanel.Name = "mapPanel";
            this.mapPanel.Size = new System.Drawing.Size(852, 592);
            this.mapPanel.TabIndex = 1;
            // 
            // InitButton
            // 
            this.InitButton.Location = new System.Drawing.Point(13, 12);
            this.InitButton.Name = "InitButton";
            this.InitButton.Size = new System.Drawing.Size(75, 23);
            this.InitButton.TabIndex = 2;
            this.InitButton.Text = "Init";
            this.InitButton.UseVisualStyleBackColor = true;
            this.InitButton.Click += new System.EventHandler(this.InitButton_Click);
            // 
            // getDirectionButton
            // 
            this.getDirectionButton.Location = new System.Drawing.Point(183, 12);
            this.getDirectionButton.Name = "getDirectionButton";
            this.getDirectionButton.Size = new System.Drawing.Size(99, 23);
            this.getDirectionButton.TabIndex = 0;
            this.getDirectionButton.Text = "Get Direction";
            this.getDirectionButton.UseVisualStyleBackColor = true;
            this.getDirectionButton.Click += new System.EventHandler(this.getDirectionButton_Click);
            // 
            // roomButton
            // 
            this.roomButton.Location = new System.Drawing.Point(288, 12);
            this.roomButton.Name = "roomButton";
            this.roomButton.Size = new System.Drawing.Size(94, 23);
            this.roomButton.TabIndex = 3;
            this.roomButton.Text = "Room";
            this.roomButton.UseVisualStyleBackColor = true;
            // 
            // spiralButton
            // 
            this.spiralButton.Location = new System.Drawing.Point(388, 12);
            this.spiralButton.Name = "spiralButton";
            this.spiralButton.Size = new System.Drawing.Size(94, 23);
            this.spiralButton.TabIndex = 3;
            this.spiralButton.Text = "Spiral";
            this.spiralButton.UseVisualStyleBackColor = true;
            this.spiralButton.Click += new System.EventHandler(this.spiralButton_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(877, 649);
            this.Controls.Add(this.spiralButton);
            this.Controls.Add(this.roomButton);
            this.Controls.Add(this.InitButton);
            this.Controls.Add(this.mapPanel);
            this.Controls.Add(this.getDirectionButton);
            this.Controls.Add(this.dijkstraButton);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button dijkstraButton;
        private System.Windows.Forms.Panel mapPanel;
		private System.Windows.Forms.Button InitButton;
		private System.Windows.Forms.Button getDirectionButton;
		private System.Windows.Forms.Button roomButton;
		private System.Windows.Forms.Button spiralButton;
    }
}

