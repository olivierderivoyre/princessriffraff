﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using PrincessRiffRaff;
using System.Diagnostics;

namespace Test
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        private const int size = 50;
        private bool[] map;
        private Button[] mapButtons;
        private IntVector2 start = new IntVector2(size / 2, 5);
        private HashSet<IntVector2> endList = new HashSet<IntVector2>(new[] { new IntVector2(2, size / 2), new IntVector2(size / 2, size / 2) });

        private void setColor(IntVector2 coord, Color c)
        {
            this.mapButtons[coord.Y * size + coord.X].BackColor = c;
        }
        private void InitButton_Click(object sender, EventArgs e)
        {
            this.mapPanel.SuspendLayout();
            this.map = new bool[size * size];
            this.mapButtons = new Button[size * size];
            this.mapPanel.Controls.Clear();
            for (int y = 0; y < size; y++)               
            { 
               for (int x = 0; x < size; x++)
               {
                    int i = y * size + x;
                    Button b = new Button();
                    this.mapButtons[i] = b;
                    b.Click += delegate
                    {
                        this.map[i] ^= true;
                        b.Text = this.map[i] ? "X" : " "; 
                    };
                    b.Text = " ";
                    b.Top = y * 20;
                    b.Left = x * 20;
                    b.Width = 20;
                    this.mapPanel.Controls.Add(b);
                }
            }
            setColor(this.start, Color.Green);
            foreach (IntVector2 end in this.endList)
            {
                setColor(end, Color.Red);
            }
            this.mapPanel.ResumeLayout();
            
        }
        private void dijkstraButton_Click(object sender, EventArgs e)
        {
            foreach (var button in this.mapButtons)
            {
                button.BackColor = Control.DefaultBackColor;
            }
            Stopwatch stopWatch = new Stopwatch();
            stopWatch.Start();
            List<IntVector2> path = null;
			//for (int i = 0; i < 100; i++)
			{
				path = Dijkstra.FindPath(this.map, size, this.start, this.endList);
			}
            stopWatch.Stop();
            Trace.WriteLine("Duration: " + stopWatch.ElapsedMilliseconds);
            if (path != null)
            {
                foreach (var dot in path)
                {
                    setColor(dot, Color.Blue);
                }
            }
            setColor(this.start, Color.Green);
            foreach (IntVector2 end in this.endList)
            {
                setColor(end, Color.Red);
            }
        }
        private void getDirectionButton_Click(object sender, EventArgs e)
        {
            foreach (var button in this.mapButtons)
            {
                button.BackColor = Control.DefaultBackColor;
            }
            Stopwatch stopWatch = new Stopwatch();
            stopWatch.Start();
            List<IntVector2> path = null;
            //for (int i = 0; i < 100; i++)
            {
                path = Dijkstra.GetDirectionOf(this.map, size, this.start, this.endList.First());
            }
            stopWatch.Stop();
            Trace.WriteLine("Duration: " + stopWatch.ElapsedMilliseconds);
            if (path != null)
            {
                foreach (var dot in path)
                {
                    setColor(dot, Color.Blue);
                }
            }
            setColor(this.start, Color.Green);
            setColor(this.endList.First(), Color.Red);
            
        }
		
		
		private IEnumerator<IntVector2> spiral;
		private void spiralButton_Click(object sender, EventArgs e)
		{
			if (spiral == null)
			{
				spiral = Tools.GetSpiralInterator(4).GetEnumerator();
			}
			if (spiral.MoveNext())
			{
				IntVector2 p = this.start + spiral.Current;
				int i = p.Y * size + p.X;
				Button button = this.mapButtons[i];
				button.BackColor = Color.Orange;
			}
			else
			{
				spiral = null;
			}
		}

       

		
        
    }
}
