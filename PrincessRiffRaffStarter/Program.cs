﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using Microsoft.Win32;
using System.Diagnostics;
using System.IO;
using log4net;

namespace DigCraftFightStarter
{
	static class Program
	{
		private static ILog logger = LogManager.GetLogger(typeof(Program));
		
		
		/// <summary>
		/// The exe does not start on some PC, look that XNA is installed before starting the game in order to have a nice MsgBox.
		/// </summary>
		[STAThread]
		static void Main()
		{
			try
			{
				log4net.Config.XmlConfigurator.Configure();
				logger.Info("");
				logger.Info("Start PrincessRiffRaffStarter");
				logger.Info("");
				if (!isXnaInstalled())
				{
					logger.Info("XNA not found");
					DialogResult r = MessageBox.Show("XNA 4.0 is not installed.\nIn order to work this program need that you install the \"Microsoft XNA Framework Redistributable 4.0\"." +
						" Do you want to download and install it now?", "XNA 4.0 is not installed.", MessageBoxButtons.YesNo);
					if (r == DialogResult.Yes)
					{
						logger.Info("Open browser");
						Process.Start("http://go.microsoft.com/fwlink/?LinkID=148786");
					}
					logger.Info("End Editor");
					return;
				}
                FileInfo file = new FileInfo("./PrincessRiffRaff.exe");
                if (!file.Exists)
				{
                    throw new Exception("File PrincessRiffRaff.exe not found");
				}

                Process.Start(file.FullName);

				logger.Info("");
				logger.Info("End Editor");
			}
			catch (Exception ex)
			{
				logger.Error(ex);
				System.Windows.Forms.MessageBox.Show(ex.ToString(), "Error");
			}
		}
		private static bool isXnaInstalled()
		{
			RegistryKey xnaRegistry = Registry.LocalMachine.OpenSubKey(@"Software\Microsoft\XNA\Framework\v4.0");
			if (xnaRegistry == null)
			{
				xnaRegistry = Registry.LocalMachine.OpenSubKey(@"Software\Wow6432Node\Microsoft\XNA\Framework\v4.0");
			}
			if (xnaRegistry == null)
			{
				return false;
			}
			if(!object.Equals(1, xnaRegistry.GetValue("Installed")))
			{
				return false;
			}
			return true;
		}
	}
}
