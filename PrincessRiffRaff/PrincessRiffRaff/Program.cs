using System;
using System.Diagnostics;
using System.IO;
using log4net;

namespace PrincessRiffRaff
{
#if WINDOWS || XBOX
    static class Program
    {
		private static ILog logger = LogManager.GetLogger(typeof(Program));
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main(string[] args)
        {			
			try
			{
				log4net.Config.XmlConfigurator.Configure();
				logger.Info("");
				logger.Info("Start PrincessRiffRaff");
				logger.Info("");

                if (args.Length >= 1 && args[0] == "-test")
                {
                    PrincessRiffRaffGame.DebugIncludeTestMap = true;
                }
				if (args.Length >= 2 && args[0]=="-level")
				{
                    PrincessRiffRaffGame.DebugLevelFileName = args[1];
                    if (!File.Exists(PrincessRiffRaffGame.DebugLevelFileName))
                    {
                        throw new FileNotFoundException(PrincessRiffRaffGame.DebugLevelFileName);
                    }
				}
				using (PrincessRiffRaffGame game = new PrincessRiffRaffGame())
                {
                    if (args.Length >= 2 && args[0] == "-video")
                    {
                        game.SetWindowSize(1280, 720);
                    }
					game.Run();
				}
				
				logger.Info("End program");
				logger.Info("");
			}
			catch (Exception ex)
			{
				logger.Error(ex);
				System.Windows.Forms.MessageBox.Show(ex.ToString(), "Error");
			}
        }
    }
#endif
}

