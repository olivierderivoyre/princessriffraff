﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System.Diagnostics;
using System.IO;
using System.Xml.Serialization;

namespace PrincessRiffRaff
{
    public class Level
    {
        public readonly PrincessRiffRaffGame Game;
        private Screen screen;
        public Player player1;
        public Player player2;
        public int CurrentBuffHeart = 0;
        public Land Land;
        public IntVector2 PlayerResurectPosition;
        public int Tick = 0;
        public BossZone BossZone;
        public readonly Music Music;
        public long Score = 0;
        public readonly MobScoreValue MobScoreValue;
        public int LevelIndex = -1;
        
        private HashSet<Bullet> bullets = new HashSet<Bullet>();
        private HashSet<Animation> animations = new HashSet<Animation>();
        private HashSet<Mob> mobs = new HashSet<Mob>();
        private HashSet<ZoneTrigger> zoneTriggers = new HashSet<ZoneTrigger>();
        private List<IDrawable> groundDrawables = new List<IDrawable>();
        private List<IUpdatable> updatables = new List<IUpdatable>();
        private Dictionary<IntVector2, Bomb> bombs = new Dictionary<IntVector2, Bomb>();

        public Level(PrincessRiffRaffGame game, Screen screen, Player player1, Player player2, ComLevel comLevel)
        {
            this.Game = game;
            this.screen = screen;
            this.Music = game.Music;
            this.player1 = player1;
            this.player2 = player2;
            this.player1.Level = this;
            this.player2.Level = this;
            this.Land = new Land(comLevel);
            this.MobScoreValue = new MobScoreValue(this);

            this.load(comLevel);

        }

        private void load(ComLevel comLevel)
        {
            int Width = comLevel.Carpet.Length;
            int Height = comLevel.Carpet[0].Length;
            for (int j = 0; j < Height; j++)
            {
                for (int i = 0; i < Width; i++)
                {
                    ItemType? furniture = comLevel.Furniture[i][j];
                    if (furniture != null)
                    {
                        this.tryLoad(comLevel, new IntVector2(i, j) * Tools.CellSizeInCoord, furniture.Value);
                    }
                }
            }
            foreach (Player player in new[] { this.player1, this.player2 })
            {
                player.Weapons[0] = WeaponTypeHelper.NewWeapon(player, comLevel.LevelAttributes.WeaponGun, WeaponType.BasicGun);
                player.Weapons[1] = WeaponTypeHelper.NewWeapon(player, comLevel.LevelAttributes.WeaponBomb, WeaponType.AoeBomb);
                player.Weapons[2] = WeaponTypeHelper.NewWeapon(player, comLevel.LevelAttributes.WeaponBazooka, WeaponType.Bazooka);
                player.Weapons[3] = WeaponTypeHelper.NewWeapon(player, comLevel.LevelAttributes.WeaponSpecial, WeaponType.Potion);
            }

        }

        private void tryLoad(ComLevel comLevel, IntVector2 position, ItemType itemType)
        {

            if (itemType == ItemType.ZONE_START)
            {
                this.PlayerResurectPosition = position;
            }
            if (itemType == ItemType.ZONE_BOSS)
            {
                this.BossZone = new BossZone();
                this.BossZone.Level = this;
                this.BossZone.Position = position;
                this.BossZone.BossType = comLevel.LevelAttributes.BossType;
            }
            if (itemType == ItemType.ZONE_SAVE)
            {
                var zone = new ZoneSave();
                zone.Level = this;
                zone.Position = position;
                this.Add(zone);
            }
            if (itemType == ItemType.ZONE_NEW_HEARTH)
            {
                var zone = new HeartDrop();
                zone.Level = this;
                zone.Position = position;
                this.Add(zone);
            }
            if (itemType == ItemType.ZONE_AMMO)
            {
                var zone = new AmmoProvider();
                zone.Level = this;
                zone.Position = position;
                this.Add(zone);
            }
            if (itemType > ItemType.DELIMITER_MOB && itemType < ItemType.DELIMITER_MOB_END)
            {
                Mob mob = MobTypeHelper.NewMob(this, itemType);
                if (mob != null)
                {
                    mob.Position = position;
                    this.Add(mob);
                }
            }
        }

        public void Add(Bullet bullet)
        {
            this.bullets.Add(bullet);
        }

        public void Add(Mob mob)
        {
            this.mobs.Add(mob);
        }
        public void Remove(Mob mob)
        {
            this.mobs.Remove(mob);
        }
        public void Add(IDrawable item)
        {
            this.groundDrawables.Add(item);
        }
        public void Remove(IDrawable item)
        {
            this.groundDrawables.Remove(item);
        }
        public void Add(IUpdatable item)
        {
            this.updatables.Add(item);
        }
        public void Add(ZoneTrigger item)
        {
            this.zoneTriggers.Add(item);
        }
        public void Remove(ZoneTrigger item)
        {
            this.zoneTriggers.Remove(item);
        }
        public void Add(Animation item)
        {
            this.animations.Add(item);
        }
        public bool CanAddBombHere(IntVector2 position)
        {
            if (position != Tools.FloorToCell(position))
            {
                throw new Exception();
            }
            return !this.bombs.ContainsKey(position);
        }
        public void Add(Bomb item)
        {
            if (item.Position != Tools.FloorToCell(item.Position))
            {
                throw new Exception();
            }
            this.bombs.Add(item.Position, item);
        }
        public void Remove(Bomb item)
        {
            this.bombs.Remove(item.Position);
        }


        public void Draw(Screen screen, GameTime gameTime)
        {
            this.Land.Draw(screen);



            foreach (IDrawable drawable in this.groundDrawables)
            {
                drawable.Draw(screen);
            }
            foreach (ZoneTrigger drop in this.zoneTriggers)
            {
                drop.Draw(screen);
            }
            foreach (Bomb bomb in this.bombs.Values)
            {
                bomb.Draw(screen);
            }
            if (player1.Position.Y < player2.Position.Y)
            {
                this.player1.Draw(this.screen);
                this.player2.Draw(this.screen);
            }
            else
            {
                this.player2.Draw(this.screen);
                this.player1.Draw(this.screen);
            }
            foreach (Mob item in this.mobs)
            {
                item.Draw(screen);
            }
            foreach (Bullet item in this.bullets)
            {
                item.Draw(screen);
            }
            foreach (Animation item in this.animations)
            {
                item.Draw(screen);
            }
        }

        public IntVector2 LimitPlayerMove(IntVector2 currentPosition, IntVector2 direction)
        {
            IntVector2 groundConstrainedDirection = this.Land.LimitCharacterMove(currentPosition, direction);
            if (groundConstrainedDirection == IntVector2.Zero)
            {
                groundConstrainedDirection = this.Land.HelpPlayerMove(currentPosition, direction);
            }
            if (groundConstrainedDirection == IntVector2.Zero)
            {
                return IntVector2.Zero;
            }
            IntVector2 newPosition = currentPosition + Tools.QuaterCellSizeInCoord * groundConstrainedDirection;
            IntVector2 screenConstrainedDirection = this.screen.LimitPlayerMove(newPosition, groundConstrainedDirection);
            return screenConstrainedDirection;
        }


        public void Update()
        {
            this.Tick++;

            //if (this.BossZone != null && this.BossZone.Boss != null)
            //{
            //    if (this.BossZone.Boss.IsFinished)
            //    {
            //        if (this.BossZone.Boss.MobDyeAnimation.IsFinished)
            //        {
            //            this.Game.LevelWin();
            //            this.Game.YouHaveWinMenu = new YouHaveWinMenu(this);
            //            return;
            //        }

            //    }
            //}


            foreach (Bullet bullet in this.bullets)
            {
                bullet.Update();
            }
            this.bullets.RemoveWhere(b => b.IsFisnish());
            foreach (Bomb bomb in this.bombs.Values.ToArray())
            {
                bomb.Update();
            }
            foreach (Animation item in this.animations.ToArray())
            {
                item.Update();
            }
            this.animations.RemoveWhere(b => b.IsFinished);
            foreach (Mob mob in this.mobs.ToList())
            {
                mob.Update();
            }
            this.mobs.RemoveWhere(b => b.IsFinished);
            if (this.BossZone != null)
            {
                this.BossZone.Update();
            }
            foreach (IUpdatable item in this.updatables)
            {
                item.Update();
            }

            if (!this.player1.IsActive && !this.player2.IsActive)
            {
                if (this.Game.YouAreDeadMenu == null)
                {
                    this.Game.YouAreDeadMenu = new YouAreDeadMenu(this);
                }
            }


        }


        public void CleanBullets()
        {
            foreach (Bullet item in this.bullets)
            {
                item.ForceFinish();
            }
            foreach (Bomb item in this.bombs.Values)
            {
                item.IsFinished = true;
            }
            this.bullets.Clear();
            this.bombs.Clear();
        }

        public void ResurectPlayers()
        {
            this.CleanBullets();
            this.player1.ResurectOnLastSave(this.PlayerResurectPosition);
            this.player2.ResurectOnLastSave(this.PlayerResurectPosition);
            this.screen.CenterOn(this.PlayerResurectPosition);

            if (this.BossZone != null)
            {
                this.BossZone.ResetFight();
            }
        }

        public bool IsTwoPlayers()
        {
            return !this.player2.NeedToPressStartToPlay;
        }

        public IEnumerable<Mob> GetVisibleMobsHitBy(IntVector2 targetPosition, int hitRange)
        {
            foreach (Mob unit in this.mobs)
            {
                if (targetPosition.HitRectangle(unit.Position, Tools.CellSizeInCoord, Tools.CellSizeInCoord, hitRange))
                {
                    yield return unit;
                }
            }
        }

        public IEnumerable<Character> GetFriendlyUnitsHitBy(IntVector2 targetPosition, int hitRange)
        {
            foreach (Character unit in this.GetActivePlayers())
            {
                IntVector2 centerPlayer = unit.Position + new IntVector2(1, 1) * 2 * Tools.QuaterCellSizeInCoord;
                int distance = IntVector2.GetDistance(centerPlayer, targetPosition);
                if (distance < Math.Max(hitRange, 5) + 2 * Tools.QuaterCellSizeInCoord)
                {
                    yield return unit;
                }               
            }
        }


        public Mob GetShootableMobAround(IntVector2 position, int range)
        {
            Mob mob = GetNearestMobInsideScreen(position);
            if (mob == null)
            {
                return null;
            }
            if (HasClearShoot(position, mob.Position, range))
            {
                return mob;
            }
            return null;
        }

        public IEnumerable<Mob> GetMobsInside(IntVector2 topLeft, int squareSize)
        {
            foreach (Mob mob in this.mobs)
            {
                if (mob.IsDead)
                {
                    continue;
                }
                if (mob.Position.IsInside(topLeft, squareSize, squareSize))
                {
                    yield return mob;
                }
            }
        }
        public IEnumerable<Mob> GetMobAroundCell(IntVector2 center, int radius)
        {
            IntVector2 topLeft = center - new IntVector2(radius, radius) * Tools.CellSizeInCoord;
            return this.GetMobsInside(topLeft, (radius +  radius) * Tools.CellSizeInCoord + 5);
        }
        internal Mob GetNearestMobInside(IntVector2 topLeft, int squareSize, IntVector2 center)
        {
            int nearestDistance = int.MaxValue;
            Mob nearestMob = null;
            foreach (Mob mob in this.GetMobsInside(topLeft, squareSize))
            {
                int distante = IntVector2.Delta(center, mob.Position).GetDistance();
                if (distante < nearestDistance)
                {
                    nearestDistance = distante;
                    nearestMob = mob;
                }
            }
            return nearestMob;
        }
        public Mob GetNearestMobInsideScreen(IntVector2 position)
        {
            int nearestDistance = int.MaxValue;
            int nearestMortalDistance = int.MaxValue;
            Mob nearestMob = null;
            Mob nearestMortalMob = null;
            IntVector2 topLeft = this.screen.TopLeft - new IntVector2(1, 1) * Tools.CellSizeInCoord;
            int width = this.screen.WidthCoord + 2 * Tools.CellSizeInCoord;
            int height = this.screen.HeightCoord + 2 * Tools.CellSizeInCoord;
            foreach (Mob mob in this.mobs)
            {
                if (mob.IsDead)
                {
                    continue;
                }
                if (!mob.Position.IsInside(topLeft, width, height))
                {
                    continue;
                }
                int distante = IntVector2.Delta(position, mob.Position).GetDistance();
                if (distante < nearestDistance)
                {
                    nearestDistance = distante;
                    nearestMob = mob;
                }

                if (distante < nearestMortalDistance)
                {
                    nearestMortalDistance = distante;
                    nearestMortalMob = mob;
                }

            }
            if (nearestMortalMob != null)
            {
                return nearestMortalMob;
            }
            return nearestMob;
        }

        public IEnumerable<Mob> GetMobsInsideScreen()
        {           
             foreach (Mob mob in this.mobs)
            {
                if (mob.IsDead)
                {
                    continue;
                }
                if (!this.screen.IsInside(mob.Position))
                {
                    continue;
                }
                yield return mob;

            }           
        }

        private IEnumerable<Character> GetActivePlayers()
        {
            return new[] { this.player1, this.player2 }.Where(p => p.IsActive);
        }


        public Character GetNearestPlayer(IntVector2 position, int rangeInCoord)
        {
            int nearestDistance = int.MaxValue;
            Character nearestPlayer = null;
            IntVector2 topLeft = position - new IntVector2(rangeInCoord, rangeInCoord);
            foreach (Character character in this.GetActivePlayers())
            {
                int distance = IntVector2.Delta(position, character.Position).GetDistance();
                if (distance > rangeInCoord)
                {
                    continue;
                }
                if (distance < nearestDistance)
                {
                    nearestDistance = distance;
                    nearestPlayer = character;
                }
            }
            return nearestPlayer;
        }
        public Player GetNearestAlivePlayer(IntVector2 position)
        {
            int nearestDistance = int.MaxValue;
            Player nearestPlayer = null;
            foreach (Player character in this.GetActivePlayers())
            {
                int distance = IntVector2.Delta(position, character.Position).GetDistance();
                if (distance < nearestDistance)
                {
                    nearestDistance = distance;
                    nearestPlayer = character;
                }
            }
            return nearestPlayer;
        }
        public Player GetAlivePlayerTopLeftInside(IntVector2 topLeft, int width, int height)
        {
            if (this.player1.IsActive)
            {
                if (this.player1.Position.IsInside(topLeft, width, height))
                {
                    return this.player1;
                }
            }
            if (this.player2.IsActive)
            {
                if (this.player2.Position.IsInside(topLeft, width, height))
                {
                    return this.player2;
                }
            }
            return null;
        }

        //public Player GetAlivePlayerAtPosition(IntVector2 targetPosition)
        //{
        //    if (this.player1.IsActive)
        //    {
        //        if (targetPosition.IsInside(this.player1.Position, Tools.CellSizeInCoord, Tools.CellSizeInCoord))
        //        {
        //            return this.player1;
        //        }
        //    }
        //    if (this.player2.IsActive)
        //    {
        //        if (targetPosition.IsInside(this.player2.Position, Tools.CellSizeInCoord, Tools.CellSizeInCoord))
        //        {
        //            return this.player2;
        //        }
        //    }
        //    return null;
        //}

        public bool HasPlayerInside(IntVector2 topLeft, int width, int height)
        {
            foreach (Player p in this.GetActivePlayers())
            {
                if (Tools.CenterCharacter(p.Position).IsInside(topLeft, width, height))
                {
                    return true;
                }
            }
            return false;
        }

        public bool HasAliveFriendlyUnitOver(IntVector2 topLeft)
        {
            return HasPlayerInside(
                Tools.CenterCharacter(topLeft) - new IntVector2(1, 1) * 6 * Tools.QuaterCellSizeInCoord,
                13 * Tools.QuaterCellSizeInCoord, 13 * Tools.QuaterCellSizeInCoord);
        }

        public bool HasClearShoot(IntVector2 topLeftFrom, IntVector2 topLeftTo, int? range)
        {
            IntVector2 delta = IntVector2.Delta(topLeftFrom, topLeftTo);
            int distance = delta.GetDistance();
            if (range != null && distance > range)
            {
                return false;
            }
            int cellDistance = distance / Tools.QuaterCellSizeInCoord;
            for (int i = 0; i < cellDistance; i++)
            {
                IntVector2 bulletPosition = topLeftFrom + delta * i / cellDistance;
                if (this.Land.GetFurniture(bulletPosition) != null)
                {
                    return false;
                }
            }
            return true;
        }

        public IEnumerable<Bomb> GetBombInside(IntVector2 topLeft, int squareSize)
        {
            foreach (Bomb bomb in this.bombs.Values)
            {
                if (bomb.Position.IsInside(topLeft, squareSize, squareSize))
                {
                    yield return bomb;
                }
            }
        }
        public IEnumerable<Player> GetAlivePlayers()
        {
            if (!this.player1.IsDead && this.player1.IsActive)
            {
                yield return this.player1;
            }
            if (!this.player2.IsDead && this.player2.IsActive)
            {
                yield return this.player2;
            }
        }


        private IEnumerable<IntVector2> getPopablePositions()
        {
            for (int x = -Tools.CellSizeInCoord * 3 / 2; x < screen.WidthCoord + Tools.CellSizeInCoord * 3 / 2; x += Tools.CellSizeInCoord)
            {
                yield return screen.TopLeft + new IntVector2(x, -Tools.CellSizeInCoord * 3 / 2);
                yield return screen.TopLeft + new IntVector2(x, screen.HeightCoord + Tools.CellSizeInCoord * 3 / 2);
            }
            for (int y = -Tools.CellSizeInCoord * 3 / 2; y < screen.HeightCoord + Tools.CellSizeInCoord * 3 / 2; y += Tools.CellSizeInCoord)
            {
                yield return screen.TopLeft + new IntVector2(-Tools.CellSizeInCoord * 3 / 2, y);
                yield return screen.TopLeft + new IntVector2(screen.WidthCoord + Tools.CellSizeInCoord * 3 / 2, y);
            }
        }





        public void TriggerPlayerMoved(Player player)
        {
            foreach (ZoneTrigger drop in this.zoneTriggers.ToList())
            {
                if (IntVector2.Delta(player.Position, drop.Position).GetDistance() <= 3 * Tools.QuaterCellSizeInCoord)
                {
                    drop.OnTaken(player);

                }
            }
            this.Aggro(player.Position, 5, player, player);
        }


        public void Aggro(IntVector2 position, int rangeCell, Player player, Character source)
        {
            int range = rangeCell * Tools.CellSizeInCoord;
            IntVector2 topLeft = position - new IntVector2(range, range);
            foreach (Mob mob in this.GetMobsInside(topLeft, range * 2 + Tools.CellSizeInCoord))
            {
                if (mob.IsSpleeping && !mob.WakeUp)
                {
                    if (this.HasClearShoot(mob.Position, source.Position, null))
                    {
                        mob.WakeUp = true;
                        mob.TargetPlayer = player;
                    }
                }
            }
        }



        public bool IsIsInsideScreen(IntVector2 position)
        {
            return this.screen.IsInside(position);
        }


    }
}
