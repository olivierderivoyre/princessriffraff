﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using System.Diagnostics;

namespace PrincessRiffRaff
{
	public class Land
	{
		
        private struct CellItems
		{
			public ItemType? Carpet;
			public ItemType? Furniture;
		}

		public readonly int Width;
		public readonly int Height;
        private CellItems[] cells;
		private bool[] hasBlock;
		


		public Land(ComLevel level)
		{			
			this.Width = level.Carpet.Length;
            this.Height = level.Carpet[0].Length;
            this.cells = new CellItems[this.Width * this.Height];
			this.hasBlock = new bool[this.Width * this.Height];
			
            for (int j = 0; j < this.Height; j++)
			{
                for (int i = 0; i < this.Width; i++)
				{
                    ItemType? furniture = level.Furniture[i][j];
                    if (furniture != null)
                    {
                        if (furniture.Value <= ItemType.DELIMITER_FURNITURE || furniture.Value >= ItemType.DELIMITER_FURNITURE_END)
                        {
                            furniture = null;
                        }
                    }
                    var cellItems = new CellItems{
                        Carpet = level.Carpet[i][j],
                        Furniture = furniture
                    };	
                    this.cells[j * this.Width + i] = cellItems;
				}
			}
            this.refreshIsSolid();
		}

		private void refreshIsSolid()
		{			
			for (int j = 0; j < this.Height; j++)
			{
				for (int i = 0; i < this.Width; i++)
				{
					IntVector2 cell = new IntVector2(i, j);
					ItemType? carpet = this.GetCarpet(cell * Tools.CellSizeInCoord);
					if (carpet == null || carpet == ItemType.GROUND_WATER)
					{
						this.hasBlock[j * this.Width + i] = true;
					}
					else
					{
                        ItemType? furnitureItem = this.GetFurniture(cell * Tools.CellSizeInCoord);
						if (furnitureItem == null)
						{
							this.hasBlock[j * this.Width + i] = false;
						}
						else
						{
							this.hasBlock[j * this.Width + i] = true;
						}
					}
				}
			}
		}




        public bool HasBlock(int positionX, int positionY)
        {
            return this.HasBlock(new IntVector2(positionX, positionY));
        }
		/// <summary>
		/// Ground or water
		/// </summary>
        public bool HasBlock(IntVector2 position)
        {
            IntVector2 localCell = this.getLocalCell(position);
            if(localCell.X < 0 || localCell.X >= this.Width)
            {
                return true;
            }
            if(localCell.Y < 0 || localCell.Y >= this.Height)
            {
                return true;
            }
			return this.hasBlock[localCell.Y * this.Width + localCell.X];
		}

        public ItemType? GetCarpet(IntVector2 position)
        {
            IntVector2 localCell = this.getLocalCell(position);
            if(localCell.X < 0 || localCell.X >= this.Width)
            {
                return null;
            }
            if(localCell.Y < 0 || localCell.Y >= this.Height)
            {
                return null;
            }
			return this.cells[localCell.Y * this.Width + localCell.X].Carpet;
		}
        public bool HasBulletBlock(IntVector2 position, BulletAttributes bulletAttributes)
        {
            if (bulletAttributes.GoOverFurniture)
            {
                return false;
            }
            return this.GetFurniture(position) != null;
        }
		public ItemType? GetFurniture(IntVector2 position)
		{
            IntVector2 localCell = this.getLocalCell(position);
			if(localCell.X < 0 || localCell.X >= this.Width)
            {
                return null;
            }
            if(localCell.Y < 0 || localCell.Y >= this.Height)
            {
                return null;
            }
			return this.cells[localCell.Y * this.Width + localCell.X].Furniture;
		}
       
        public IntVector2 getLocalCell(IntVector2 position)
        {
            return position / Tools.CellSizeInCoord;
        }

        private void draw(Screen screen, IntVector2 targetPosition)
        {
            ItemType? carpetItem = this.GetCarpet(targetPosition);
            ItemType? furnitureItem = this.GetFurniture(targetPosition);

            if (carpetItem != null)
            {
                screen.Draw64(ItemTypeHelper.GetTextureRect(carpetItem.Value), targetPosition, Color.White);
            }
            if (furnitureItem != null)
            {
                screen.Draw64(ItemTypeHelper.GetTextureRect(furnitureItem.Value), targetPosition, Color.White);
            }
        }

        public void Draw(Screen screen)
        {
            IntVector2 topLeftCell = (screen.TopLeft / Tools.CellSizeInCoord) * Tools.CellSizeInCoord;

            int xMinCeilled = (topLeftCell.X / Tools.CellSizeInCoord) * Tools.CellSizeInCoord;
            int xMaxCeilled = (((topLeftCell.X + screen.WidthCoord) / Tools.CellSizeInCoord) + 2) * Tools.CellSizeInCoord;
            int yMinCeilled = (topLeftCell.Y / Tools.CellSizeInCoord) * Tools.CellSizeInCoord;
            int yMaxCeilled = (((topLeftCell.Y + screen.HeightCoord) / Tools.CellSizeInCoord) + 2) * Tools.CellSizeInCoord;


            for (int x = xMinCeilled; x < xMaxCeilled; x += Tools.CellSizeInCoord)
            {
                for (int y = yMinCeilled; y < yMaxCeilled; y += Tools.CellSizeInCoord)
                {
                    this.draw(screen, new IntVector2(x, y));
                }
            }
        }





        public IntVector2 LimitCharacterMove(IntVector2 currentPosition, IntVector2 direction)
        {
            IntVector2 constrainedDirection = direction;
            IntVector2 quaterCellPosition = currentPosition / Tools.QuaterCellSizeInCoord;
            if (quaterCellPosition.X % 4 != 0 && quaterCellPosition.Y % 4 != 0)
            {
                return constrainedDirection;///part of the player is already in the cell 
            }
            if (quaterCellPosition.Y % 4 == 0 && constrainedDirection.Y != 0)///Player want to go up or down
            {
                if (HasBlock(currentPosition.X, currentPosition.Y + constrainedDirection.Y * Tools.CellSizeInCoord))
                {
                    constrainedDirection.Y = 0;
                }
                if (quaterCellPosition.X % 4 != 0)
                {
                    if (HasBlock(currentPosition.X + Tools.CellSizeInCoord, currentPosition.Y + constrainedDirection.Y * Tools.CellSizeInCoord))
                    {
                        constrainedDirection.Y = 0;
                    }
                }
            }
            if (quaterCellPosition.X % 4 == 0 && constrainedDirection.X != 0)///Player want to go left or right
            {
                if (HasBlock(currentPosition.X + constrainedDirection.X * Tools.CellSizeInCoord, currentPosition.Y))
                {
                    constrainedDirection.X = 0;
                }
                if (quaterCellPosition.Y % 4 != 0)
                {
                    if (HasBlock(currentPosition.X + constrainedDirection.X * Tools.CellSizeInCoord, currentPosition.Y + Tools.CellSizeInCoord))
                    {
                        constrainedDirection.X = 0;
                    }
                }

            }
            ///Diagonal pariculiar case: can we go in X+1, Y+1?
            if (quaterCellPosition.X % 4 == 0 && quaterCellPosition.Y % 4 == 0)
            {
                if (constrainedDirection.X != 0 && constrainedDirection.Y != 0)
                {
                    if (HasBlock(
                        currentPosition.X + constrainedDirection.X * Tools.CellSizeInCoord,
                        currentPosition.Y + constrainedDirection.Y * Tools.CellSizeInCoord))
                    {
                        //constrainedDirection = IntVector2.Zero;
                        constrainedDirection.Y = 0;
                    }
                }
            }
            return constrainedDirection;
        }

        /// <summary>
        /// Round corner
        /// </summary>
        public IntVector2 HelpPlayerMove(IntVector2 currentPosition, IntVector2 inputDirection)
        {
            IntVector2 quaterCellPosition = currentPosition / Tools.QuaterCellSizeInCoord;
            if (inputDirection.X != 0 && inputDirection.Y == 0)///Player want to go right, but there is a block
            {
                if (quaterCellPosition.Y % 4 != 0)
                {
                    ///Try to go up
                    if (!HasBlock(currentPosition.X, currentPosition.Y)
                        && !HasBlock(currentPosition.X + Tools.CellSizeInCoord * inputDirection.X, currentPosition.Y))
                    {
                        return new IntVector2(0, -1);
                    }
                    ///Try go down
                    if (!HasBlock(currentPosition.X, currentPosition.Y + Tools.CellSizeInCoord)
                        && !HasBlock(currentPosition.X + Tools.CellSizeInCoord * inputDirection.X, currentPosition.Y + Tools.CellSizeInCoord))
                    {
                        return new IntVector2(0, +1);
                    }
                }
            }
            if (inputDirection.X == 0 && inputDirection.Y != 0)///Player want to go up, but there is a block
            {
                if (quaterCellPosition.X % 4 != 0)
                {
                    ///Try to go left
                    if (!HasBlock(currentPosition.X, currentPosition.Y)
                        && !HasBlock(currentPosition.X, currentPosition.Y + Tools.CellSizeInCoord * inputDirection.Y))
                    {
                        return new IntVector2(-1, 0);
                    }
                    ///Try go right
                    if (!HasBlock(currentPosition.X + Tools.CellSizeInCoord, currentPosition.Y)
                        && !HasBlock(currentPosition.X + Tools.CellSizeInCoord, currentPosition.Y + Tools.CellSizeInCoord * inputDirection.Y))
                    {
                        return new IntVector2(+1, 0);
                    }
                }
            }
            return IntVector2.Zero;
        }



	}
}
