﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PrincessRiffRaff
{
    public class MobScoreValue
    {
        private readonly Level level;
        private int lastTick;
        private long currentValue;

        public MobScoreValue(Level level)
        {
            this.level = level;
        }

        /// <summary>
        /// Increase mob score. First mob give 100, second give 200, third give 300, etc...
        /// </summary>
        public long GetMobScore(Mob mob)
        {
            if (mob.IsBoss)
            {
                return 5000;
            }
            int mobValue = (mob.MaxLife / 50 + 1) * 100;
            if (this.level.Tick - this.lastTick > 60)
            {
                this.currentValue = mobValue;
            }
            else
            {
                this.currentValue += mobValue;
            }
            this.lastTick = this.level.Tick;
            return this.currentValue;
        }

    }
}
