﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PrincessRiffRaff
{
    public class BossPhases
    {

        private readonly BossAI bossAI;
        private List<Func<BossPhases, bool>> triggers = new List<Func<BossPhases, bool>>();
        public int CurrentPhaseIndex = 0;
        private readonly int startTime;
        private int currentPhaseStartTime = 0;



        public BossPhases(BossAI bossAI)
        {
            this.bossAI = bossAI;
            this.startTime = this.bossAI.Level.Tick;
            this.currentPhaseStartTime = this.bossAI.Level.Tick;
        }

        public void AddPhase(Func<BossPhases, bool> condition)
        {
            this.triggers.Add(condition);
        }

        public bool TryChangePhase()
        {
            if (this.CurrentPhaseIndex < this.triggers.Count)
            {
                if (this.triggers[this.CurrentPhaseIndex](this))
                {
                    this.CurrentPhaseIndex++;
                    this.currentPhaseStartTime = this.bossAI.Level.Tick;
                    return true;
                }
            }
            return false;
        }


        public int CurrentPhaseDuration
        {
            get
            {
                return this.bossAI.Level.Tick - this.currentPhaseStartTime;
            }
        }
        public int BossDuration
        {
            get
            {
                return this.bossAI.Level.Tick - this.startTime;
            }
        }
        public int BossLifePercent
        {
            get
            {
                return this.bossAI.GetLifePercent();
            }
        }

    }
}
