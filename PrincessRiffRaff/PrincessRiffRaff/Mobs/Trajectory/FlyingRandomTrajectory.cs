﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using System.Collections;

namespace PrincessRiffRaff
{
    public class FlyingRandomTrajectory
    {
        private readonly Mob mob;
        private readonly IntVector2 center;
        private readonly int radius;

        private LinearFlyingMobTrajectory currentTrajectory;
        private IntVector2 previousCell;
       
        public FlyingRandomTrajectory(Mob mob, IntVector2 center, int radius)
        {
            this.mob = mob;
            this.center = center;
            this.radius = radius;
        }

        public IEnumerable QuarterCellMove(int speedInCellDuration)
        {
            if (this.currentTrajectory == null || this.currentTrajectory.IsFinish())
            {
               IntVector2[] nextPositionList = Tools.CardinalPoints
                    .Select(c => this.mob.Position + c * Tools.CellSizeInCoord)
                    .Where(c => c != this.previousCell)
                    .Where(c => IntVector2.GetDistance(this.center, c) < radius)
                    .ToArray();
                IntVector2 nextPosition = this.center;
                if (nextPositionList.Length != 0)
                {
                    nextPosition = Tools.RandPickOne(nextPositionList);
                }
                nextPosition = Tools.FloorToCell(nextPosition);
                this.currentTrajectory = new LinearFlyingMobTrajectory(this.mob, nextPosition);
                this.previousCell = this.mob.Position;
            }
            return this.currentTrajectory.QuarterCellMove(speedInCellDuration);
        }
    }
}
