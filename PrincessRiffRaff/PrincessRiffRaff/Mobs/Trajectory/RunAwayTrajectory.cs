﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

namespace PrincessRiffRaff
{
    public class RunAwayTrajectory : DijkstraMobTrajectory
    {

        private IntVector2 runAwayFromPosition;

        public RunAwayTrajectory(Mob source, IntVector2 runAwayFromPosition)
            : base(source)
        {
            this.runAwayFromPosition = runAwayFromPosition;
        }

        protected override Queue<IntVector2> GetPath()
        {
            IntVector2 topLeft = Tools.FloorToCell(this.mob.Position) - new IntVector2(1, 1) * lookAroundNbCell * Tools.CellSizeInCoord;
            bool[] map = createDijkstraMap(topLeft);
            IntVector2 start = new IntVector2(lookAroundNbCell, lookAroundNbCell);
            IntVector2 fleeFrom = (this.runAwayFromPosition - topLeft) / Tools.CellSizeInCoord;
            List<IntVector2> localPath = Dijkstra.GetRunAwayDirection(map, DijkstraMapSize, start, fleeFrom);
            if (localPath == null || localPath.Count == 0)
            {
                return this.getRandomPath();
            }
            Queue<IntVector2> queue = new Queue<IntVector2>(localPath.Select(l => topLeft + l * Tools.CellSizeInCoord));
            return queue;
        }

    }
}
