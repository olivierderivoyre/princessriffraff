﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

namespace PrincessRiffRaff
{
    public class DiamondMobTrajectory
    {
        private readonly Mob mob;
        private readonly IntVector2 centerPosition;
        private readonly IntVector2 arenaTopLeft;
        private readonly int arenaRadius;
        private readonly int arenaSize;


        public DiamondMobTrajectory(Mob mob, IntVector2 centerPosition, int arenaRadiusInCell)
        {
            this.mob = mob;
            this.centerPosition = centerPosition;
            this.arenaRadius = arenaRadiusInCell;
            this.arenaTopLeft = centerPosition - new IntVector2(1, 1) * arenaRadius * Tools.CellSizeInCoord;
            this.arenaSize = arenaRadius + 1 + arenaRadius;
        }
        public IEnumerable QuarterCellMove(int speedInCellDuration)
        {
            IntVector2 quaterCellLocalPosition = IntVector2.Delta(this.centerPosition, this.mob.Position) / Tools.QuaterCellSizeInCoord;
            IntVector2 direction = new IntVector2();
            if (Math.Abs(quaterCellLocalPosition.X) + Math.Abs(quaterCellLocalPosition.Y) != this.arenaRadius*4)
            {
                ///Mob not in the trajectory, let go up or down
                direction.Y = (quaterCellLocalPosition.Y *-1 + Math.Abs(quaterCellLocalPosition.X) < this.arenaRadius * 4) ? -1 : 1;
            }
            else
            {
                direction.X = quaterCellLocalPosition.Y > 0 ? 1 : -1;
                if (quaterCellLocalPosition.Y == 0)
                {
                    direction.X = quaterCellLocalPosition.X > 0 ? -1 : 1;
                }
                direction.Y = quaterCellLocalPosition.X > 0 ? -1 : 1;
                if (quaterCellLocalPosition.X == 0)
                {
                    direction.Y = quaterCellLocalPosition.Y > 0 ? -1 : 1;
                }
            }
            if (direction == IntVector2.Zero)
            {
                throw new Exception();
            }
            int cellDuration = speedInCellDuration;
            if (direction.X != 0 && direction.Y != 0)
            {
                cellDuration = speedInCellDuration * 141 / 100;///Avoid diag mob go merge with horizontal mobs
            }
            FixedTargetMoveAnimation animation = FixedTargetMoveAnimation.CreateQuarterCellMove(this.mob.Position, direction, cellDuration);
            while (!animation.IsFinish())
            {
                this.mob.Position = animation.GetUpdatedPosition();
                yield return null;
            }           
        }
    }
}
