﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

namespace PrincessRiffRaff
{
    public class GoNearPlayerTrajectory : DijkstraMobTrajectory
    {

        private IntVector2 centerModif;///Avoid merging of pack of mobs
        private IntVector2 currentTarget;

        public GoNearPlayerTrajectory(Mob source)
            : base(source)
        {
            this.centerModif = new IntVector2(Tools.Rand.Next(-4, 5), Tools.Rand.Next(-4, 5)) * Tools.QuaterCellSizeInCoord;
        }
        protected override bool IsPathNeedToBeRefreshed()
        {
            if (IntVector2.GetDistance(this.mob.TargetPlayer.Position, this.currentTarget) > 1 * Tools.CellSizeInCoord)
            {
                return true;
            }
            return base.IsPathNeedToBeRefreshed();
        }


        protected override Queue<IntVector2> GetPath()
        {
            if (Tools.RandomWin(10))
            {
                this.centerModif = new IntVector2(Tools.Rand.Next(-4, 5), Tools.Rand.Next(-4, 5)) * Tools.QuaterCellSizeInCoord;
            }
            this.currentTarget = this.mob.TargetPlayer.Position + this.centerModif;
            
            IntVector2 topLeft = Tools.FloorToCell(this.mob.Position) - new IntVector2(1, 1) * lookAroundNbCell * Tools.CellSizeInCoord;
            bool[] map = createDijkstraMap(topLeft);
            IntVector2 start = new IntVector2(lookAroundNbCell, lookAroundNbCell);
            IntVector2 target = (this.currentTarget - topLeft) / Tools.CellSizeInCoord;
            List<IntVector2> localPath = Dijkstra.GetDirectionOf(map, DijkstraMapSize, start, target);
            if (localPath == null || localPath.Count == 0)
            {
                return this.getRandomPath();
            }
            Queue<IntVector2> queue = new Queue<IntVector2>(localPath.Select(l => topLeft + l * Tools.CellSizeInCoord));
            return queue;
        }






    }
}
