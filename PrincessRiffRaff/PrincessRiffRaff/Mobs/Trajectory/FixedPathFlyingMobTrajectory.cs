﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using Microsoft.Xna.Framework;

namespace PrincessRiffRaff
{
    public class FixedPathFlyingMobTrajectory
    {
        private readonly Mob mob;
        private readonly IntVector2[] cycleLocation;
        private int nextTargetLocationIndex = 0;
        private LinearFlyingMobTrajectory currentTrajectory;

        public FixedPathFlyingMobTrajectory(Mob mob, IntVector2[] cycleLocation)
        {
            this.mob = mob;
            this.cycleLocation = cycleLocation;
            if (cycleLocation.Length < 2)
            {
                throw new ArgumentException();
            }            
        }

        public static FixedPathFlyingMobTrajectory NewRectangleTrajectory(Mob mob, IntVector2 referential, Rectangle cellRect)
        {
            IntVector2[] localPath = new[]{
                new IntVector2(cellRect.X, cellRect.Y),
                new IntVector2(cellRect.X, cellRect.Bottom),
                new IntVector2(cellRect.Right, cellRect.Bottom),
                new IntVector2(cellRect.Right, cellRect.Y),
             };
            IntVector2[] cycleLocation = localPath.Select(l => referential + l * Tools.CellSizeInCoord).ToArray();
            return new FixedPathFlyingMobTrajectory(mob, cycleLocation);
        }


        public IEnumerable QuarterCellMove(int speedInCellDuration)
        {
            if (this.currentTrajectory == null || this.currentTrajectory.IsFinish())
            {
                IntVector2 targetPosition = this.cycleLocation[this.nextTargetLocationIndex];
                this.currentTrajectory = new LinearFlyingMobTrajectory(this.mob, targetPosition);
                this.nextTargetLocationIndex = (this.nextTargetLocationIndex + 1) % this.cycleLocation.Length;
            }
            return this.currentTrajectory.QuarterCellMove(speedInCellDuration);
        }
    }
}
