﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using System.Collections;

namespace PrincessRiffRaff
{
    public class FlyingAroundPlayerTrajectory
    {
        private readonly Mob mob;
        private readonly int circleRadius;
        private IntVector2? centerModif = null;
        private readonly bool rotationSign;

        public FlyingAroundPlayerTrajectory(Mob mob, int circleRadius)
        {
            this.mob = mob;
            this.circleRadius = circleRadius;
            this.rotationSign = Tools.RandomWin(2);
        }

        public IEnumerable QuarterCellMove(int speedInCellDuration)
        {
            if (centerModif == null || Tools.RandomWin(40))
            {
                ///Avoid merging of pack of mobs
                centerModif = new IntVector2(Tools.Rand.Next(-4, 4), Tools.Rand.Next(-4, 4)) * Tools.QuaterCellSizeInCoord;
            }
            IntVector2 direction = new IntVector2(1, 1);
            IntVector2 center = this.mob.TargetPlayer.Position + centerModif.Value;
            IntVector2 position = this.mob.Position;
            IntVector2 delta = position - center;
            int distance = delta.GetDistance();
            //int circleRadius = 3 * Tools.CellSizeInCoord;
            if (distance > circleRadius + 4 * Tools.QuaterCellSizeInCoord)
            {
                direction = MobAI.GetFlyingDirectionToGoZero(delta);
            }
            else if (distance < circleRadius - 4 * Tools.QuaterCellSizeInCoord)
            {
                direction = MobAI.GetFlyingDirectionToGoFarZero(delta);
            }
            else
            {
                IntVector2 far = MobAI.GetFlyingDirectionToGoFarZero(delta);
                ///Rotate 90
                direction = new IntVector2(-far.Y, far.X);

            }
            FixedTargetMoveAnimation animation = FixedTargetMoveAnimation.CreateQuarterCellMove(this.mob.Position, direction, speedInCellDuration);
            while (!animation.IsFinish())
            {
                this.mob.Position = animation.GetUpdatedPosition();
                yield return null;
            }

        }
    }
}
