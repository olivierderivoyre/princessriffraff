﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

namespace PrincessRiffRaff
{
    public abstract class DijkstraMobTrajectory
    {
        protected const int lookAroundNbCell = 4;
        protected const int DijkstraMapSize = lookAroundNbCell + 1 + lookAroundNbCell;
       

        protected readonly Level level;
        protected readonly Mob mob;
        private Queue<IntVector2> path = new Queue<IntVector2>();
        private IntVector2? nextCell;
        
        protected DijkstraMobTrajectory(Mob mob)
        {
            this.mob = mob;
            this.level = mob.Level;
        }


        protected Queue<IntVector2> getRandomPath()
        {
            IntVector2 current = Tools.FloorToCell(this.mob.Position);
            IntVector2[] freeCells = Tools.CardinalPoints
                .Select(l => current + l * Tools.CellSizeInCoord)
                .Where(p => !this.level.Land.HasBlock(p))
                .ToArray();
            if (freeCells.Length == 0)
            {
                return null;
            }
            IntVector2 nextCell = Tools.RandPickOne(freeCells);
            return new Queue<IntVector2>(new[] { current, nextCell });
        }

        protected bool[] createDijkstraMap(IntVector2 topLeft)
        {
            bool[] map = new bool[DijkstraMapSize * DijkstraMapSize];
            for (int i = 0; i < DijkstraMapSize; i++)
            {
                for (int j = 0; j < DijkstraMapSize; j++)
                {
                    IntVector2 position = topLeft + new IntVector2(i, j) * Tools.CellSizeInCoord;
                    map[j * DijkstraMapSize + i] = this.level.Land.HasBlock(position);
                }
            }
            return map;
        }

        protected abstract Queue<IntVector2> GetPath();

        protected virtual bool IsPathNeedToBeRefreshed()
        {
            if (this.path == null || this.path.Count == 0)                
            {
                return true;
            }
            if (this.path.Peek() != this.mob.Position)
            {
                return true;
            }
            return false;
        }

        private IntVector2? getNextCell()
        {
            if (this.IsPathNeedToBeRefreshed())
            {
                this.path = this.GetPath();
            }
            if (path == null)
            {
                return null;
            }
            if (this.path.Peek() == this.mob.Position)
            {
                this.path.Dequeue();
            }
            if (this.path.Count > 0)
            {
                return this.path.Peek();
            }
            else
            {
                return null;
            }
        }
       

        public IEnumerable QuarterCellMove(int speedInCellDuration)
        {
            if (this.nextCell == null || this.mob.Position == this.nextCell.Value)
            {                
                this.nextCell = this.getNextCell();
            }
            if (this.nextCell == null)
            {
                yield return CoroutineCpu.Current.GoSleep(Tools.Rand.Next(30, 60));
                yield break;
            }
            IntVector2 delta = IntVector2.Delta(this.mob.Position, this.nextCell.Value);
            IntVector2 currentDirection = new IntVector2(Math.Sign(delta.X), Math.Sign(delta.Y));
            if (currentDirection != IntVector2.Zero)
            {
                int cellDuration = speedInCellDuration;
                if (currentDirection.X != 0 && currentDirection.Y != 0)
                {
                    cellDuration = speedInCellDuration * 141 / 100;///Avoid diag mob go merge with horizontal mobs
                }
                FixedTargetMoveAnimation animation = FixedTargetMoveAnimation.CreateQuarterCellMove(this.mob.Position, currentDirection, cellDuration);
                while (!animation.IsFinish())
                {
                    this.mob.Position = animation.GetUpdatedPosition();
                    yield return null;
                }
            }
            else
            {

                yield return CoroutineCpu.Current.GoSleep(Tools.Rand.Next(1, 10));
            }
        }

    }
}
