﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

namespace PrincessRiffRaff
{
    public class LinearFlyingMobTrajectory
    {
        private readonly Mob mob;
        private readonly IntVector2 targetPosition;



        public LinearFlyingMobTrajectory(Mob mob, IntVector2 targetTrajectory)
        {
            this.mob = mob;
            this.targetPosition = targetTrajectory;            
        }
        public bool IsFinish()
        {
            return this.mob.Position == this.targetPosition;
        }
        public IEnumerable QuarterCellMove(int speedInCellDuration)
        {
            if (this.IsFinish())
            {
                yield break;
            }

            IntVector2 direction = MobAI.GetFlyingDirectionToGo(this.mob.Position, this.targetPosition);            
            int cellDuration = speedInCellDuration;
            if (direction.X != 0 && direction.Y != 0)
            {
                cellDuration = speedInCellDuration * 141 / 100;///Avoid diag mob go merge with horizontal mobs
            }
            FixedTargetMoveAnimation animation = FixedTargetMoveAnimation.CreateQuarterCellMove(this.mob.Position, direction, cellDuration);
            while (!animation.IsFinish())
            {
                this.mob.Position = animation.GetUpdatedPosition();
                yield return null;
            }           
        }
    }
}
