﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PrincessRiffRaff
{
    /// <summary>
    /// Just stand there an fire.
    /// </summary>
    public class JustFireAI : MobAI
    {
        protected override System.Collections.IEnumerable Live()
        {
           
            while (true)
            {
                yield return null;
                this.Mob.Weapon.TryActivate();
            }
        }
    


    }
}
