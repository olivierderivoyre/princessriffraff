﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PrincessRiffRaff
{
    public class SkeletonWizardAI: MobAI
    {
        private Bullet bullet;
        protected override System.Collections.IEnumerable Live()
        {
            while (!this.Mob.IsDead)
            {
                yield return null;
                if(this.Mob.TargetPlayer == null)
                {
                    continue;
                }
                this.Mob.CurrentInvocation = new Invocation(this, this.Mob.Weapon);
                while (!this.Mob.CurrentInvocation.IsFinished)
                {
                    yield return null;
                    this.Mob.CurrentInvocation.AIManagedInvoke();
                }
                var trajectory = new BulletMovingTargetTrajectory(Tools.CenterCharacter(this.Mob.Position), this.Mob.Weapon.BulletAttributes, this.Mob.TargetPlayer);
                this.bullet = new Bullet(this.Level, this.Mob.Weapon.BulletAttributes, trajectory);
                this.Level.Add(bullet);
                this.Level.Music.MobBasicFire.Play();
                while (!bullet.IsFisnish())
                {
                    yield return null;
                }
                yield return CoroutineCpu.Current.GoSleep(30);
            }
        }

        public override void OnDying()
        {
            base.OnDying();
            if (this.bullet != null)
            {
                this.bullet.ForceFinish();
            }
        }
    }
}
