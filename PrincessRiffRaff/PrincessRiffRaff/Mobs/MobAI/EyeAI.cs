﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PrincessRiffRaff
{
    public class EyeAI : MobAI
    {
        private bool isHorizontal;
        public EyeAI(bool isHorizontal)
        {
            this.isHorizontal = isHorizontal;
        }

        protected override System.Collections.IEnumerable Live()
        {           
           // bool isHorizontal = ((this.Mob.Position.X + this.Mob.Position.Y) / Tools.CellSizeInCoord) % 2 == 0;
            IntVector2 direction;
            direction.X = this.isHorizontal ? Tools.RandPickOne(1, -1) : 0;
            direction.Y = !this.isHorizontal ? Tools.RandPickOne(1, -1) : 0;

            while (true)
            {
                yield return null;
                IntVector2 currentDirection = this.Level.Land.LimitCharacterMove(this.Mob.Position, direction);
                if (currentDirection == IntVector2.Zero)
                {
                    direction = direction * -1;
                    currentDirection = this.Level.Land.LimitCharacterMove(this.Mob.Position, direction);
                    if (currentDirection == IntVector2.Zero)
                    {
                        yield return CoroutineCpu.Current.GoSleep(60*2);
                    }
                }
                else
                {
                    FixedTargetMoveAnimation animation = FixedTargetMoveAnimation.CreateQuarterCellMove(this.Mob.Position, currentDirection, 60);
                    while (!animation.IsFinish())
                    {
                        this.Mob.Position = animation.GetUpdatedPosition();
                        yield return null;
                    }                    
                }
                if (Tools.FloorToCell(this.Mob.Position) == this.Mob.Position)
                {
                    if (this.Mob.Weapon != null)
                    {
                        yield return CoroutineCpu.Current.GoSleep(30);
                        this.perpendicularShoot(!this.isHorizontal);

                    }
                }
            }
        }


        private void perpendicularShoot(bool isShootHorizontal)
        {            
            foreach (int i in new[] { -1, 1 })
            {
                IntVector2 direction;
                direction.X = isShootHorizontal ? i : 0;
                direction.Y = !isShootHorizontal ? i : 0;
                if (!this.Level.Land.HasBulletBlock(this.Mob.Position + direction * Tools.CellSizeInCoord, this.Mob.Weapon.BulletAttributes))
                {
                    this.Mob.PlaySoundOnScreen( this.Level.Music.MobBasicFire); 
                    BulletLinearTrajectory trajectory = new BulletLinearTrajectory(Tools.CenterCharacter(this.Mob.Position), direction * 256, this.Mob.Weapon.BulletAttributes, null);
                    Bullet bullet = new Bullet(this.Level, this.Mob.Weapon.BulletAttributes, trajectory);
                    this.Level.Add(bullet);
                }
            }
        }


       
        
    }
}
