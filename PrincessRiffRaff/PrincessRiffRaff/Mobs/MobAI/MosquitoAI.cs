﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

namespace PrincessRiffRaff
{
    /// <summary>
    /// Go near player and shoot.
    /// </summary>
    public class MosquitoAI : MobAI
    {
        
        protected override IEnumerable Live()
        {
            
            yield return null;
            
            FlyingAroundPlayerTrajectory around = new FlyingAroundPlayerTrajectory(this.Mob, 4 * Tools.CellSizeInCoord);
            
            while (true)
            {
                int startRounding = this.Level.Tick;
                while (this.Level.Tick - startRounding < 60 * 12)
                {
                    yield return CoroutineCpu.Current.Go(around.QuarterCellMove(30));
                }
                while (!TryHitPlayer())
                {
                    LinearFlyingMobTrajectory attack = new LinearFlyingMobTrajectory(this.Mob, this.Mob.TargetPlayer.Position);
                    yield return CoroutineCpu.Current.Go(attack.QuarterCellMove(15));
                }                                
            }
        }

        private bool TryHitPlayer()
        {
            bool hit = false;
            foreach (Player p in this.Level.GetFriendlyUnitsHitBy(Tools.CenterCharacter(this.Mob.Position), Tools.QuaterCellSizeInCoord))
            {
                p.HitBy(this.Mob.Weapon.BulletAttributes, -1);
                hit = true;
            }
            return hit;
        }

        
    


    }
}
