﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PrincessRiffRaff
{
    public class BatAI : MobAI
    {

        protected override System.Collections.IEnumerable Live()
        {
            yield return null;
            FlyingAroundPlayerTrajectory around = new FlyingAroundPlayerTrajectory(this.Mob, 3 * Tools.CellSizeInCoord);
            while (true)
            {
                yield return CoroutineCpu.Current.Go(around.QuarterCellMove(64));
                this.TryActivateClearShoot();
            }


            //IntVector2? centerModif = null;
            //while (true)
            //{
            //    if (centerModif == null || Tools.RandomWin(40))
            //    {
            //        ///Avoid merging of pack of mobs
            //        centerModif = new IntVector2(Tools.Rand.Next(-4, 4), Tools.Rand.Next(-4, 4)) * Tools.QuaterCellSizeInCoord;
            //    }
            //    IntVector2 direction = new IntVector2(1, 1);
            //    IntVector2 center = this.Mob.TargetPlayer.Position + centerModif.Value;                
            //    IntVector2 position = this.Mob.Position;
            //    IntVector2 delta = position - center;
            //    int distance = delta.GetDistance();
            //    int circleRadius = 3 * Tools.CellSizeInCoord;
            //    if (distance > circleRadius + 4 * Tools.QuaterCellSizeInCoord)
            //    {
            //        direction = MobAI.GetFlyingDirectionToGoZero(delta);
            //    }
            //    else if (distance < circleRadius - 4 * Tools.QuaterCellSizeInCoord)
            //    {
            //        direction = MobAI.GetFlyingDirectionToGoFarZero(delta);
            //    }
            //    else
            //    {
            //        if (this.Level.Land.GetFurniture(Tools.CenterCharacterCell(this.Mob.Position)) == null)
            //        {
            //            this.Mob.Weapon.TryActivate();
            //        }

            //        IntVector2 far = GetFlyingDirectionToGoFarZero(delta);
            //        ///Rotate 90
            //        direction = new IntVector2(-far.Y, far.X);
                    
            //    }

               
            //    FixedTargetMoveAnimation animation = FixedTargetMoveAnimation.CreateQuarterCellMove(this.Mob.Position, direction, 64);
            //    while (!animation.IsFinish())
            //    {
            //        this.Mob.Position = animation.GetUpdatedPosition();
            //        yield return null;
            //    }
            //    yield return null;
            //}
        }
    


    }
}
