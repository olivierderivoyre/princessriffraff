﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

namespace PrincessRiffRaff
{
    public class BeeAI : MobAI
    {
        private bool hasEnrage = false;
        public BeeAI()
        {

        }

        protected override System.Collections.IEnumerable Live()
        {
            IntVector2 initialPosition = this.Mob.Position;

            FlyingRandomTrajectory trajectory = new FlyingRandomTrajectory(this.Mob, initialPosition, 6 * Tools.CellSizeInCoord);

            while (this.Mob.Life == this.Mob.MaxLife && !this.hasEnrage)
            {
                yield return null;
                yield return CoroutineCpu.Current.Go(trajectory.QuarterCellMove(60));
                if (Tools.FloorToCell(this.Mob.Position) == this.Mob.Position && Tools.RandomWin(3))
                {
                    for (int i = 0; i < 60 && this.Mob.Life == this.Mob.MaxLife && !this.hasEnrage; i++)
                    {
                        yield return null;
                    }
                }
            }
            foreach (Mob mob in this.Level.GetMobAroundCell(this.Mob.Position, 2))
            {
                if (mob.MobAI is BeeAI)
                {
                    ((BeeAI)mob.MobAI).hasEnrage = true;
                }
            }

            while (this.Mob.TargetPlayer != null)
            {
                yield return CoroutineCpu.Current.Go(goDropBombOnPlayer());
                if (this.Mob.IsDead)
                {
                    yield break;
                }
                IntVector2 randomPosition = this.Mob.Position + new IntVector2(Tools.Rand.Next(-2, 3), Tools.Rand.Next(-2, 3)) * Tools.CellSizeInCoord;

                LinearFlyingMobTrajectory someWhereAround = new LinearFlyingMobTrajectory(this.Mob, randomPosition);
                while (!someWhereAround.IsFinish())
                {
                    yield return CoroutineCpu.Current.Go(someWhereAround.QuarterCellMove(20));
                }

            }
        }

        private IEnumerable goDropBombOnPlayer()
        {
            while (this.Mob.TargetPlayer != null)
            {
                if (IntVector2.GetDistance(this.Mob.Position, this.Mob.TargetPlayer.Position) < 2 * Tools.QuaterCellSizeInCoord)
                {
                    break;
                }
                IntVector2 direction = GetFlyingDirectionToGo(this.Mob.Position, this.Mob.TargetPlayer.Position);
                FixedTargetMoveAnimation animation = FixedTargetMoveAnimation.CreateQuarterCellMove(this.Mob.Position, direction, 18);
                while (!animation.IsFinish())
                {
                    this.Mob.Position = animation.GetUpdatedPosition();
                    yield return null;
                }
            }

            if (this.dropBomb() != null)
            {
                this.Mob.IsDead = true;
                yield break;
            }
        }



        //public override void OnDying()
        //{
        //    base.OnDying();
        //    this.dropBomb();
        //}


    }
}
