﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

namespace PrincessRiffRaff
{
    /// <summary>
    /// Go near player and shoot.
    /// </summary>
    public class MobGoNearPlayerAI : MobAI
    {
        private int speed_timeForQuaterCell;

        public MobGoNearPlayerAI(int speed_timeForQuaterCell)
        {
            this.speed_timeForQuaterCell = speed_timeForQuaterCell;
        }

        protected override IEnumerable Live()
        {
            
            yield return null;
            GoNearPlayerTrajectory trajectory = new GoNearPlayerTrajectory(this.Mob);
            
            while (true)
            {
               
                TryActivateClearShoot();          
                int distance = IntVector2.GetDistance(this.Mob.Position, this.Mob.TargetPlayer.Position);
                if (distance > 3 * Tools.QuaterCellSizeInCoord)
                {
                    yield return CoroutineCpu.Current.Go(trajectory.QuarterCellMove(this.speed_timeForQuaterCell));
                }
                else
                {
                    yield return null;
                }                  
            }
        }

        
    


    }
}
