﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

namespace PrincessRiffRaff
{
    public class TornadoAI : MobAI
    {
        protected override IEnumerable Live()
        {           
            int angus = Tools.Rand.Next(360);
            while (!this.Mob.IsDead)
            {
                yield return null;
                foreach (Player p in this.Level.GetFriendlyUnitsHitBy(Tools.CenterCharacter(this.Mob.Position), Tools.QuaterCellSizeInCoord))
                {
                    p.HitBy(this.Mob.Weapon.BulletAttributes, -1);
                }
                IntVector2 incr = Weapon.RotateCoord(new IntVector2(255, 0), angus);
                IntVector2 newPosition = this.Mob.Position + incr;
                if (!this.isValidPosition(newPosition))
                {
                    angus = Tools.Rand.Next(360);
                    continue;
                }
                this.Mob.Position = newPosition;
            }
        }

        private bool isValidPosition(IntVector2 newPosition)
        {
            IntVector2 center = Tools.CenterCharacter(newPosition);
            foreach (IntVector2 around in Tools.AroundPoints)
            {
                IntVector2 point = center + around * (Tools.QuaterCellSizeInCoord * 7 / 4);
                if (this.Level.Land.HasBlock(point))
                {
                    return false;
                }
            }
            return true;
        }
    }
}
