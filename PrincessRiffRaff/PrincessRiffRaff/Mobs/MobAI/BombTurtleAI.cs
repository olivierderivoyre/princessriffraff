﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

namespace PrincessRiffRaff
{
    public class BombTurtleAI : MobAI
    {

      

        protected override IEnumerable Live()
        {
            yield return null;
            GoNearPlayerTrajectory trajectory = new GoNearPlayerTrajectory(this.Mob);

            while (true)
            {
                yield return null;               
                while (this.getDistanceToNearestPlayer() > 4 * Tools.QuaterCellSizeInCoord)
                {                   
                    yield return CoroutineCpu.Current.Go(trajectory.QuarterCellMove(120));                   
                }
                Bomb bomb = this.dropBomb();
                if (bomb != null)
                {
                    RunAwayTrajectory runAway = new RunAwayTrajectory(this.Mob, bomb.Position);
                    while (!bomb.IsFinished)
                    {
                        yield return CoroutineCpu.Current.Go(runAway.QuarterCellMove(40));
                    }
                }
                CoroutineCpu.Current.GoSleep(30);
            }
        }


        private int getDistanceToNearestPlayer()
        {
            int min = 100 * Tools.CellSizeInCoord;
            foreach (Player player in this.Level.GetAlivePlayers())
            {
                int distance = IntVector2.GetDistance(this.Mob.Position, player.Position);
                if (distance < min)
                {
                    min = distance;
                }
            }
            return min;
        }
    }
}
