﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

namespace PrincessRiffRaff
{
    public class MobAI
    {
        public Level Level;
        public Mob Mob;
        private readonly CoroutineCpu cpu = new CoroutineCpu();
        public void Update()
        {
            if (this.Mob.IsSpleeping && this.Mob.WakeUp)
            {
                this.Mob.IsSpleeping = false;
                this.Mob.WakeUp = false;
                this.Level.Aggro(this.Mob.Position, 2, this.Mob.TargetPlayer, this.Mob);
            }
            if (this.Mob.CanSleep && this.Mob.IsSpleeping)
            {
                return;
            }
            this.SetTargetPlayer();
            if (this.Mob.CanSleep && this.Mob.IsSpleeping)
            {
                return;
            }
            if (this.cpu.IsIdle)
            {
                if (this.Mob.Weapon != null)
                {
                    this.Mob.Weapon.Cooldown.RandomizeStarting(this.Level.Tick);
                }
                this.cpu.Go(this.Live());
            }
            else
            {
                this.OnEachTick();
                this.cpu.Update();
            }

        }
        /// <summary>
        /// For boss, to check user does not escape
        /// </summary>
        protected virtual void OnEachTick()
        {
        }

        /// <summary>
        /// Once dead
        /// </summary>
        public virtual void OnDying()
        {
        }

        protected virtual void SetTargetPlayer()
        {
            if (this.Mob.TargetPlayer != null)
            {
                if (!this.Mob.TargetPlayer.IsActive)
                {
                    this.Mob.TargetPlayer = null;
                }
            }
            if (this.Mob.TargetPlayer == null || Tools.RandomWin(3 * 60))
            {
                this.Mob.TargetPlayer = this.Level.GetNearestAlivePlayer(this.Mob.Position);
            }
            if (this.Mob.TargetPlayer == null)
            {
                this.Mob.IsSpleeping = true;
            }
        }

        protected virtual IEnumerable Live()
        {
            yield return null;
        }
        /// <summary>
        /// Overriden for boss to cleanup the helper
        /// </summary>
        public virtual void ResetFight()
        {
        }

        public static IntVector2 GetFlyingDirectionToGo(IntVector2 from, IntVector2 to)
        {
            if (from == to)
            {
                return IntVector2.Zero;
            }
            IntVector2 delta = IntVector2.Delta(from, to);
            IntVector2 d = new IntVector2(Math.Sign(delta.X), Math.Sign(delta.Y));
            if (Math.Abs(delta.X) > 2 * Math.Abs(delta.Y))
            {
                d.Y = 0;
            }
            else if (Math.Abs(delta.Y) > 2 * Math.Abs(delta.X))
            {
                d.X = 0;
            }
            return d;
        }
        public static IntVector2 GetFlyingDirectionToGoFarZero(IntVector2 delta)
        {
            if (delta == IntVector2.Zero)
            {
                return new IntVector2(1, 0);
            }
            return GetFlyingDirectionToGo(IntVector2.Zero, delta);
            //IntVector2 d = new IntVector2(Math.Sign(position.X), Math.Sign(position.Y));
            //if (Math.Abs(position.X) > 2 * Math.Abs(position.Y))
            //{
            //    d.Y = 0;
            //}
            //else if (Math.Abs(position.Y) > 2 * Math.Abs(position.X))
            //{
            //    d.X = 0;
            //}
            //return d;
        }
        public static IntVector2 GetFlyingDirectionToGoZero(IntVector2 delta)
        {
            if (delta == IntVector2.Zero)
            {
                throw new ArgumentException();
            }
            return GetFlyingDirectionToGo(delta, IntVector2.Zero);
        }
        public int GetLifePercent()
        {
            return this.Mob.Life * 100 / this.Mob.MaxLife;
        }


        protected List<IntVector2> GetGroundDirectionToGoNear(IntVector2 targetPosition)
        {
            IntVector2 delta = IntVector2.Delta(this.Mob.Position, targetPosition);
            IntVector2 direction = this.Level.Land.LimitCharacterMove(this.Mob.Position, new IntVector2(Math.Sign(delta.X), Math.Sign(delta.Y)));
            List<IntVector2> returned = new List<IntVector2>();
            if (direction != null)
            {
                returned.Add(direction);
            }
            else
            {
            }
            return returned;
        }

        protected bool TryActivateClearShoot()
        {
            if (this.Mob.Weapon.CanClearShoot(this.Mob, this.Mob.TargetPlayer))
            {
                return this.Mob.Weapon.TryActivate();
            }
            return false;
        }

        protected Bomb dropBomb()
        {
            IntVector2 bombPosition = Tools.CenterCharacterCell(this.Mob.Position);
            if (!this.Level.CanAddBombHere(bombPosition))
            {
                return null;
            }
            BulletAttributes bombAttributes = WeaponTypeHelper.NewMobAoeBomb(this.Mob);
            bombAttributes.TargetPlayer = this.Mob.TargetPlayer;
            Bomb bomb = new Bomb(this.Level, bombPosition, bombAttributes);
            this.Level.Add(bomb);
            return bomb;
        }

    }
}
