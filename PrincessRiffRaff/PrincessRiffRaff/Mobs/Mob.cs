﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace PrincessRiffRaff
{
    public class Mob : Character
    {
        public TextureRect Image;
        public MobAI MobAI;
        public Weapon Weapon;
        public bool CanSleep = true;
        public bool IsSpleeping = true;
        public bool WakeUp = false;
        public bool IsFinished = false;
        public Player TargetPlayer;
        public bool IsBoss;
        public MobDyeAnimation MobDyeAnimation;

        public Mob()
        {
           
        }

        public void Draw(Screen screen)
        {
            Color color = Color.White;
            bool hasJustBeenHit = this.Level.Tick - this.LastBeenHitTick < 5;
            if (hasJustBeenHit)
            {
                color = Color.Red;
            }          
            screen.Draw64(this.Image, this.Position, color);
            if (this.CurrentInvocation != null)
            {
                this.CurrentInvocation.Draw(screen, this);
            }
        }

        public void Update()
        {
            if (this.IsDead)
            {
                if (!this.IsFinished)
                {
                    this.IsFinished = true;                    
                    long score = this.Level.MobScoreValue.GetMobScore(this);
                    this.Level.Score += score;

                    if (this.IsBoss)
                    {
                        ///Victory!!
                        
                        this.MobDyeAnimation = new BossDyeAnimation(this, score);
                        this.Level.Add(this.MobDyeAnimation);
                    }
                    else
                    {
                        this.MobDyeAnimation = new MobDyeAnimation(this, score);
                        this.Level.Add(this.MobDyeAnimation);
                    }                   
                    if (this.MobAI != null)
                    {
                        this.MobAI.OnDying();
                    }                   
                }
            }
            else
            {
                if (this.MobAI != null)
                {
                    this.MobAI.Update();
                }
            }
        }
    }
}
