﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace PrincessRiffRaff
{
    public class MobGrave : IDrawable
    {
        private static readonly TextureRect flower1Image = TextureRect.NewItems32(7, 1);
        private static readonly TextureRect flower2Image = TextureRect.NewItems32(8, 1);
        private static readonly TextureRect flower3Image = TextureRect.NewItems32(9, 1);
        private static readonly TextureRect flowerTreeImage = TextureRect.NewGround(7, 0);

        private readonly IntVector2 center;
        private readonly int radius;
        private readonly IntVector2 Position;
        private readonly TextureRect Image;
        private bool isBoss;

        ///No grave on water or on furniture
        public bool IsOverBlock(Level level)
        {
            foreach(IntVector2 p in Tools.DiagonalPoints)
            {
                if (level.Land.HasBlock(this.center + p * (radius - 1))) ///-1 for border limit
                {
                    return true;
                }
            }
            return false;            
        }

        public MobGrave(IntVector2 center, bool isBoss)
        {
            this.center = center;            
            this.isBoss = isBoss;           
            if (this.isBoss)
            {
                this.Image = MobGrave.flowerTreeImage;
                this.radius = 2 * Tools.QuaterCellSizeInCoord;
               
            }
            else
            {
                this.Image = Tools.RandPickOne(MobGrave.flower1Image, MobGrave.flower2Image, MobGrave.flower3Image);
                this.radius = Tools.QuaterCellSizeInCoord;
            }
            this.Position = this.center - new IntVector2(1, 1) * radius;
        }

        public void Draw(Screen screen)
        {
            if (this.isBoss)
            {
                screen.Draw64(this.Image, this.Position, Color.White);
            }
            else
            {
                screen.Draw32(this.Image, this.Position, Color.White);
            }
        }   
    }
}
