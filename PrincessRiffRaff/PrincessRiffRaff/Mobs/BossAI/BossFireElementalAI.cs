﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using Microsoft.Xna.Framework;

namespace PrincessRiffRaff
{
    public class BossFireElementalAI : BossAI
    {


        protected override IEnumerable BossLive()
        {
            yield return null;
            Cooldown cooldown = new Cooldown(1, 20 * 60);
            cooldown.StartCooldown(this.Level.Tick);
            bool isEndGame = false;
            while (!this.Mob.IsDead)
            {
                yield return null;
                this.TryActivateClearShoot();
                if (this.Mob.TargetPlayer == null
                    || IntVector2.GetDistance(this.Mob.Position, this.Mob.TargetPlayer.Position) < 2 * Tools.QuaterCellSizeInCoord)
                {
                    continue;
                }
                IntVector2 direction = GetFlyingDirectionToGo(this.Mob.Position, this.Mob.TargetPlayer.Position);
                int speed = 60;
                if (this.GetLifePercent() < 40)
                {
                    speed = 35;
                }
                else if (this.GetLifePercent() < 60)
                {
                    speed = 50;
                }
                FixedTargetMoveAnimation animation = FixedTargetMoveAnimation.CreateQuarterCellMove(this.Mob.Position, direction, speed);
                while (!animation.IsFinish())
                {
                    this.Mob.Position = animation.GetUpdatedPosition();
                    yield return null;
                }
                if (!isEndGame)
                {
                    if (this.GetLifePercent() < 25)
                    {
                        isEndGame = true;
                        this.Mob.EndEnrageTick = this.Level.Tick + 10000;
                        yield return CoroutineCpu.Current.Go(this.castFireAllGround(60 * 1000));
                    }
                }
                this.tryBurnGroundUnderBoss();
                this.TryActivateClearShoot();
                if (!isEndGame)
                {
                    if (cooldown.TryStartCooldown(this.Level.Tick, this.Mob.IsEnraged()))
                    {

                        yield return CoroutineCpu.Current.Go(this.castFireAllGround());

                    }
                }
                else
                {
                    this.fireAllGroundNoCasting(60 * 1000);
                }

            }
        }





    }
}
