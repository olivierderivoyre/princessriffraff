﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using Microsoft.Xna.Framework;

namespace PrincessRiffRaff
{
    public class BossPurpleBeholderAI : BossAI
    {
        //private TextureRect AwakeImage = TextureRect.NewMob(0, 1);
        //private TextureRect SleepingImage = TextureRect.NewMob(1, 1);

        protected override IEnumerable BossLive()
        {
            yield return null;
            FixedPathFlyingMobTrajectory trajectory = FixedPathFlyingMobTrajectory.NewRectangleTrajectory(this.Mob, this.BossZone.Position, 
                new Rectangle(-4,-4,8,8));
            Cooldown coolDown = new Cooldown(1, 35, 3, 10 * 60);
            while (this.GetLifePercent() > 60)
            {
                yield return null;
                CoroutineCpu.Current.Go(trajectory.QuarterCellMove(60));
                if (coolDown.TryStartCooldown(this.Level.Tick, false))
                {
                    RaiseExplosionOfBullets();
                }
            }
            this.Level.Music.BossRoar.Play();
            coolDown = new Cooldown(1, 35, 3, 4 * 60);
            while (this.GetLifePercent() > 30)
            {
                yield return null;
                CoroutineCpu.Current.Go(trajectory.QuarterCellMove(40));
                if (coolDown.TryStartCooldown(this.Level.Tick, false))
                {
                    RaiseExplosionOfBullets();
                }
            }
            this.Level.Music.BossRoar.Play();
            coolDown = new Cooldown(1, 35);
            while (!this.Mob.IsDead)
            {
                yield return null;
                CoroutineCpu.Current.Go(trajectory.QuarterCellMove(20));
                if (coolDown.TryStartCooldown(this.Level.Tick, false))
                {
                    RaiseExplosionOfBullets();
                }
            }
        }


        private void RaiseExplosionOfBullets()
        {
            this.Level.Music.MobSharpExplosion.Play();
            for (int i = 0; i < 360; i+=5)
            {
                BulletAttributes attributes = this.Mob.Weapon.BulletAttributes.Clone();
                attributes.GoOverFurniture = false;
                attributes.SpeedInCoord = 4000;
                attributes.RangeInCoord = 40 * Tools.CellSizeInCoord;
                IntVector2 from = Tools.CenterCharacter( this.Mob.Position);
                IntVector2 direction = new IntVector2(Tools.CosCoord[i], Tools.SinCoord[i]);
                var trajectory = new BulletLinearTrajectory(from, direction, attributes, null);
                var bullet = new Bullet(this.Level, attributes, trajectory);
                this.Level.Add(bullet);
            }
        }

    }
}
