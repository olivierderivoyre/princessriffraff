﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

namespace PrincessRiffRaff
{
    public class BossHairyBeholderAI : BossAI
    {

        protected override IEnumerable BossLive()
        {
            
            while (!this.Mob.IsDead)
            {
                yield return null;
                this.Mob.Weapon.TryActivate();

               
                ContinueSpiralFire(66);

            }
        }

       
    }
}
