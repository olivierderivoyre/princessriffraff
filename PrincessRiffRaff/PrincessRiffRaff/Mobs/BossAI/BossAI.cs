﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using Microsoft.Xna.Framework;

namespace PrincessRiffRaff
{
    public abstract class BossAI : MobAI
    {
        public BossZone BossZone;
        protected List<Mob> popedMobList = new List<Mob>();


        public void OnStartFight()
        {
            this.Level.Music.BossRoar.Play();
        }
        protected sealed override IEnumerable Live()
        {
            yield return null;
            this.Level.Music.BossRoar.Play();
            yield return CoroutineCpu.Current.GoSleep(40);

            yield return CoroutineCpu.Current.Go(this.BossLive());
        }
        protected abstract IEnumerable BossLive();
       
        public override void ResetFight()
        {
            base.ResetFight();
            resetPopedMobs();

            foreach (FireGround item in this.fireCells.Values)
            {
                this.Level.Remove(item);
            }
            this.fireCells.Clear();

            foreach (FireGroundBullet item in this.fireGroundBulletList)
            {
                this.Level.Remove(item);
            }
            this.fireGroundBulletList.Clear();

        }

        protected void resetPopedMobs()
        {
            foreach (Mob mob in this.popedMobList)
            {
                this.Level.Remove(mob);
            }
            this.popedMobList.Clear();
        }

        protected IEnumerable WaitOrLiving(int tickCount, int lifeLevel)
        {
            for (int i = 0; i < tickCount && this.GetLifePercent() > lifeLevel; i++)
            {
                yield return null;
            }
        }
        protected IEnumerable FireWhile(int lifeLevel)
        {
            while (this.GetLifePercent() > lifeLevel)
            {
               
                bool hasFire = this.Mob.Weapon.TryActivate();
                if (hasFire)///Change target
                {
                    this.Mob.TargetPlayer = this.Level.GetNearestAlivePlayer(this.Mob.Position);
                }
                yield return null;
            }
        }
        
        protected override void OnEachTick()
        {
            base.OnEachTick();
            this.updateRainFire();
            this.updateFireGrounds();
            this.updateFireGroundBullets();
        }

        #region RainFire (if player escape)

        private void updateRainFire()
        {
            if (this.Level.Tick % 10 == 0)
            {
                foreach (Player player in this.Level.GetAlivePlayers())
                {
                    int distance = IntVector2.Delta(player.Position, this.BossZone.Position).GetDistance();
                    if (distance > 8 * Tools.CellSizeInCoord)
                    {
                        RaiseRainOfFire(player);
                    }
                }
            }
        }

        

        private void RaiseRainOfFire(Player player)
        {
            for (int i = -20; i < 20; i++)
            {
                BulletAttributes attributes = this.Mob.Weapon.BulletAttributes.Clone();
                attributes.GoOverFurniture = true;
                attributes.SpeedInCoord = 4000;
                attributes.RangeInCoord = 40 * Tools.CellSizeInCoord;
                IntVector2 from = player.Position + new IntVector2(i, -20) * Tools.CellSizeInCoord;
                IntVector2 to = player.Position + new IntVector2(i, +20) * Tools.CellSizeInCoord;
                var trajectory = BulletLinearTrajectory.NewToTarget(from, to, attributes);
                var bullet = new Bullet(this.Level, attributes, trajectory);
                this.Level.Add(bullet);
            }
        }
        
        #endregion RainFire

        #region Bullet spiral

        private int spiraltick = 0;
        protected void ContinueSpiralFire(int angusSpeedRatio)
        {
            this.spiraltick++;
            if (this.spiraltick % 10 == 0)
            {
                int angus = this.spiraltick * angusSpeedRatio / 100;
                if (this.GetLifePercent() > 75)
                {
                    fireLinear(angus);
                }
                else if (this.GetLifePercent() > 50)
                {
                    fireLinear(angus);
                    fireLinear(angus + 180);
                }
                else if (this.GetLifePercent() > 25)
                {
                    fireLinear(angus);
                    fireLinear(angus + 120);
                    fireLinear(angus + 240);
                }
                else
                {
                    fireLinear(angus);
                    fireLinear(angus + 90);
                    fireLinear(angus + 180);
                    fireLinear(angus + 270);
                }
            }
        }

        protected void fireLinear(int angus)
        {
            IntVector2 bulletDirection = Weapon.RotateCoord(new IntVector2(255, 0), angus);
            IntVector2 gunPosition = Tools.CenterCharacter(this.Mob.Position) + bulletDirection * Tools.CellSizeInCoord / 256;
            Bullet bullet = new Bullet(Level, this.Mob.Weapon.BulletAttributes, new BulletLinearTrajectory(gunPosition, bulletDirection, this.Mob.Weapon.BulletAttributes, this.Mob.Weapon.BulletAttributes.RangeInCoord));
            this.Level.Add(bullet);
        }

        #endregion Bullet spiral

        #region Fire ground

        private Dictionary<IntVector2, FireGround> fireCells = new Dictionary<IntVector2, FireGround>();

        protected IEnumerable castFireAllGround(int nbTick = 4 * 60)
        {
            //yield return null;

            foreach (FireGround fireGround in this.fireCells.Values)
            {
                fireGround.isWarning = true;
            }
            for (int i = 0; i < 2 * 60; i++)
            {
                this.TryActivateClearShoot();
                yield return null;
            }
            this.Level.Music.BossMagic.Play();
            fireAllGroundNoCasting(nbTick);
        }

        protected void fireAllGroundNoCasting(int nbTick)
        {

            foreach (FireGround fireGround in this.fireCells.Values)
            {
                fireGround.Burn(nbTick);
            }
        }

        protected void tryBurnGroundUnderBoss()
        {
            foreach (IntVector2 around in Tools.AroundPoints)
            {
                tryBurnGroundUnder(this.Mob.Position + around * 3 * Tools.QuaterCellSizeInCoord);
            }
        }

        protected void tryBurnGroundUnder(IntVector2 position)
        {
            IntVector2 burnCell = Tools.CenterCharacterCell(position);
            if (this.fireCells.ContainsKey(burnCell))
            {
                this.fireCells[burnCell].Burn();
                return;
            }
            if (this.Level.Land.HasBlock(burnCell))
            {
                return;
            }
            FireGround fireGround = new FireGround(this.Level, burnCell);
            this.fireCells.Add(fireGround.Position, fireGround);
            this.Level.Add(fireGround);
        }


        private class FireGround : IDrawable
        {
            private static TextureRect coldImage = TextureRect.NewGround(9, 1);
            private static TextureRect burnImage = TextureRect.NewGround(10, 1);

            public readonly Level level;
            public readonly IntVector2 Position;
            public int BurnUntil;
            public bool isWarning = false;



            public FireGround(Level level, IntVector2 position)
            {
                this.level = level;
                this.Position = position;
                this.Burn();
            }

            public void Burn(int nbTick = 4 * 60)
            {
                this.isWarning = false;
                this.BurnUntil = this.level.Tick + nbTick;
            }

            public bool Isburning()
            {
                return this.level.Tick < this.BurnUntil;
            }
            public void Draw(Screen screen)
            {
                TextureRect img = coldImage;
                Color color = Color.White;
                if (this.level.Tick < this.BurnUntil)
                {
                    img = burnImage;
                }
                else if (this.isWarning)
                {
                    color = Color.Tomato;
                }
                screen.Draw64(img, this.Position, color);
            }
        }


        private void updateFireGrounds()
        {

            foreach (FireGround fireGround in this.fireCells.Values)
            {
                if (fireGround.Isburning())
                {
                    foreach (Player p in this.Level.GetAlivePlayers())
                    {
                        if (Tools.CenterCharacterCell(p.Position) == fireGround.Position)
                        {
                            p.HitBy(this.Mob.Weapon.BulletAttributes, -1);
                        }
                    }
                }
            }
        }



        #endregion Fire ground
        #region FireGroundBullet


        private HashSet<FireGroundBullet> fireGroundBulletList = new HashSet<FireGroundBullet>();


        protected void FireGroundBurnBullet()
        {

            foreach (Player p in this.Level.GetAlivePlayers())
            {
                this.Level.Music.MobBasicFire.Play();
                var bullet = new FireGroundBullet(this, this.Mob.Position, Tools.CenterCharacterCell(p.Position));
                this.Level.Add(bullet);
                this.fireGroundBulletList.Add(bullet);
            }
        }

        private void updateFireGroundBullets()
        {
            foreach (FireGroundBullet item in this.fireGroundBulletList.ToArray())
            {
                item.Update();
            }
        }

        private class FireGroundBullet : IDrawable
        {
            public readonly BossAI boss;
            public readonly IntVector2 initialPosition;
            public readonly IntVector2 target;

            private static TextureRect Image = new TextureRect(TextureRect.SpriteFile.TilesetBullet32, new Rectangle(33, 33, 32, 32));
            private int tick = 0;
            private IntVector2 position;
            private IntVector2 delta;
            private readonly int endTick;
            public FireGroundBullet(BossAI boss, IntVector2 position, IntVector2 target)
            {
                this.boss = boss;
                this.initialPosition = Tools.CenterCharacter(position);
                this.target = Tools.CenterCharacter(target);
                this.position = this.initialPosition;
                this.delta = IntVector2.Delta(this.position, this.target);
                this.endTick = 1 + delta.GetDistance() / 512;
            }

            public void Draw(Screen screen)
            {
                screen.Draw64(Image, this.position - 2 * new IntVector2(1, 1) * Tools.QuaterCellSizeInCoord, Color.White);
            }

            public void Update()
            {
                this.tick++;
                if (this.tick > this.endTick)
                {
                    this.explode();
                }
                this.position = this.initialPosition + this.tick * this.delta / this.endTick;
            }
            private void explode()
            {
                this.boss.Level.Remove(this);
                this.boss.fireGroundBulletList.Remove(this);
                foreach (IntVector2 cell in Tools.AroundPoints)
                {
                    this.boss.tryBurnGroundUnder(Tools.FloorToCell(this.target) + cell * Tools.CellSizeInCoord);
                }
            }
        }

        #endregion FireGroundBullet

       

    }
}
