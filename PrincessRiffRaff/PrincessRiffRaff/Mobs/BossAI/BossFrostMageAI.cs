﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using Microsoft.Xna.Framework;

namespace PrincessRiffRaff
{
    public class BossFrostMageAI : BossAI
    {       
        protected override IEnumerable BossLive()
        {
            yield return null;           
            FixedPathFlyingMobTrajectory trajectory = FixedPathFlyingMobTrajectory.NewRectangleTrajectory(this.Mob, this.BossZone.Position, 
                new Rectangle(-4,-4,8,8));
            Cooldown coolDown = new Cooldown(1, 35, 3, 10 * 60);
            while (this.GetLifePercent() > 60)
            {
                yield return null;
                CoroutineCpu.Current.Go(trajectory.QuarterCellMove(60));
                if (coolDown.TryStartCooldown(this.Level.Tick, false))
                {
                    createTornados(8);
                }
            }
            this.Level.Music.BossRoar.Play();
            coolDown = new Cooldown(1, 35, 3, 4 * 60);
            while (this.GetLifePercent() > 30)
            {
                yield return null;
                CoroutineCpu.Current.Go(trajectory.QuarterCellMove(40));
                if (coolDown.TryStartCooldown(this.Level.Tick, false))
                {
                    createTornados(16);
                }
            }
            this.Level.Music.BossRoar.Play();
            coolDown = new Cooldown(1, 35);
            while (!this.Mob.IsDead)
            {
                yield return null;
                CoroutineCpu.Current.Go(trajectory.QuarterCellMove(20));
                if (coolDown.TryStartCooldown(this.Level.Tick, false))
                {
                    createTornados(32);
                }
            }
        }

        private void createTornados(int minNumber)
        {
            this.createTornado();
            for (int i = 0; i < 3; i++)
            {
                if (this.popedMobList.Count < minNumber)
                {
                    this.createTornado();
                }
            }
        }

        private void createTornado()
        {
            this.Level.Music.BossMagic.Play();
            Mob mob = MobTypeHelper.NewMob(this.Level, ItemType.MOB_TORNADO);
            mob.Position = this.Mob.Position + Tools.RandPickOne(Tools.NeighborgPoints) * 4 * Tools.QuaterCellSizeInCoord;
            mob.VunerableOnlyTo = DamageTag.MobBombAoe;
            mob.Life = 1;///One bomb kill them
            mob.IsSpleeping = false;
            this.Level.Add(mob);
            this.popedMobList.Add(mob);
        }

        protected override void OnEachTick()
        {
            base.OnEachTick();
            this.Mob.Weapon.TryActivate();
        }
        public override void OnDying()
        {
            base.OnDying();
            this.resetPopedMobs();
        }
    }
}
