﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

namespace PrincessRiffRaff
{
    public class BossWormOctopusAI : BossAI
    {
        
        protected override IEnumerable BossLive()
        {
            List<Mob> tentacles = new List<Mob>();
            Cooldown popTentacleCooldown = new Cooldown(1, 60 * 5);
            while (!this.Mob.IsDead)
            {
                yield return null;
                this.Mob.Weapon.TryActivate();
                if (popTentacleCooldown.TryStartCooldown(this.Level.Tick, this.GetLifePercent() < 40))
                {
                    tentacles.RemoveAll(mob => mob.IsDead);
                    int maxTentacles;
                    if (this.GetLifePercent() > 75)
                    {
                        maxTentacles = 2;
                    }
                    else if (this.GetLifePercent() > 50)
                    {
                        maxTentacles = 4;
                    }
                    else if (this.GetLifePercent() > 25)
                    {
                        maxTentacles = 6;
                    }
                    else
                    {
                        maxTentacles = 8;
                    }
                    if (tentacles.Count < maxTentacles)
                    {
                        Mob tentacle = this.PopTentacle();
                        tentacles.Add(tentacle);
                        this.Level.Music.BossMagic.Play();
                    }
                }
            }
        }


        protected Mob PopTentacle()
        {
            IntVector2 position = new IntVector2(4 + Tools.Rand.Next(3), Tools.Rand.Next(0, 6));
            if (Tools.RandomWin(2))
            {
                position = new IntVector2(position.Y, position.X);
            }
            if (Tools.RandomWin(2))
            {
                position = new IntVector2(position.X, 1 - position.Y);
            }
            if (Tools.RandomWin(2))
            {
                position = new IntVector2(1 - position.X, position.Y);
            }

            Mob mob = MobTypeHelper.NewMob(this.Level, ItemType.MOB_JELLY_SPOT);
            mob.Position = this.BossZone.Position + position * Tools.CellSizeInCoord;
            mob.Image = TextureRect.NewMob(1 + Tools.Rand.Next(6), 2);
            mob.Life = mob.MaxLife = 2 * mob.Life;
            mob.IsSpleeping = false;
            this.Level.Add(mob);
            this.popedMobList.Add(mob);
            return mob;
        }
       
    }
}
