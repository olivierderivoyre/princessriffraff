﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

namespace PrincessRiffRaff
{
    public class BossFrogAI : BossAI
    {

        private const int arenaRadius = 5;
        protected override IEnumerable BossLive()
        {
            Cooldown cooldown = new Cooldown(1, 60 * 5);

            DiamondMobTrajectory trajectory = new DiamondMobTrajectory(this.Mob, this.BossZone.Position, arenaRadius);
            while (!this.Mob.IsDead)
            {
                yield return null;
                if (cooldown.TryStartCooldown(this.Level.Tick, this.GetLifePercent() < 30))
                {                    
                    this.PopMosquitoMob(this.BossZone.Position);
                }
                this.Mob.Weapon.TryActivate();
                if (this.GetLifePercent() < 55)
                {
                    int cellDuration = this.GetLifePercent() > 30 ? 80 : 40;
                    yield return CoroutineCpu.Current.Go(trajectory.QuarterCellMove(cellDuration));
                }
            }
        }

        protected void PopMosquitoMob(IntVector2 arenaCenter)
        {
            this.Level.Music.BossMagic.Play();
            int nbMuscito = 1;
            if (this.GetLifePercent() < 25)
            {
                nbMuscito = 4;
            }
            else if (this.GetLifePercent() < 50)
            {
                nbMuscito = 3;
            }
            else if (this.GetLifePercent() < 75)
            {
                nbMuscito = 2;
            }
            for (int i = 0; i < nbMuscito; i++)
            {
                Mob mob = MobTypeHelper.NewMob(this.Level, ItemType.MOB_MOSQUITO);
                mob.Position = arenaCenter + Tools.RandPickOne(Tools.AroundPoints) * (arenaRadius + 4) * Tools.CellSizeInCoord;
                mob.IsSpleeping = false;
                this.Level.Add(mob);
                this.popedMobList.Add(mob);
            }
        }

        public override void OnDying()
        {
            base.OnDying();
            this.resetPopedMobs();
        }
    }
}
