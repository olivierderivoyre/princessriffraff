﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using Microsoft.Xna.Framework;

namespace PrincessRiffRaff
{
    public class BossFireDjinnAI : BossAI
    {
      
        protected override IEnumerable BossLive()
        {
            yield return null;

            FixedPathFlyingMobTrajectory trajectory = FixedPathFlyingMobTrajectory.NewRectangleTrajectory(this.Mob, this.BossZone.Position,
                new Rectangle(-5, -5, 10, 10));
            Cooldown cooldownFire = new Cooldown(1, 3 * 60);
            cooldownFire.StartCooldown(this.Level.Tick);           
            Cooldown cooldownFireGround = new Cooldown(1, 17 * 60);
            cooldownFireGround.StartCooldown(this.Level.Tick);
            Cooldown cooldownRepopMob = new Cooldown(1, 31 * 60);
            cooldownRepopMob.StartCooldown(this.Level.Tick);

            Mob topMob = PopHorizontalEye(true);
            Mob downpMob = PopHorizontalEye(false);
            while (!this.Mob.IsDead)
            {
                yield return null;
                TryActivateClearShoot();
                CoroutineCpu.Current.Go(trajectory.QuarterCellMove(60));

                if (cooldownFire.TryStartCooldown(this.Level.Tick, false))
                {
                    this.FireGroundBurnBullet();
                    cooldownFire.StartCooldown(this.Level.Tick);
                }
                if (cooldownFireGround.TryStartCooldown(this.Level.Tick, false))
                {
                    yield return CoroutineCpu.Current.Go(this.castFireAllGround(this.GetLifePercent() < 35 ? 60 * 60 : 4 * 60 ));
                    cooldownFireGround.StartCooldown(this.Level.Tick);
                }
                if (cooldownRepopMob.TryStartCooldown(this.Level.Tick, false))
                {
                    if (topMob.IsDead)
                    {
                        this.Level.Music.BossMagic.Play();
                        topMob = PopHorizontalEye(true);
                    }
                    if (downpMob.IsDead)
                    {
                        this.Level.Music.BossMagic.Play();
                        downpMob = PopHorizontalEye(false);
                    }
                }
            }
        }

        protected Mob PopHorizontalEye(bool top)
        {
            Mob mob = MobTypeHelper.NewMob(this.Level, ItemType.MOB_EYE_HORIZONTAL);
            mob.Position = this.BossZone.Position + new IntVector2(0, top ? -7 : 7) * Tools.CellSizeInCoord;
            this.Level.Add(mob);
            this.popedMobList.Add(mob);
            return mob;
        }


    }
}
