﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

namespace PrincessRiffRaff
{
    public class BossJellyfishAI : BossAI
    {

        public static readonly TextureRect JellyfishImg = TextureRect.NewMob(7, 1);
        private static readonly TextureRect BlueSmokeImg = TextureRect.NewMob(8, 1);
       
        private const int arenaRadius = 4;
        protected override IEnumerable BossLive()
        {
            BossPhases phases = new BossPhases(this);
            phases.AddPhase(p => p.BossLifePercent < 80);
            phases.AddPhase(p => p.BossLifePercent < 60);
            phases.AddPhase(p => p.BossLifePercent < 40);
            phases.AddPhase(p => p.BossLifePercent < 30);
            phases.AddPhase(p => p.BossLifePercent < 20);
            phases.AddPhase(p => p.BossLifePercent < 10);
            
            int lastMagicTime = this.Level.Tick;
            int magicPeriod = 60 * 16;
            DiamondMobTrajectory trajectory = new DiamondMobTrajectory(this.Mob, this.BossZone.Position, arenaRadius);
            while (!this.Mob.IsDead)
            {                
                this.Mob.Weapon.TryActivate();
                int cellDuration = this.GetLifePercent() > 75 ? 70 : this.GetLifePercent() > 30 ? 50 : 30;
                yield return CoroutineCpu.Current.Go(trajectory.QuarterCellMove(cellDuration));
                if (this.Level.Tick - lastMagicTime > magicPeriod)
                {
                    this.Level.Music.BossMagic.Play();
                    this.Mob.Image = BlueSmokeImg;
                    yield return CoroutineCpu.Current.GoSleep(90);
                    int previousLife = this.Mob.Life;
                    for (int i = 0; i < 60 * 10; i++)
                    {
                        yield return null;
                        this.Mob.Weapon.TryActivate();
                        if (previousLife != this.Mob.Life)
                        {
                            previousLife = this.Mob.Life;
                            for (int x = 0; x < 20; x++)
                            {
                                this.Mob.Weapon.ForceFire();
                            }
                        }
                    }
                    lastMagicTime = this.Level.Tick;
                    this.Mob.Image = JellyfishImg;
                    
                }
                if (phases.TryChangePhase())
                {
                    magicPeriod = 60 * Math.Max(1, 16 - phases.CurrentPhaseIndex * 3);
                }
            }
        }


        protected override void OnEachTick()
        {
            base.OnEachTick();

            this.ContinueSpiralFire(20);
        }
        
    }
}
