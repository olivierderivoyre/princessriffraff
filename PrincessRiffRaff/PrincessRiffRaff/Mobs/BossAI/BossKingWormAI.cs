﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

namespace PrincessRiffRaff
{
    public class BossKingWormAI : BossAI
    {

        protected override IEnumerable BossLive()
        {
            
            yield return CoroutineCpu.Current.Go(this.FireWhile(90));
            yield return CoroutineCpu.Current.Go(this.PopMob(3));
            yield return CoroutineCpu.Current.Go(this.WaitOrLiving(5 * 60, 85));         

            yield return CoroutineCpu.Current.Go(this.FireWhile(75));
            yield return CoroutineCpu.Current.Go(this.PopMob(6));
            yield return CoroutineCpu.Current.Go(this.WaitOrLiving(5 * 60, 70));         

            yield return CoroutineCpu.Current.Go(this.FireWhile(50));
            yield return CoroutineCpu.Current.Go(this.PopMob(9));           
            yield return CoroutineCpu.Current.Go(this.WaitOrLiving(2*60, 45));
    
            yield return CoroutineCpu.Current.Go(this.FireWhile(35));                       
            while (!this.Mob.IsDead)
            {                
                yield return CoroutineCpu.Current.Go(this.PopMob(12));               
                for (int i = 0; i < 6 * 60; i++)
                {
                    this.Mob.Weapon.TryActivate();                
                    yield return null;
                }              
                yield return null;
            }
        }

        protected IEnumerable PopMob(int nb)
        {
            yield return null;
            if (this.popedMobList.Count(m => !m.IsDead) > 50)
            {
                yield return CoroutineCpu.Current.GoSleep(60);            
                yield break ;
            }
            this.Level.Music.BossMagic.Play();
            for (int i = 0; i < nb; i++)
            {
                Mob mob = MobTypeHelper.NewMob(this.Level, ItemType.MOB_WORM);
                mob.Position = this.Mob.Position + Tools.RandPickOne(Tools.AroundPoints) * Tools.CellSizeInCoord;
                this.Level.Add(mob);
                this.popedMobList.Add(mob);
            }
        }
    }
}
