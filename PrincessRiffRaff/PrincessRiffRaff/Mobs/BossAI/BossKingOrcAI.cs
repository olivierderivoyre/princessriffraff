﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

namespace PrincessRiffRaff
{
    public class BossKingOrcAI : BossAI
    {

        protected override IEnumerable BossLive()
        {            
            yield return CoroutineCpu.Current.Go(this.FireWhile(90));            
            yield return CoroutineCpu.Current.Go(this.ShortAoe());
            yield return CoroutineCpu.Current.Go(this.WaitOrLiving(5 * 60, 85));         

            yield return CoroutineCpu.Current.Go(this.FireWhile(75));
            yield return CoroutineCpu.Current.Go(this.FarAoe());
            yield return CoroutineCpu.Current.Go(this.WaitOrLiving(5 * 60, 70));         

            yield return CoroutineCpu.Current.Go(this.FireWhile(50));
            yield return CoroutineCpu.Current.Go(this.ShortAoe());           
            yield return CoroutineCpu.Current.Go(this.WaitOrLiving(2*60, 45));
    
            yield return CoroutineCpu.Current.Go(this.FireWhile(35));                       
            while (!this.Mob.IsDead)
            {
                if (Tools.RandomWin(2))
                {
                    yield return CoroutineCpu.Current.Go(this.ShortAoe());
                }
                else
                {
                    yield return CoroutineCpu.Current.Go(this.FarAoe());
                }
                for (int i = 0; i < 4 * 60; i++)
                {
                    this.Mob.Weapon.TryActivate();                
                    yield return null;
                }              
                yield return null;
            }
        }


        protected IEnumerable ShortAoe()
        {
            yield return null;
            this.Level.Music.BossMagic.Play();            
            BulletAttributes attributes = this.Mob.Weapon.BulletAttributes.Clone();
            attributes.GoOverFurniture = true;
            for (int i = 0; i < 8; i++)
            {
                var trajectory = new BulletSpiralTrajectory(Tools.CenterCharacter(this.Mob.Position), 4 * Tools.CellSizeInCoord, 360* i /8);
                var bullet = new Bullet(this.Level, attributes, trajectory);
                this.Level.Add(bullet);
            }           
        }

        protected IEnumerable FarAoe()
        {
            yield return null;
            this.Level.Music.BossMagic.Play();            
            BulletAttributes attributes = this.Mob.Weapon.BulletAttributes.Clone();
            attributes.GoOverFurniture = true;
            for (int i = 0; i < 100; i++)
            {
                var trajectory = new BulletIncomingSpiralTrajectory(Tools.CenterCharacter(this.Mob.Position), Tools.Rand.Next(10, 16) * Tools.QuaterCellSizeInCoord);
                var bullet = new Bullet(this.Level, attributes, trajectory);
                this.Level.Add(bullet);
            }
           

        }
    }
}
