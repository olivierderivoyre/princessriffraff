﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

namespace PrincessRiffRaff
{
    public class BossWaterElementalAI : BossAI
    {

        private const int arenaRadius = 5;
        protected override IEnumerable BossLive()
        {

            DiamondMobTrajectory trajectory = new DiamondMobTrajectory(this.Mob, this.BossZone.Position, arenaRadius);
            Mob turtle = this.PopTurtleBombMob(this.BossZone.Position);
            while (!this.Mob.IsDead)
            {
                yield return null;
                if (turtle.IsDead)
                {
                    this.Level.Music.BossMagic.Play();
                    yield return CoroutineCpu.Current.GoSleep(15);
                    turtle = this.PopTurtleBombMob(this.BossZone.Position);
                }
                this.Mob.Weapon.TryActivate();
                if (this.GetLifePercent() < 55)
                {
                    int cellDuration = this.GetLifePercent() > 30 ? 80 : 40;
                    yield return CoroutineCpu.Current.Go(trajectory.QuarterCellMove(cellDuration));
                }
            }
        }

        protected Mob PopTurtleBombMob(IntVector2 arenaCenter)
        {
            Mob mob = MobTypeHelper.NewMob(this.Level, ItemType.MOB_TURTLE_BOMB);
            mob.Position = arenaCenter + Tools.RandPickOne(Tools.AroundPoints) * (arenaRadius - 1) * Tools.CellSizeInCoord;
            this.Level.Add(mob);
            this.popedMobList.Add(mob);
            return mob;
        }
    }
}
