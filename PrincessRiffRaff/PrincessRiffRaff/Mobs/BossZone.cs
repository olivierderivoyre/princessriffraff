﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PrincessRiffRaff
{
    public class BossZone
    {
        public IntVector2 Position;
        public Level Level;
        public Mob Boss;
        public BossType? BossType;

        public void Update()
        {
            if (this.Boss == null)
            {

                var alivePlayers = this.Level.GetAlivePlayers().ToList();

                int count = GetNbPlayerAround(alivePlayers, 3);
                if (count == 0)
                {
                    return;
                }
                ///2nd player mustn't be not too far
                if (GetNbPlayerAround(alivePlayers, 6) == alivePlayers.Count)
                {
                    this.createBoss();
                }
            }
            else if (this.Boss.IsFinished)
            {
            }
        }

        private int GetNbPlayerAround(List<Player> alivePlayers, int radius)
        {
            IntVector2 topLeft = this.Position - new IntVector2(radius, radius) * Tools.CellSizeInCoord;
            return alivePlayers.Count(p =>
                p.Position.IsInside(topLeft,
                (radius + 1 + radius) * Tools.CellSizeInCoord,
                (radius + 1 + radius) * Tools.CellSizeInCoord));
        }

        private void createBoss()
        {
            this.Boss = BossTypeHelper.CreateBoss(this.Level, this.BossType);
            this.Boss.Position = this.Position;
            if (this.Boss.MobAI is BossAI)
            {
                ((BossAI)this.Boss.MobAI).BossZone = this;

            }
            this.Level.Add(this.Boss);
        }

        public void ResetFight()
        {
            if (this.Boss != null && !this.Boss.IsDead)
            {
                if (this.Boss.MobAI != null)
                {
                    this.Boss.MobAI.ResetFight();
                }
                this.Level.Remove(this.Boss);
                this.Boss = null;
            }
        }

       
    
    }
}
