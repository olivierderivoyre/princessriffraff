﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PrincessRiffRaff
{
    /// <summary>
    /// For the Score ate the end of the level
    /// </summary>
    public class PlayerStats
    {
        public int DamageDone;
        public int MobKilled;
        public int Death;
        public int DamageTaken;
    }
}
