﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using System.Diagnostics;

namespace PrincessRiffRaff
{
    public sealed class Player : Character
    {
        public InputController InputController;
        private int walkTickCount = 0;
        private IntVector2 lastOrientation = IntVector2.Zero;
        private FixedTargetMoveAnimation quaterCellMoveAnimation;
        public readonly bool IsPlayer1;
        public Mob targetMob;
        public bool NeedToPressStartToPlay;
        public int NeedToPressStartToPlayTickDelay = 60;
        public bool IsActive { get { return !this.NeedToPressStartToPlay && !this.IsDead; } }
        public Weapon[] Weapons = new Weapon[4];
        private bool wasFiringOnLastTurn = false;
        public PlayerStats PlayerStats = new PlayerStats();

        private int ResurectionCounter = 0;
        public bool IsResurecting = false;
        public const int QuaterHearthLife = 5;

        public Player(InputController inputController, bool isPlayer1)
        {
            this.InputController = inputController;
            this.IsPlayer1 = isPlayer1;
            this.NeedToPressStartToPlay = !isPlayer1;
            this.Life = this.MaxLife = 2 * 4 * Player.QuaterHearthLife;
            this.HitInvulnerableDuration = 40;
        }



        public static Rectangle GetSpriteSource(IntVector2 lastMovedirection, int walkTickCount)
        {
            int directionIndex = 0;
            if (lastMovedirection.X < 0)
            {
                directionIndex = 1;
            }
            else if (lastMovedirection.X > 0)
            {
                directionIndex = 2;
            }
            else if (lastMovedirection.Y < 0)
            {
                directionIndex = 3;
            }
            int walkTickIndex = 0;
            if (walkTickCount != 0)
            {
                walkTickIndex = (walkTickCount % 4);
            }
            return new Rectangle(walkTickIndex * 64, directionIndex * 64, 64, 64);
        }

        private IntVector2 getSpriteLookingDirection()
        {
            if (this.LastMovedirection.X != 0)
            {
                return new IntVector2(this.LastMovedirection.X, 0);
            }
            else
            {
                return new IntVector2(0, this.LastMovedirection.Y);
            }
        }
        public Color GetLifeRedAlertIndicator()
        {
            if (this.IsDead)
            {
                return Color.Red;
            }
            if (this.Life < 3 * Player.QuaterHearthLife)
            {
                if ((this.Level.Tick / 10) % 2 == 0)
                {
                    return Color.Red;
                }
                else
                {
                    return Color.White;
                }
            }
            else if (this.Life < 5 * Player.QuaterHearthLife)
            {
                if ((this.Level.Tick / 20) % 2 == 0)
                {
                    return Color.Red;
                }
                else
                {
                    return Color.White;
                }
            }
            return Color.White; ;
        }

        public void Draw(Screen screen)
        {
            if (this.NeedToPressStartToPlay)
            {
                return;
            }
            Rectangle source = Player.GetSpriteSource(this.LastMovedirection, this.walkTickCount);
            Texture2D spriteTexture = getTexture2D(screen.Ressource);
            if (this.IsDead)
            {
                Color color = Color.White;
                if (this.IsResurecting)
                {
                    if ((this.Level.Tick / 8) % 2 == 0)
                    {
                        color = Color.Yellow;
                    }
                    else
                    {
                        color = Color.Green;
                    }
                }
                screen.Draw64(screen.Ressource.TilesetGround, this.Position, new Rectangle(260, 0, 64, 64), color);
                return;
            }
            else///Alive
            {
                bool hasJustBeenHit = this.Level.Tick - this.LastBeenHitTick < 5;
                Color color = this.GetLifeRedAlertIndicator();
                if (hasJustBeenHit)
                {
                    color = Color.Red;
                }
                if (color == Color.White && this.IsEnraged())
                {
                    color = Color.Gray;
                }
                screen.Draw64(spriteTexture, this.Position, source, color);
                if (this.CurrentInvocation != null)
                {
                    this.CurrentInvocation.Draw(screen, this);
                }
            }

        }

        public Texture2D getTexture2D(Ressource ressource)
        {
            Texture2D spriteTexture = this.IsPlayer1 ? ressource.TilesetPlayer1 : ressource.TilesetPlayer2;

            return spriteTexture;
        }

        public void Update(InputState inputState, InputState previousInputState)
        {
            if (this.Level.Tick < 30)
            {
                return;
            }
            if (this.NeedToPressStartToPlay)
            {
                if (this.Level.Tick < this.NeedToPressStartToPlayTickDelay)
                {
                    return;///Press enter does not count
                }
                if (this.InputController.HasAnyActionButtonPressed(inputState))
                {
                    if (!inputState.Keyboard.IsKeyDown(Keys.Enter))///I press enter to validate menus => I do not wants the 2nd player in this case.
                    {
                        this.NeedToPressStartToPlay = false;
                        this.Position = this.IsPlayer1 ? this.Level.player2.Position : this.Level.player1.Position;
                    }
                }
                return;
            }
            if (this.IsDead)
            {
                this.TryResurect(this.Level.HasAliveFriendlyUnitOver(this.Position));
                return;
            }

            if (this.quaterCellMoveAnimation == null)
            {
                if (this.canMove())
                {

                    IntVector2 direction = this.InputController.GetMoveDirection(inputState);
                    if (direction == IntVector2.Zero)
                    {
                        this.walkTickCount = 0;
                    }
                    else
                    {
                        this.LastMovedirection = direction;
                        this.lastOrientation = IntVector2.Zero;
                        IntVector2 constrainedDirection = this.Level.LimitPlayerMove(this.Position, direction);
                        if (constrainedDirection == IntVector2.Zero)
                        {
                            this.walkTickCount = 0;
                        }
                        else
                        {
                            this.quaterCellMoveAnimation = FixedTargetMoveAnimation.CreateQuarterCellMove(this.Position, constrainedDirection, 24);
                            this.walkTickCount++;
                        }
                    }
                }
            }
            if (this.quaterCellMoveAnimation != null)
            {
                this.Position = this.quaterCellMoveAnimation.GetUpdatedPosition();
                if (this.quaterCellMoveAnimation.IsFinish())
                {
                    this.quaterCellMoveAnimation = null;
                    this.Level.TriggerPlayerMoved(this);
                }
            }

            Weapon weapon = this.getPressedWeapon(inputState);
            if (weapon != null)
            {
                if (!this.wasFiringOnLastTurn || this.targetMob == null || this.targetMob.IsDead)
                {
                    this.targetMob = this.selectTargetMob();
                }
                weapon.TryActivate();
            }
            this.wasFiringOnLastTurn = weapon != null;
            Invocation.PlayerPressing(this, weapon);
        }

        private Weapon getPressedWeapon(InputState inputState)
        {
            for (int i = this.InputController.ButtonPressedTaskList.Length - 1; i >= 0; i--)
            {
                if (this.InputController.ButtonPressedTaskList[i](inputState))
                {
                    Weapon weapon = this.Weapons[i];
                    if (weapon != null)
                    {
                        return weapon;
                    }
                }
            }
            return null;
        }
        private bool canMove()
        {
            if (this.CurrentInvocation != null && this.CurrentInvocation.IsInvoking)
            {
                return false;
            }
            return true;
        }

        private int getMobFocusScore(Mob mob, IntVector2 lookingDirection)
        {
            int malus = 0;
            IntVector2 delta = IntVector2.Delta(this.Position, mob.Position);
            int distance = delta.GetDistance() / Tools.QuaterCellSizeInCoord;
            malus += distance;
            if (distance > 6)///Sniper mode when no emergency
            {
                if (lookingDirection.Y != 0)
                {
                    if (Math.Sign(delta.Y) != Math.Sign(lookingDirection.Y))
                    {
                        malus += 100;
                    }
                    else if (Math.Abs(delta.X) > Math.Abs(delta.Y))
                    {
                        malus += 20;
                    }
                }
                if (lookingDirection.X != 0)
                {
                    if (Math.Sign(delta.X) != Math.Sign(lookingDirection.X))
                    {
                        malus += 100;
                    }
                    else if (Math.Abs(delta.Y) > Math.Abs(delta.X))
                    {
                        malus += 20;
                    }
                }
            }
            if (!this.Level.HasClearShoot(this.Position, mob.Position, null))
            {
                malus += 1000;
            }
            if (mob.VunerableOnlyTo == DamageTag.MobBombAoe)
            {
                malus += 50;
            }
            return malus;
        }


        private Mob selectTargetMob()
        {
            IntVector2 lookingDirection = this.getSpriteLookingDirection();
            int minScore = 500;
            Mob best = null;
            foreach (Mob mob in this.Level.GetMobsInsideScreen())
            {
                int score = getMobFocusScore(mob, lookingDirection);
                if (score < minScore)
                {
                    minScore = score;
                    best = mob;
                }
            }
            return best;
        }

        public void TryResurect(bool isResurecting)
        {
            this.IsResurecting = isResurecting;
            if (isResurecting)
            {
                this.ResurectionCounter++;
                if (this.ResurectionCounter > 60 * 3)
                {
                    this.ResurectThanksToTheOtherPlayer();
                }
            }
            else
            {
                this.ResurectionCounter = Math.Max(0, this.ResurectionCounter - 2);
            }
        }
        public void ResurectThanksToTheOtherPlayer()
        {
            this.quaterCellMoveAnimation = null;
            this.ResurectionCounter = 0;
            this.Life = this.MaxLife / 2;
            this.IsDead = false;
        }
        public void ResurectOnLastSave(IntVector2 position)
        {
            this.Position = position;
            this.quaterCellMoveAnimation = null;
            this.ResurectionCounter = 0;
            this.Life = this.MaxLife;
            this.IsDead = false;
            foreach (Weapon weapon in this.Weapons.Where(w => w != null))
            {
                weapon.ResetOnResurrect();
            }
        }


    }
}
