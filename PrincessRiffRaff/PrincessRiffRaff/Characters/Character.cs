﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;

namespace PrincessRiffRaff
{
    public abstract class Character
    {
        public Level Level;
        public IntVector2 Position;
        public bool IsDead;
        public IntVector2 LastMovedirection = new IntVector2(0, 1);
        public int Life;
        public int MaxLife = 80;
        public int LastBeenHitTick = -100;
        public int HitInvulnerableDuration = 0;
        public Invocation CurrentInvocation;
        public DamageTag VunerableOnlyTo = DamageTag.None;
        public int EndEnrageTick = -1;
        private int lastBombExplosionIdHit = -1;


        public Character GetTargetCharacter()
        {
            if (this is Player)
            {
                return ((Player)this).targetMob;
            }
            else if (this is Mob)
            {
                return ((Mob)this).TargetPlayer;
            }
            throw new NotImplementedException();
        }

        public IntVector2 GetWeaponDirection()
        {
            Character target = this.GetTargetCharacter();
            if (target != null)
            {
                return Bullet.GetBulletDirection(this.Position, target.Position);
            }
            if ((this is Mob) || this.LastMovedirection == IntVector2.Zero)///Mob does not have direction
            {
                return Tools.RandPickOne(Tools.NeighborgPoints) * Tools.PixelSizeInCoord;
            }
            return this.LastMovedirection * Tools.PixelSizeInCoord;
        }

        public bool IsVulnerableTo(BulletAttributes bulletAttributes)
        {
            if (this.VunerableOnlyTo == DamageTag.None)
            {
                return true;
            }

            if ((bulletAttributes.DamageTag & this.VunerableOnlyTo) != 0)
            {
                return true;
            }
            return false;
        }

        public void HitBy(BulletAttributes bulletAttributes, int explosionId)
        {

            if (this.Level.Tick - this.LastBeenHitTick < this.HitInvulnerableDuration)
            {
                return;
            }
            if (!this.IsVulnerableTo(bulletAttributes))
            {
                return;
            }

            this.LastBeenHitTick = this.Level.Tick;
            if (this is Player)
            {                
                this.Level.Music.PlayerHit.Play();
            }
            if (this is Mob)
            {
                Mob mob = (Mob)this;
                if (mob.IsSpleeping)
                {
                    mob.IsSpleeping = false;
                    mob.TargetPlayer = bulletAttributes.SourceCharacter as Player;
                }

            }

            int damage = bulletAttributes.Damage;
            if (explosionId != -1)
            {
                if (explosionId <= this.lastBombExplosionIdHit)
                {
                    damage = bulletAttributes.Damage / 4;
                }
                else
                {
                    this.lastBombExplosionIdHit = explosionId;
                }
            }
            int damageDone = Math.Min(this.Life, damage);
            bool isKillShoot = false;
            this.Life = Math.Max(0, this.Life - damage);
            if (this.Life == 0 && !this.IsDead)
            {
                this.IsDead = true;
                isKillShoot = true;
            }
            if (this is Mob)
            {
                Player sourcePlayer = bulletAttributes.SourceCharacter as Player;
                if (sourcePlayer == null)
                {
                    sourcePlayer = bulletAttributes.TargetPlayer;///Bomb case
                }
                if (sourcePlayer != null)
                {
                    sourcePlayer.PlayerStats.DamageDone += damageDone;
                    if (isKillShoot)
                    {
                        sourcePlayer.PlayerStats.MobKilled++;
                    }
                }
            }
            if (this is Player)
            {
                ((Player)this).PlayerStats.DamageTaken += damageDone;
                if (isKillShoot)
                {
                    ((Player)this).PlayerStats.Death++;
                }

            }
        }



        public bool IsEnraged()
        {
            return this.Level.Tick - this.EndEnrageTick < 0;
        }

        public void PlaySoundOnScreen(SoundEffectInstance sound)
        {
            if (sound == null)
            {
                return;
            }
            if (this.Level.IsIsInsideScreen(this.Position))
            {
                sound.Play();
            }
        }
    }
}
