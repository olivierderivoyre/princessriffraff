using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using System.Diagnostics;
using System.IO;
using System.Xml.Serialization;
using log4net;

namespace PrincessRiffRaff
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class PrincessRiffRaffGame : Microsoft.Xna.Framework.Game
    {
		private static ILog logger = LogManager.GetLogger(typeof(PrincessRiffRaffGame));

        private GraphicsDeviceManager graphics;
        private SpriteBatch spriteBatch;
        private SpriteBatch spriteBatchInterfaceLayer;

        public LevelsMapMenu LevelsMapMenu;
        public YouAreDeadMenu YouAreDeadMenu;
        public YouHaveWinMenu YouHaveWinMenu;
        public PauseMenu PauseMenu;
        public IntroMenu IntroMenu;

        private InterfaceLayer interfaceLayer;

        private Screen screen;
        private Ressource ressource;
        public Music Music;
        public InputController Player1InputController;
        public InputController Player2InputController;
        private Player player1;
        private Player player2;
        private Level level;
        private int fpsCounter = 0;
        private int fpsLastShowTickCount = Environment.TickCount;
        private InputState previousInputState;
        private int fps = 60;
        private List<ComLevel> campaignLevelList;
        private int campaignProgression = 0;
        private List<ComLevel> myLevelList;
        private int myLevelProgression = 0;
        private GameModeEnum GameMode;
        public enum GameModeEnum { Free, Campaign, MyLevel };

        public static string DebugLevelFileName;
        public static bool DebugIncludeTestMap;

        public PrincessRiffRaffGame()
        {
            this.graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
            this.Window.AllowUserResizing = true;
            this.Exiting += DigCraftFightGame_Exiting;
            //this.mainMenu = new MainMenu(this);
            this.campaignProgression = Settings.Default.CampaignProgression;
            this.myLevelProgression = Settings.Default.MyLevelProgression;
        }

       

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            base.Initialize();
        }


        private static XmlSerializer serializer = new XmlSerializer(typeof(ComLevel));

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            this.spriteBatch = new SpriteBatch(GraphicsDevice);
            this.spriteBatchInterfaceLayer = new SpriteBatch(GraphicsDevice);
            this.ressource = new Ressource(this.Content);
            this.Music = new Music(this.ressource);
            this.Player1InputController = new InputController(PlayerIndex.One);
            this.Player2InputController = new InputController(PlayerIndex.Two);


            
            this.campaignLevelList = this.Load(Path.Combine(this.Content.RootDirectory, "Level"));
            this.myLevelList = this.Load(Tools.GetPlayerLevelDirectory());

            if (string.IsNullOrEmpty(PrincessRiffRaffGame.DebugLevelFileName))
            {
                this.IntroMenu = new IntroMenu(this);
                //this.LevelsMapMenu = new LevelsMapMenu(this, comLevelList, this.progression);
            }
            else
            {
                ComLevel comLevel;
                using (var stream = File.OpenRead(PrincessRiffRaffGame.DebugLevelFileName))
                {
                    comLevel = (ComLevel)serializer.Deserialize(stream);
                }
                campaignLevelList.Add(comLevel);
                this.StartLevel(comLevel, -1);
            }           
            this.previousInputState = InputState.GetState();
        }

        private List<ComLevel> Load(string levelDir)
        {            
            List<ComLevel> comLevelList = new List<ComLevel>();
            if (!Directory.Exists(levelDir))
            {
                return comLevelList;
            }
            foreach (string file in Directory.GetFiles(levelDir, "*.xml").OrderBy(s => s))
            {
                ComLevel comLevel;
                using (var stream = File.OpenRead(file))
                {
                    comLevel = (ComLevel)serializer.Deserialize(stream);
                }
                if (comLevel.LevelAttributes == null)
                {
                    comLevel.LevelAttributes = new LevelAttributes();
                }
                if (comLevel.LevelAttributes.LevelName == null)
                {
                    comLevel.LevelAttributes.LevelName = "";
                }
                if (!PrincessRiffRaffGame.DebugIncludeTestMap)
                {
                    if (comLevel.LevelAttributes.LevelName.ToUpper().Contains("TEST"))
                    {
                        continue;
                    }
                }
                
                comLevelList.Add(comLevel);
            }
            return comLevelList;
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {

        }

        public void StartLevel(ComLevel comLevel, int levelIndex)
        {
			logger.Info("Start level: " + comLevel.LevelAttributes.LevelName + " (" + this.GameMode+ ")");
            this.screen = new Screen(this.spriteBatch, this.ressource, new IntVector2(0, 0), this.Window.ClientBounds.Width, this.Window.ClientBounds.Height);

            this.player1 = new Player(this.Player1InputController, true);
            this.player2 = new Player(this.Player2InputController, false);


            this.level = new Level(this, this.screen, player1, player2, comLevel);
            this.level.LevelIndex = levelIndex;
           
            this.player1.Position = this.level.PlayerResurectPosition;
            this.player2.Position = this.player1.Position + new IntVector2(2, 0) * Tools.CellSizeInCoord;
            this.screen.CenterOn(this.player1.Position);
            this.interfaceLayer = new InterfaceLayer(this.level);
        }

        public void LevelWin()
        {
			logger.Info("Level win");
            if (this.GameMode == GameModeEnum.Campaign)
            {
                if (this.level.LevelIndex > this.campaignProgression)
                {
                    this.campaignProgression = this.level.LevelIndex;
                    Settings.Default.CampaignProgression = this.campaignProgression;
                    Settings.Default.Save();
                }
            }
            else if (this.GameMode == GameModeEnum.MyLevel)
            {
                if (this.level.LevelIndex > this.myLevelProgression)
                {
                    this.myLevelProgression = this.level.LevelIndex;
                    Settings.Default.MyLevelProgression = this.myLevelProgression;
                    Settings.Default.Save();
                }
            }
        }

        public void ShowLevelsMap(GameModeEnum? mode)
        {
            logger.Info("ShowLevelsMap");
            this.PauseMenu = null;
            this.YouHaveWinMenu = null;
            if (mode != null)
            {
                this.GameMode = mode.Value;
            }
            if (this.GameMode == GameModeEnum.MyLevel)
            {
                this.LevelsMapMenu = new LevelsMapMenu(this, this.myLevelList, this.myLevelProgression);
            }
            else
            {
                this.LevelsMapMenu = new LevelsMapMenu(this, this.campaignLevelList, this.campaignProgression);
            }
        }

        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(new Color(0.13f, 0.13f, 0.13f));
            if (this.LevelsMapMenu != null || this.IntroMenu != null)///Show menu
            {
                this.spriteBatchInterfaceLayer.Begin(SpriteSortMode.Immediate, null, null, null, null, null, Tools.GetTransformMatrixForMyBound(800, 450, this.Window.ClientBounds.Width, this.Window.ClientBounds.Height));
                this.spriteBatchInterfaceLayer.Draw(this.ressource.TilesetInterface40, new Rectangle(0, 0, 800, 450), new Rectangle(1 * 41, 2 * 41, 40, 40), Color.White);

                if (this.IntroMenu != null)
                {
                    this.IntroMenu.Draw(this.spriteBatchInterfaceLayer, this.ressource);
                }
                else if (this.LevelsMapMenu != null)
                {
                    this.LevelsMapMenu.Draw(this.spriteBatchInterfaceLayer, this.ressource);
                }
                this.spriteBatchInterfaceLayer.End();
            }
            else///Show game
            {

                this.spriteBatch.Begin(SpriteSortMode.Immediate, null, null, null, null, null, Matrix.Identity);

                this.level.Draw(this.screen, gameTime);

                if (this.YouAreDeadMenu == null && this.YouHaveWinMenu == null && this.PauseMenu == null)
                {
                    this.interfaceLayer.Draw(this.spriteBatch, new IntVector2(this.Window.ClientBounds.Width, this.Window.ClientBounds.Height), screen.Ressource);
                }
                this.spriteBatch.End();
                if (this.YouAreDeadMenu != null)
                {
                    this.spriteBatchInterfaceLayer.Begin(SpriteSortMode.Immediate, null, null, null, null, null, Tools.GetTransformMatrixForMyBound(800, 450, this.Window.ClientBounds.Width, this.Window.ClientBounds.Height));
                    this.YouAreDeadMenu.Draw(this.spriteBatchInterfaceLayer, this.ressource);
                    this.spriteBatchInterfaceLayer.End();
                }
                if (this.YouHaveWinMenu != null)
                {
                    this.spriteBatchInterfaceLayer.Begin(SpriteSortMode.Immediate, null, null, null, null, null, Tools.GetTransformMatrixForMyBound(800, 450, this.Window.ClientBounds.Width, this.Window.ClientBounds.Height));
                    this.YouHaveWinMenu.Draw(this.spriteBatchInterfaceLayer, this.ressource);
                    this.spriteBatchInterfaceLayer.End();
                }
                if (this.PauseMenu != null)
                {
                    this.spriteBatchInterfaceLayer.Begin(SpriteSortMode.Immediate, null, null, null, null, null, Tools.GetTransformMatrixForMyBound(800, 450, this.Window.ClientBounds.Width, this.Window.ClientBounds.Height));
                    this.PauseMenu.Draw(this.spriteBatchInterfaceLayer, this.ressource);
                    this.spriteBatchInterfaceLayer.End();
                }
            }
            base.Draw(gameTime);
        }



        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            this.fpsCounter++;
            if (Environment.TickCount - this.fpsLastShowTickCount > 1500)
            {
                this.fps = this.fpsCounter * 1000 / (Environment.TickCount - this.fpsLastShowTickCount);

                this.fpsCounter = 0;
                this.fpsLastShowTickCount = Environment.TickCount;
            }

            InputState inputState = InputState.GetState();
            if (inputState.Keyboard.IsKeyDown(Keys.Escape) && inputState.Keyboard.IsKeyDown(Keys.LeftShift))
            {
                this.Exit();
                return;
            }
            if (inputState.Keyboard.IsKeyDown(Keys.F12) && inputState.Keyboard.IsKeyDown(Keys.LeftShift))
            {
                this.Exit();
                return;
            }
            if (inputState.Keyboard.IsKeyDown(Keys.LeftAlt) && inputState.Keyboard.IsKeyDown(Keys.Enter))
            {
                this.ToggleFullScreen();
            }

            if (this.IntroMenu != null)
            {
                this.IntroMenu.Update(inputState, this.previousInputState);
            }
            else if (this.LevelsMapMenu != null)
            {
                this.LevelsMapMenu.Update(inputState, this.previousInputState);
            }
            else if (this.YouAreDeadMenu != null)
            {
                this.YouAreDeadMenu.Update(inputState, this.previousInputState);
            }
            else if (this.YouHaveWinMenu != null)
            {
                this.YouHaveWinMenu.Update(inputState, this.previousInputState);
            }
            else if (this.PauseMenu != null)
            {
                this.PauseMenu.Update(inputState, this.previousInputState);
            }
            else if (this.level != null)
            {
                if (IsPausePressed(inputState, this.previousInputState))
                {
                    if (this.PauseMenu == null)
                    {
                        this.PauseMenu = new PauseMenu(this.level);
                    }
                    else
                    {
                        this.PauseMenu = null;
                    }
                }
                // this.interfaceLayer.Update(gameTime, inputState, this.previousInputState);
                if (this.PauseMenu == null)
                {
                    this.level.Update();
                    this.screen.Update(this.Window.ClientBounds.Width, this.Window.ClientBounds.Height, this.player1, this.player2);

                    this.player1.Update(inputState, this.previousInputState);
                    this.player2.Update(inputState, this.previousInputState);
                }
            }

            base.Update(gameTime);
            this.previousInputState = inputState;
        }

        private bool IsPausePressed(InputState inputState, InputState previousInputState)
        {
            return (this.player1.InputController.HasPausePressed(inputState) && !this.player1.InputController.HasPausePressed(previousInputState))
                                || (this.player2.InputController.HasPausePressed(inputState) && !this.player2.InputController.HasPausePressed(previousInputState));
        }

        public void ToggleFullScreen()
        {
            this.graphics.IsFullScreen = !this.graphics.IsFullScreen;
            if (this.graphics.IsFullScreen)
            {
                logger.Info("Switch to FullScreen mode");
                DisplayMode screen = GraphicsAdapter.DefaultAdapter.CurrentDisplayMode;
                this.graphics.PreferredBackBufferWidth = screen.Width;
                this.graphics.PreferredBackBufferHeight = screen.Height;
            }
            else
            {
				logger.Info("Switch to Windows mode");
                this.graphics.PreferredBackBufferWidth = 800;
                this.graphics.PreferredBackBufferHeight = 600;
            }
            this.graphics.ApplyChanges();
        }

        public void SetWindowSize(int width, int height)
        {
            this.graphics.PreferredBackBufferWidth = width;
            this.graphics.PreferredBackBufferHeight = height;
        }

        public void ResetProgression()
        {
            logger.Info("ResetProgression");
            this.Music.MenuClick.Play();
            this.campaignProgression = 0;
            this.myLevelProgression = 0;
            if (this.campaignLevelList.First().LevelAttributes.LevelName.ToUpper().Contains("TEST"))
            {
                this.campaignProgression = 1;
            }
        }

        private void DigCraftFightGame_Exiting(object sender, EventArgs e)
        {

        }


    }
}