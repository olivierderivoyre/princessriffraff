﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework;
using System.Diagnostics;
using log4net;


namespace PrincessRiffRaff
{
    public class InputState
    {
		private static ILog logger = LogManager.GetLogger(typeof(InputState));

        public readonly KeyboardState Keyboard;
        public readonly GamePadState[] PlayerGamePads;
		public readonly SharpDXJoystick.JoystickControler.JoystickState[] DirectInputJoystick;

		private static bool directInputInitialized = false;
		private static SharpDXJoystick.JoystickControler directInput;


        private InputState()
        {
            this.Keyboard = Microsoft.Xna.Framework.Input.Keyboard.GetState();
            this.PlayerGamePads = new []{
                GamePad.GetState(PlayerIndex.One),
                GamePad.GetState(PlayerIndex.Two)
            };
			if (InputState.directInput != null)
			{
				this.DirectInputJoystick = new[]{
					InputState.directInput.GetState(0),
					InputState.directInput.GetState(1)
				};
			}
			else///null object
			{
				this.DirectInputJoystick = new[]{
					new SharpDXJoystick.JoystickControler.JoystickState(),
					new SharpDXJoystick.JoystickControler.JoystickState()
				};
			}
        }
        public static InputState GetState()
        {
			if (!InputState.directInputInitialized)
			{
				InputState.directInputInitialized = true;
                if (!GamePad.GetCapabilities(PlayerIndex.One).IsConnected)
                {
                    try
                    {
                        InputState.directInput = new SharpDXJoystick.JoystickControler();
                    }
                    catch (Exception ex)
                    {
						logger.Warn(ex);
                    }
                }
			}

            return new InputState();
        }
    }
}
