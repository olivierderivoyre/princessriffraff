﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PrincessRiffRaff
{
    [System.Diagnostics.DebuggerStepThrough]
    public struct IntVector2
    {
        public static IntVector2 Zero = new IntVector2(0, 0);
        public int X;
        public int Y;

        public IntVector2(int x, int y)
        {
            this.X = x;
            this.Y = y;
        }
        public static bool operator ==(IntVector2 v1, IntVector2 v2)
        {
            return v1.X == v2.X && v1.Y == v2.Y;
        }
        public static bool operator !=(IntVector2 v1, IntVector2 v2)
        {
            return !(v1 == v2);
        }
        public override bool Equals(object obj)
        {
            return obj is IntVector2 && this == (IntVector2)obj;
        }
        public override int GetHashCode()
        {
            return (int)(this.X + 104729 * this.Y);
        }
        public static IntVector2 operator *(int i, IntVector2 v)
        {
            return new IntVector2(v.X * i, v.Y * i);
        }
        public static IntVector2 operator *( IntVector2 v, int i)
        {
            return new IntVector2(v.X * i, v.Y * i);
        }
        public static IntVector2 operator /(IntVector2 v, int i)
        {
            return new IntVector2(v.X / i, v.Y / i);
        }
        public static IntVector2 operator +(IntVector2 v1, IntVector2 v2)
        {
            return new IntVector2(v1.X + v2.X, v1.Y + v2.Y);
        }
        public static IntVector2 operator -(IntVector2 v1, IntVector2 v2)
        {
            return new IntVector2(v1.X - v2.X, v1.Y - v2.Y);
        }
        public static IntVector2 Delta(IntVector2 from, IntVector2 to)
		{			
			return new IntVector2(checked((int)(to.X - from.X)), checked((int)(to.Y - from.Y)));
		}

		public int GetDistance()
		{
			return Math.Max(Math.Abs(this.X), Math.Abs(this.Y));
		}
        public static int GetDistance(IntVector2 from, IntVector2 to)
        {
            return Delta(from, to).GetDistance();
        }
        public bool IsInside(IntVector2 topLeft, int width, int height)
        {
            return this.X >= topLeft.X && this.X < topLeft.X + width &&
                this.Y >= topLeft.Y && this.Y < topLeft.Y + height;
        }        
        public bool HitRectangle(IntVector2 topLeft, int width, int height, int hitRange)
        {
            return this.X + hitRange >= topLeft.X && this.X < topLeft.X + width + hitRange &&
                this.Y + hitRange >= topLeft.Y && this.Y < topLeft.Y + height + hitRange;
        }

        public override string ToString()
        {
            if (Math.Abs(this.X) < 10 && Math.Abs(this.Y) < 10)
            {
                return this.X + ", " + this.Y;
            }
            return (this.X / Tools.CellSizeInCoord) + "., " + (this.Y / Tools.CellSizeInCoord)+".";
        }
       
    }
}
