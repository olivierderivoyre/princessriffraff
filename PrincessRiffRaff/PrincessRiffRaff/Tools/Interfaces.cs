﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PrincessRiffRaff
{
	public interface IAlive
	{
		bool Update();	
		void Draw(Screen screen);
		IntVector2 Position { get; }
	}
}
