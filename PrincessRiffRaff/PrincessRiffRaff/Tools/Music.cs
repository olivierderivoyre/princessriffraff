﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Audio;

namespace PrincessRiffRaff
{
    public class Music
    {
        public readonly SoundEffectInstance BasicGun;
        public readonly SoundEffectInstance MobBasicFire;
        public readonly SoundEffectInstance BossRoar;
        public readonly SoundEffectInstance BossMagic;
        public readonly SoundEffectInstance BazookaFire;
        public readonly SoundEffectInstance BazookaExplode;
        public readonly SoundEffectInstance MobDying;
        public readonly SoundEffectInstance Drop;
        public readonly SoundEffectInstance BombExplosion;
        public readonly SoundEffectInstance PutBomb;
        public readonly SoundEffectInstance ScoreCounting;
        public readonly SoundEffectInstance MenuClick;
        public readonly SoundEffectInstance BossDying;
        public readonly SoundEffectInstance LevelWin;
        public readonly SoundEffectInstance MobSharpExplosion;
        public readonly SoundEffectInstance PlayerHit;

       
     
        public Music(Ressource ressource)
        {
            this.BasicGun = ressource.SoundLazer.CreateInstance();
            this.MobBasicFire = ressource.SoundBubble.CreateInstance();
            this.BossRoar = ressource.SoundRoar.CreateInstance();
            this.BazookaFire = ressource.SoundFireworkStart.CreateInstance();
            this.BazookaExplode = ressource.SoundFireworkEnd.CreateInstance();
            this.BossMagic = ressource.SoundMagicMissile.CreateInstance();
            this.MobDying = ressource.SoundPopCartoon.CreateInstance();
            this.Drop = ressource.SoundReloadGun.CreateInstance();
            this.BombExplosion = ressource.SoundBombExplosion.CreateInstance();
            this.PutBomb = ressource.SoundHouseKick.CreateInstance();
            this.ScoreCounting = ressource.SoundMiltonFruitMachine.CreateInstance();
            this.MenuClick = ressource.SoundPopCartoon.CreateInstance();
            this.BossDying = ressource.SoundDragonRoar.CreateInstance();
            this.LevelWin = ressource.SoundHappyJingle.CreateInstance();
            this.MobSharpExplosion = ressource.SoundSharpExplosion.CreateInstance();
            this.PlayerHit = ressource.SoundGirlGiggling.CreateInstance();
        }

       
    }
}
