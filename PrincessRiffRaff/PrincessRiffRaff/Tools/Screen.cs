﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace PrincessRiffRaff
{
	public class Screen
	{

		private readonly SpriteBatch spriteBatch;
		public readonly Ressource Ressource;

        public IntVector2 TopLeft;
		public int WidthCoord;
		public int HeightCoord;


		public Screen(SpriteBatch spriteBatch, Ressource ressource, IntVector2 center, int pxWidth, int pxHeight)
		{
			this.spriteBatch = spriteBatch;
			this.Ressource = ressource;
			this.WidthCoord = pxWidth * Tools.PixelSizeInCoord;
			this.HeightCoord = pxHeight * Tools.PixelSizeInCoord;
			this.CenterOn(center);
		}

        public void CenterOn(IntVector2 center)
		{
			this.TopLeft = Tools.FloorToCell(center + new IntVector2(-this.WidthCoord / 2, -this.HeightCoord / 2));
		}

        public IntVector2 GetCenter()
		{
			return this.TopLeft + new IntVector2(WidthCoord, HeightCoord) / 2;
		}

		public void Update(int pxWidth, int pxHeight, Player p1, Player p2)
		{
			this.WidthCoord = pxWidth * Tools.PixelSizeInCoord;
			this.HeightCoord = pxHeight * Tools.PixelSizeInCoord;
            IntVector2 newTopLeft;

            IntVector2 p1Position = p1.Position;
            IntVector2 p2Position = p2.Position;
			if (p1.IsActive && !p2.IsActive)
			{
				p2Position = p1Position;
			}
			if (!p1.IsActive && p2.IsActive)
			{
				p1Position = p2Position;
			}
			newTopLeft.X = getNewBorder(this.TopLeft.X, this.WidthCoord, p1Position.X, p2Position.X);
			newTopLeft.Y = getNewBorder(this.TopLeft.Y, this.HeightCoord, p1Position.Y, p2Position.Y);
			this.TopLeft = newTopLeft;
		}

		private static int getNewBorder(int currentBorder, int size, int p1, int p2)
		{
			int nbCellBorder = 3;
            if (size / Tools.CellSizeInCoord > 16)
            {
                nbCellBorder = 4;
            }
			int minP = Math.Min(p1, p2);
			int maxP = Math.Max(p1, p2);
			int borderForLeft = minP - nbCellBorder * Tools.CellSizeInCoord;
			///minBorderForRight + width == player + 2 cells
			int borderForRight = maxP + nbCellBorder * Tools.CellSizeInCoord + 1 * Tools.CellSizeInCoord - size;

			int minBorder = Math.Min(borderForLeft, borderForRight);
			int maxBorder = Math.Max(borderForLeft, borderForRight);
			bool mustChangeForMax = currentBorder >= maxBorder;
			bool mustChangeForMin = currentBorder <= minBorder;
			if (!mustChangeForMax && !mustChangeForMin)
			{
				return currentBorder;///No need to change
			}
			else if (mustChangeForMax && mustChangeForMin)
			{
				return currentBorder;///Players disagree on direction
			}
			else if (mustChangeForMax)
			{
				return maxBorder;
			}
			else
			{
				return minBorder;
			}
		}

		public IntVector2 LimitPlayerMove(IntVector2 newPosition, IntVector2 direction)
		{
			IntVector2 constrainedDirection = direction;
			if (limitPlayerMove(this.TopLeft.X, this.WidthCoord, newPosition.X, direction.X))
			{
				constrainedDirection.X = 0;
			}
			if (limitPlayerMove(this.TopLeft.Y, this.HeightCoord, newPosition.Y, direction.Y))
			{
				constrainedDirection.Y = 0;
			}
			return constrainedDirection;
		}

		private static bool limitPlayerMove(int screen, int size, int player, int move)
		{
			if (move < 0)
			{
				return player < screen;
			}
			if (move > 0)
			{
				return player + 1 * Tools.CellSizeInCoord > screen + size;
			}
			return false;
		}



		public void Draw(Texture2D texture, IntVector2 destinationWorld, Rectangle sourceRectangle, Color color)
		{
			Rectangle destinationScreen = new Rectangle(
				Tools.WorldCoordToPixel(destinationWorld.X - this.TopLeft.X),
				 Tools.WorldCoordToPixel(destinationWorld.Y - this.TopLeft.Y),
				 sourceRectangle.Width, sourceRectangle.Height);

			this.spriteBatch.Draw(texture, destinationScreen, sourceRectangle, color);
		}
		public void Draw32(TextureRect textureRect, IntVector2 destinationWorld, Color color)
		{
			DrawScale(textureRect.GetTileset(this.Ressource), destinationWorld, 32, textureRect.Rectangle, color);
		}
		public void Draw64(TextureRect textureRect, IntVector2 destinationWorld, Color color)
		{
			DrawScale(textureRect.GetTileset(this.Ressource), destinationWorld, 64, textureRect.Rectangle, color);
		}
        public void DrawScale(TextureRect textureRect, IntVector2 destinationWorld, int sizeInPx, Color color)
        {
            DrawScale(textureRect.GetTileset(this.Ressource), destinationWorld, sizeInPx, textureRect.Rectangle, color);
        }
		public void Draw64(Texture2D texture, IntVector2 destinationWorld, Rectangle sourceRectangle, Color color)
		{
			DrawScale(texture, destinationWorld, 64, sourceRectangle, color);
		}
		public void DrawScale(Texture2D texture, IntVector2 destinationWorld, int size, Rectangle sourceRectangle, Color color)
		{
			Rectangle destinationScreen = new Rectangle(
				Tools.WorldCoordToPixel(destinationWorld.X - this.TopLeft.X),
				 Tools.WorldCoordToPixel(destinationWorld.Y - this.TopLeft.Y),
				 size, size);

			this.spriteBatch.Draw(texture, destinationScreen, sourceRectangle, color);
		}
		public void DrawString(string text, IntVector2 destinationWorld, Color color)
		{
			Vector2 destinationScreen = new Vector2(
				Tools.WorldCoordToPixel(destinationWorld.X - this.TopLeft.X),
				 Tools.WorldCoordToPixel(destinationWorld.Y - this.TopLeft.Y));
			this.spriteBatch.DrawString(this.Ressource.Font, text, destinationScreen, color);

		}

        public bool IsInside(IntVector2 position)
        {
            return position.IsInside(this.TopLeft - new IntVector2(1, 1) * Tools.CellSizeInCoord,
                this.WidthCoord + 2 * Tools.CellSizeInCoord,
                this.HeightCoord + 2 * Tools.CellSizeInCoord);
        }
		public bool IsCharacterOutside(IntVector2 position)
		{
			return !position.IsInside(this.TopLeft - new IntVector2(1, 1) * Tools.CellSizeInCoord,
				this.WidthCoord + 2 * Tools.CellSizeInCoord,
				this.HeightCoord + 2 * Tools.CellSizeInCoord);
		}

		public void DrawIndicator(TextureRect textureRect, IntVector2 position, Color color)
		{
			if (!IsCharacterOutside(position))
			{
				return;
			}
			IntVector2? plot = null;
			IntVector2? xplot = this.getRadarCoordX(position);
			if (xplot != null)
			{
				plot = xplot;
			}
			else
			{
				IntVector2? yPlot = this.getRadarCoordY(position);
				if (yPlot != null)
				{
					plot = yPlot;
				}
			}
			if (plot != null)
			{
				this.DrawScale(this.Ressource.TilesetInterface40, plot.Value - new IntVector2(4,4) * Tools.PixelSizeInCoord, 24, new Rectangle(3 * 41, 2 * 41, 40, 40), color);
				this.DrawScale(textureRect.GetTileset(this.Ressource), plot.Value, 16, textureRect.Rectangle, Color.White);
			}
		}

		private IntVector2? getRadarCoordX(IntVector2 position)
		{	
			IntVector2 topLeft = this.TopLeft + new IntVector2(4,4) * Tools.PixelSizeInCoord;
			IntVector2 downRight = this.TopLeft + new IntVector2(this.WidthCoord, this.HeightCoord) - new IntVector2(4 + 16, 4 + 16) * Tools.PixelSizeInCoord;
			IntVector2 center = this.GetCenter();
			int xAxis;
			int centerCorectif = 1;
			if(position.X < topLeft.X)
			{
				xAxis = topLeft.X;
			} 
			else if(position.X > downRight.X)
			{
				xAxis = downRight.X;
				centerCorectif = -1;
			} else	{
				return null;
			}
			//  o
			//  |     |-------------|
			//  |_____|      .      |
			//        |-------------|
			//  w   xAxis  Center

			int radarYFromCenter = (position.Y - center.Y) * (topLeft.X - center.X) / (position.X - center.X);
			int radarYAbs = center.Y + centerCorectif * radarYFromCenter;
			if (radarYAbs < topLeft.Y || radarYAbs >= downRight.Y)
			{
				return null;
			}
			return new IntVector2(xAxis, radarYAbs);
		}

		private IntVector2? getRadarCoordY(IntVector2 position)
		{
			IntVector2 topLeft = this.TopLeft + new IntVector2(4, 4) * Tools.PixelSizeInCoord;
			IntVector2 downRight = this.TopLeft + new IntVector2(this.WidthCoord, this.HeightCoord) - new IntVector2(4 + 16, 4 + 16) * Tools.PixelSizeInCoord;
			IntVector2 center = this.GetCenter();
			int yAxis;
			int centerCorectif = 1;
			if (position.Y < topLeft.Y)
			{
				yAxis = topLeft.Y;
			}
			else if (position.Y > downRight.Y)
			{
				yAxis = downRight.Y;
				centerCorectif = -1;
			}
			else
			{
				return null;
			}
			int radarXFromCenter = (position.X - center.X) * (topLeft.Y - center.Y) / (position.Y - center.Y);
			int radarXAbs = center.X + centerCorectif * radarXFromCenter;
			if (radarXAbs < topLeft.X)
			{
				radarXAbs = topLeft.X;
			}
			if (radarXAbs >= downRight.X)
			{
				radarXAbs = downRight.X;
			}
			//if (radarXAbs < topLeft.X || radarXAbs >= downRight.X)
			//{
			//    return null;
			//}
			return new IntVector2(radarXAbs, yAxis);
		}
	}
}
