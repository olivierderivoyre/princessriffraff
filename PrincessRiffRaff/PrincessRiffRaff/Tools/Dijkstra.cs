﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PrincessRiffRaff
{
    public class Dijkstra
    {
        public static List<IntVector2> FindPath(bool[] map, int mapSize, IntVector2 start, HashSet<IntVector2> endList)
        {
            if (endList.Contains(start))
            {
                return new List<IntVector2> { start };
            }
            Dictionary<IntVector2, IntVector2?> previous = new Dictionary<IntVector2, IntVector2?>(mapSize * 4);

            PriorityQueue<IntVector2> nodesToExplore = new PriorityQueue<IntVector2>();
            nodesToExplore.Enqueue(start, 0);
            previous[start] = null;
            while (!nodesToExplore.IsEmpty())
            {
                int distanceFromSource;
                IntVector2 current = nodesToExplore.Dequeue(out distanceFromSource);
                foreach (var direction in new int[][] {
                    new int[]{0,1}, new int[]{1,0}, new int[]{-1,0}, new int[]{0,-1},
                    new int[]{1,1}, new int[]{-1,1}, new int[]{-1,-1}, new int[]{1,-1}
                })
                {
                    IntVector2 next = current + new IntVector2(direction[0], direction[1]);
                    if (!isInRange(mapSize, next))
                    {
                        continue;
                    }
                    if (!canGo(map, mapSize, next))
                    {
                        continue;
                    }
                    if (previous.ContainsKey(next))
                    {
                        continue;///already visited
                    }
                    bool isDiagonalMove = direction[0] != 0 && direction[1] != 0;
                    if (isDiagonalMove)
                    {
                        IntVector2 verticalNeighbour = current + new IntVector2(direction[0], 0);
                        IntVector2 horizontalNeighbour = current + new IntVector2(0, direction[1]);
                        if (!canGo(map, mapSize, verticalNeighbour) || !canGo(map, mapSize, horizontalNeighbour))
                        {
                            continue;
                        }
                    }

                    previous.Add(next, current);
                    if (endList.Contains(next))
                    {
                        return readPreviousPath(previous, mapSize, next);
                    }
                    else
                    {
                        nodesToExplore.Enqueue(next, distanceFromSource + (isDiagonalMove ? 14 : 10));
                    }
                }
            }
            return null;
        }



        public static List<IntVector2> FindPathMultipleSource(bool[] map, int mapSize, Dictionary<IntVector2, int> startList, IntVector2 end)
        {
            Dictionary<IntVector2, IntVector2?> previous = new Dictionary<IntVector2, IntVector2?>(mapSize * 4);

            PriorityQueue<IntVector2> nodesToExplore = new PriorityQueue<IntVector2>();
            foreach (IntVector2 start in startList.Keys)
            {
                nodesToExplore.Enqueue(start, startList[start]);
                previous[start] = null;
            }

            while (!nodesToExplore.IsEmpty())
            {
                int distanceFromSource;
                IntVector2 current = nodesToExplore.Dequeue(out distanceFromSource);
                foreach (var direction in new int[][] {
                    new int[]{0,1}, new int[]{1,0}, new int[]{-1,0}, new int[]{0,-1},
                    new int[]{1,1}, new int[]{-1,1}, new int[]{-1,-1}, new int[]{1,-1}
                })
                {
                    IntVector2 next = current + new IntVector2(direction[0], direction[1]);
                    if (!isInRange(mapSize, next))
                    {
                        continue;
                    }
                    if (!canGo(map, mapSize, next))
                    {
                        continue;
                    }
                    if (previous.ContainsKey(next))
                    {
                        continue;///already visited
                    }
                    bool isDiagonalMove = direction[0] != 0 && direction[1] != 0;
                    if (isDiagonalMove)
                    {
                        IntVector2 verticalNeighbour = current + new IntVector2(direction[0], 0);
                        IntVector2 horizontalNeighbour = current + new IntVector2(0, direction[1]);
                        if (!canGo(map, mapSize, verticalNeighbour) || !canGo(map, mapSize, horizontalNeighbour))
                        {
                            continue;
                        }
                    }

                    previous.Add(next, current);
                    if (end == next)
                    {
                        return readPreviousPath(previous, mapSize, next);
                    }
                    else
                    {
                        nodesToExplore.Enqueue(next, distanceFromSource + (isDiagonalMove ? 14 : 10));
                    }
                }
            }
            return null;
        }


        private static int getScore(IntVector2 border, IntVector2 target)
        {
            IntVector2 delta = IntVector2.Delta(border, target);
            return (int)(10 * Math.Sqrt(delta.X * delta.X + delta.Y * delta.Y));
        }
        /// <summary>
        /// Fast and stupid
        /// </summary>
        public static List<IntVector2> GetDirectionOf(bool[] map, int mapSize, IntVector2 start, IntVector2 target)
        {

            if (target.IsInside(IntVector2.Zero, mapSize, mapSize))
            {
                HashSet<IntVector2> endList = new HashSet<IntVector2>();
                endList.Add(target);
                return FindPath(map, mapSize, start, endList);
            }
            else
            {
                Dictionary<IntVector2, int> startList = new Dictionary<IntVector2, int>();
                for (int i = 0; i < mapSize; i++)
                {
                    IntVector2 border = new IntVector2(0, i);
                    startList[border] = getScore(border, target);
                }
                for (int i = 0; i < mapSize; i++)
                {
                    IntVector2 border = new IntVector2(mapSize - 1, i);
                    startList[border] = getScore(border, target);
                }
                for (int i = 0; i < mapSize; i++)
                {
                    IntVector2 border = new IntVector2(i, 0);
                    startList[border] = getScore(border, target);
                }
                for (int i = 0; i < mapSize; i++)
                {
                    IntVector2 border = new IntVector2(i, mapSize - 1);
                    startList[border] = getScore(border, target);
                }
                List<IntVector2> r = FindPathMultipleSource(map, mapSize, startList, start);
                if (r != null && r.Count != 0)
                {
                    r.Reverse();
                    r.RemoveAt(r.Count - 1);///Remove the border element since it can be a block
                }
                return r;
            }
        }

        private static int getRunAwayScore(IntVector2 border, IntVector2 fleeFrom)
        {
            IntVector2 delta = IntVector2.Delta(border, fleeFrom);
            int distance = (int)(10 * Math.Sqrt(delta.X * delta.X + delta.Y * delta.Y));
            return Math.Max(0, 10 * 32 - distance);
        }
        public static List<IntVector2> GetRunAwayDirection(bool[] map, int mapSize, IntVector2 start, IntVector2 fleeFrom)
        {


            Dictionary<IntVector2, int> startList = new Dictionary<IntVector2, int>();
            for (int i = 0; i < mapSize; i++)
            {
                IntVector2 border = new IntVector2(0, i);
                startList[border] = getRunAwayScore(border, fleeFrom);
            }
            for (int i = 0; i < mapSize; i++)
            {
                IntVector2 border = new IntVector2(mapSize - 1, i);
                startList[border] = getRunAwayScore(border, fleeFrom);
            }
            for (int i = 0; i < mapSize; i++)
            {
                IntVector2 border = new IntVector2(i, 0);
                startList[border] = getRunAwayScore(border, fleeFrom);
            }
            for (int i = 0; i < mapSize; i++)
            {
                IntVector2 border = new IntVector2(i, mapSize - 1);
                startList[border] = getRunAwayScore(border, fleeFrom);
            }
            List<IntVector2> r = FindPathMultipleSource(map, mapSize, startList, start);
            if (r != null && r.Count != 0)
            {
                r.Reverse();               
                r.RemoveAt(r.Count - 1);///Remove the border element since it can be a block
            }
            return r;
        }



        private static bool isInRange(int mapSize, IntVector2 cell)
        {
            if (cell.X < 0 || cell.X >= mapSize || cell.Y < 0 || cell.Y >= mapSize)
            {
                return false;
            }
            return true;
        }
        private static bool canGo(bool[] map, int mapSize, IntVector2 cell)
        {
            if (map[cell.Y * mapSize + cell.X])
            {
                return false;
            }
            return true;
        }
        private static List<IntVector2> readPreviousPath(Dictionary<IntVector2, IntVector2?> previous, int mapSize, IntVector2 end)
        {
            List<IntVector2> r = new List<IntVector2>();
            int i = 0;
            int max = mapSize * mapSize;
            IntVector2? current = end;
            while (current != null)
            {
                if (i++ > max)
                {
                    throw new Exception();
                }
                r.Add(current.Value);
                current = previous[current.Value];
            }
            r.Reverse();
            return r;
        }
    }
}
