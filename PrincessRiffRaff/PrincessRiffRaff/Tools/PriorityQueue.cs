﻿using System.Text;
using System;
using System.Diagnostics;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace PrincessRiffRaff
{

    public class PriorityQueue<T>
    {
        int total_size;
        SortedDictionary<int, Queue<T>> storage;

        public PriorityQueue()
        {
            this.storage = new SortedDictionary<int, Queue<T>>();
            this.total_size = 0;
        }

        public bool IsEmpty()
        {
            return (total_size == 0);
        }

        public T Dequeue(out int priority)
        {
            if (IsEmpty())
            {
                throw new Exception("Please check that priorityQueue is not empty before dequeing");
            }

            // foreach (Queue<T> q in storage.Values)
            foreach (var item in storage)
            {
                // we use a sorted dictionary
                if (item.Value.Count > 0)
                {
                    total_size--;
                    priority = item.Key;
                    T value = item.Value.Dequeue();
                    if (item.Value.Count == 0)
                    {
                        storage.Remove(item.Key);
                    }
                    return value;
                }
            }

            throw new ArgumentException();
        }

        // same as above, except for peek.

        public T Peek()
        {
            if (IsEmpty())
            {
                throw new ArgumentException("Please check that priorityQueue is not empty before peeking");
            }

            foreach (Queue<T> q in storage.Values)
            {
                if (q.Count > 0)
                {
                    return q.Peek();
                }
            }

            throw new ArgumentException();
        }

        public void Enqueue(T item, int prio)
        {
            if (!storage.ContainsKey(prio))
            {
                storage.Add(prio, new Queue<T>());
            }
            storage[prio].Enqueue(item);
            total_size++;

        }

        
        public T DequeueIfUnderPriority(int priority)
        {            
            // foreach (Queue<T> q in storage.Values)
            foreach (var item in storage)
            {
                if (item.Key > priority)
                {
                    break;
                }
                // we use a sorted dictionary
                if (item.Value.Count > 0)
                {
                    total_size--;
                    priority = item.Key;
                    T value = item.Value.Dequeue();
                    if (item.Value.Count == 0)
                    {
                        storage.Remove(item.Key);
                    }
                    return value;
                }
            }
            return default(T);
           
        }

    }


}

