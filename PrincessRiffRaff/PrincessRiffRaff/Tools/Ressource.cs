﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Audio;

namespace PrincessRiffRaff
{
    public class Ressource
    {
        public readonly SpriteFont Font;
        public readonly SpriteFont FontScore;
        public readonly SpriteFont FontTitle;

        public readonly Texture2D TilesetBullet32;
        public readonly Texture2D TilesetMob64;
        public readonly Texture2D TilesetGround;
        public readonly Texture2D TilesetItems32;
        public readonly Texture2D TilesetInterface40;
        public readonly Texture2D TilesetPlayer1;
        public readonly Texture2D TilesetPlayer2;
        public readonly Texture2D ImgPrincessRiffRaffLogo;



        public readonly SoundEffect SoundBubble;
        public readonly SoundEffect SoundLazer;
        public readonly SoundEffect SoundRoar;
        public readonly SoundEffect SoundFireworkStart;
        public readonly SoundEffect SoundFireworkEnd;
        public readonly SoundEffect SoundMagicMissile;
        public readonly SoundEffect SoundPopCartoon;
        public readonly SoundEffect SoundReloadGun;
        public readonly SoundEffect SoundBombExplosion;
        public readonly SoundEffect SoundHouseKick;
        public readonly SoundEffect SoundMiltonFruitMachine;
        public readonly SoundEffect SoundDragonRoar;
        public readonly SoundEffect SoundHappyJingle;
        public readonly SoundEffect SoundSharpExplosion;
        public readonly SoundEffect SoundGirlGiggling;

        public Dictionary<string, Texture2D> heroTexureList = new Dictionary<string, Texture2D>();

        public Ressource(ContentManager content)
        {
            this.Font = content.Load<SpriteFont>("Font/Font");
            this.FontScore = content.Load<SpriteFont>("Font/FontScore");
            this.FontTitle = content.Load<SpriteFont>("Font/FontTitle");

            this.TilesetBullet32 = content.Load<Texture2D>("Tileset/TilesetBullet32");
            this.TilesetMob64 = content.Load<Texture2D>("Tileset/TilesetMob64");
            this.TilesetGround = content.Load<Texture2D>("Tileset/TilesetGround64");
            this.TilesetItems32 = content.Load<Texture2D>("Tileset/TilesetItems32");
            this.TilesetInterface40 = content.Load<Texture2D>("Tileset/TilesetInterface40");
            this.TilesetPlayer1 = content.Load<Texture2D>("Sprite/harajuku4");
            this.TilesetPlayer2 = content.Load<Texture2D>("Sprite/moderngirl02");
            this.ImgPrincessRiffRaffLogo = content.Load<Texture2D>("Tileset/PrincessRiffRaffLogo");

            this.SoundBubble = content.Load<SoundEffect>("Sound/Bubble");
            this.SoundLazer = content.Load<SoundEffect>("Sound/Lazer");
            this.SoundRoar = content.Load<SoundEffect>("Sound/Roar");
            this.SoundFireworkStart = content.Load<SoundEffect>("Sound/FireworkStart");
            this.SoundFireworkEnd = content.Load<SoundEffect>("Sound/FireworkEnd");
            this.SoundMagicMissile = content.Load<SoundEffect>("Sound/MagicMissile");
            this.SoundPopCartoon = content.Load<SoundEffect>("Sound/PopCartoon");
            this.SoundReloadGun = content.Load<SoundEffect>("Sound/ReloadGun");
            this.SoundBombExplosion = content.Load<SoundEffect>("Sound/BombExplosion");
            this.SoundHouseKick = content.Load<SoundEffect>("Sound/HouseKick");
            this.SoundMiltonFruitMachine = content.Load<SoundEffect>("Sound/MiltonFruitMachine");
            this.SoundDragonRoar = content.Load<SoundEffect>("Sound/DragonRoar");
            this.SoundHappyJingle = content.Load<SoundEffect>("Sound/HappyJingle");
            this.SoundSharpExplosion = content.Load<SoundEffect>("Sound/SharpExplosion");
            this.SoundGirlGiggling = content.Load<SoundEffect>("Sound/GirlGiggling");

            
        }
    }
}
