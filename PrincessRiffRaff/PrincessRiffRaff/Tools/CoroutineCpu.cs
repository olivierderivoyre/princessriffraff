﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Diagnostics;
using log4net;

namespace PrincessRiffRaff
{
	/// <summary>
	/// Use and abuse of "yiel return" to do process only some action on each game tick.
	/// </summary>
	public class CoroutineCpu
	{
		private static ILog logger = LogManager.GetLogger(typeof(CoroutineCpu));

		private static CoroutineCpu current;
		public static CoroutineCpu Current { get { return current; } }

		private Stack<IEnumerator> coroutineStack = new Stack<IEnumerator>();

		public object Go(IEnumerable coroutine)
		{
			IEnumerator enumetor = coroutine.GetEnumerator();
			this.coroutineStack.Push(enumetor);					
			return null;
		}

		public void Update()
		{
			if (CoroutineCpu.current != null)
			{
				throw new Exception();
			}
			CoroutineCpu.current = this;
			try
			{
				while (true)
				{
					if (this.coroutineStack.Count == 0)
					{
						return;
					}
                    IEnumerator currentAction = this.coroutineStack.Peek();
                    Stopwatch watch = new Stopwatch();
                    watch.Start();
                    bool isCoroutineFinished = !currentAction.MoveNext();
                    watch.Stop();
                    if (watch.ElapsedMilliseconds > 50)
                    {
						logger.Debug("Slow: " + watch.ElapsedMilliseconds.ToString("0") + " ms");
                    }
					if (isCoroutineFinished)
					{
						if (this.coroutineStack.Count != 0)///Happens on Interrupt()
						{
							this.coroutineStack.Pop();
						}
					}
					else
					{
						return;
					}
				}
			}
			finally
			{
				CoroutineCpu.current = null;
			}
		}

		public bool IsIdle { get { return this.coroutineStack.Count == 0; } }

		public void Interrrupt()
		{
			this.coroutineStack.Clear();
		}

		public object GoSleep(int tickCount)
		{
			return Go(sleep(tickCount));
		}
		private IEnumerable sleep(int tickCount)
		{
			for (int i = 0; i < tickCount; i++)
			{
				yield return null;
			}
		}
	}
}
