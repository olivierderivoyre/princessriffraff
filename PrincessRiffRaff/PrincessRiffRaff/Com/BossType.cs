﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PrincessRiffRaff
{
    public enum BossType
    {
        TODO,
        BigEye, KingOrc, KingOfWorms, HairyBeholder, WaterElemental,
        FireElemental, FireDjinn, WormOctopus, Frog,
        PurpleBeholder, Jellyfish, FrostMage,
    };

    public static class BossTypeHelper
    {
        public static Mob CreateBoss(Level level, BossType? bossType)
        {
            Mob boss = new Mob();
            boss.Life = boss.MaxLife = 300;

            if (level != null && level.IsTwoPlayers())
            {
                boss.Life = boss.MaxLife = 2 * boss.MaxLife;
            }
            boss.CanSleep = boss.IsSpleeping = false;
            boss.Level = level;
            boss.Image = TextureRect.NewMob(0, 0);
            boss.IsBoss = true;
            boss.Weapon = WeaponTypeHelper.NewMobRandomGun(boss);
            if (bossType == null || bossType.Value == BossType.KingOrc)
            {
                boss.MobAI = new BossKingOrcAI();
                boss.Image = TextureRect.NewMob(3, 0);
            }
            else if (bossType.Value == BossType.BigEye)
            {
                boss.MobAI = new BossBigEyeAI();
                boss.Life = boss.MaxLife = 100;
                boss.Image = TextureRect.NewMob(6, 0);
            }
            else if (bossType.Value == BossType.KingOfWorms)
            {
                boss.MobAI = new BossKingWormAI();
                boss.Image = TextureRect.NewMob(5, 0);
            }
            else if (bossType.Value == BossType.HairyBeholder)
            {
                boss.MobAI = new BossHairyBeholderAI();
                boss.Image = TextureRect.NewMob(5, 1);
            }
            else if (bossType.Value == BossType.WaterElemental)
            {
                boss.MobAI = new BossWaterElementalAI();
                boss.Image = TextureRect.NewMob(9, 0);
                boss.Weapon = WeaponTypeHelper.NewMobBasicGun(boss);
                boss.VunerableOnlyTo = DamageTag.MobBombAoe;
                boss.Life = boss.MaxLife = 105;
            }
            else if (bossType.Value == BossType.PurpleBeholder)
            {
                boss.MobAI = new BossPurpleBeholderAI();
                boss.Image = TextureRect.NewMob(0, 1);
            }
            else if (bossType.Value == BossType.FireElemental)
            {
                boss.MobAI = new BossFireElementalAI();
                boss.Image = TextureRect.NewMob(2, 1);
                boss.Life = boss.MaxLife = (boss.MaxLife * 200 / 100);
            }
            else if (bossType.Value == BossType.FireDjinn)
            {
                boss.MobAI = new BossFireDjinnAI();
                boss.Image = TextureRect.NewMob(3, 1);
                boss.Life = boss.MaxLife = (boss.MaxLife * 200 / 100);
            }
            else if (bossType.Value == BossType.WormOctopus)
            {
                boss.MobAI = new BossWormOctopusAI();
                boss.Image = TextureRect.NewMob(0, 2);
                boss.Life = boss.MaxLife = (boss.MaxLife * 200 / 100);
            }
            else if (bossType.Value == BossType.Frog)
            {
                boss.MobAI = new BossFrogAI();
                boss.Image = TextureRect.NewMob(9, 2);
                boss.Life = boss.MaxLife = (boss.MaxLife * 200 / 100);
            }
            else if (bossType.Value == BossType.Jellyfish)
            {
                boss.MobAI = new BossJellyfishAI();
                boss.Image = BossJellyfishAI.JellyfishImg;
                boss.Life = boss.MaxLife = (boss.MaxLife * 200 / 100);
            }
            else if (bossType.Value == BossType.FrostMage)
            {
                boss.MobAI = new BossFrostMageAI();
                boss.Image = TextureRect.NewMob(9, 1);
                boss.Life = boss.MaxLife = (boss.MaxLife * 200 / 100);
            }
            if (boss.MobAI != null)
            {
                boss.MobAI.Level = level;
                boss.MobAI.Mob = boss;
            }
            else
            {
                boss.Life = boss.MaxLife = 200;///Boss is not yet finish, we are testing
            }
            return boss;
        }
    }
}
