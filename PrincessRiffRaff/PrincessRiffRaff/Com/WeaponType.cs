﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace PrincessRiffRaff
{
    public enum WeaponType {Default, None, BasicGun, Bazooka, AoeBomb, Potion }

    public class WeaponTypeHelper
    {
        public static Weapon NewWeapon(Player owner, WeaponType weaponType, WeaponType defaultWeaponType)
        {           
            WeaponType selectedWeaponType = weaponType;
            if (weaponType == WeaponType.Default)
            {
                selectedWeaponType = defaultWeaponType;
            }
            if (selectedWeaponType == WeaponType.None)
            {
                return null;
            }
            switch (selectedWeaponType)
            {
                case WeaponType.BasicGun: return NewBasicGun(owner);
                case WeaponType.Bazooka: return NewBazooka(owner);
                case WeaponType.AoeBomb: return NewAoeBomb(owner);
                case WeaponType.Potion :
                    if (owner == null || owner.IsPlayer1)
                    {
                        return NewPotionHeal(owner);
                    }
                    else
                    {
                        return NewPotionEnrage(owner);
                    }
                default: throw new NotImplementedException();
            }
        }

        public static Weapon NewMobBasicGun(Character owner)
        {
            if (owner.Level == null)
            {
                return null;///For LevelsMapMenu
            }
            var bulletAttributes = new BulletAttributes();
            bulletAttributes.RangeInCoord = 10 * Tools.CellSizeInCoord;
            bulletAttributes.Damage = 10;
            bulletAttributes.ExplosionRange = 0;
            bulletAttributes.SpeedInCoord = 500;
            bulletAttributes.StartSpeedInCoord = 50;
            bulletAttributes.TargetType = TargetType.Player;
            Rectangle sourceRectangle = new Rectangle(0, 0, 32, 32);
            bulletAttributes.TextureRect = new TextureRect(TextureRect.SpriteFile.TilesetBullet32, sourceRectangle);
            bulletAttributes.SourceCharacter = owner;

            Weapon weapon = new Weapon();
            weapon.BulletAttributes = bulletAttributes;
            weapon.Cooldown = new Cooldown(1, 180);
            weapon.FireSound = owner.Level.Music.MobBasicFire;
            weapon.Level = owner.Level;
            return weapon;
        }
        public static Weapon NewMobRandomGun(Character owner)
        {
            Weapon weapon = NewMobBasicGun(owner);
            if (weapon != null)
            {
                weapon.BulletAttributes.WeaponFireType = WeaponFireType.RandomLinear;
            }
            return weapon;
        }
        public static BulletAttributes NewMobAoeBomb(Character owner)
        {
            var bulletAttributes = newBombAttributes(owner);            
            return bulletAttributes;
        }

        private static BulletAttributes newBombAttributes(Character owner)
        {
            var bulletAttributes = new BulletAttributes();
            bulletAttributes.WeaponFireType = WeaponFireType.AoeBomb;
            bulletAttributes.Damage = 20;
            bulletAttributes.ExplosionRange = 9 * Tools.QuaterCellSizeInCoord;
            bulletAttributes.TargetType = TargetType.Monster | TargetType.Player;
            bulletAttributes.TextureRect = Bomb.Image;
            if (owner != null)
            {
                bulletAttributes.SoundExplosion = owner.Level.Music.BombExplosion;
            }
            bulletAttributes.SourceCharacter = owner;
            bulletAttributes.DamageTag = DamageTag.MobBombAoe;
            return bulletAttributes;
        }



        private static Weapon NewBasicGun(Character owner)
        {
            var bulletAttributes = new BulletAttributes();
           
            bulletAttributes.RangeInCoord = 8 * Tools.CellSizeInCoord;
            bulletAttributes.Damage = 3;
            bulletAttributes.ExplosionRange = 0;
            bulletAttributes.SpeedInCoord = 2000;
            bulletAttributes.TargetType = TargetType.Monster;
            Rectangle sourceRectangle = new Rectangle(0, 33, 32, 32);
            bulletAttributes.TextureRect = new TextureRect(TextureRect.SpriteFile.TilesetBullet32, sourceRectangle);
            bulletAttributes.SourceCharacter = owner;

            Weapon weapon = new Weapon();
            weapon.Icon = TextureRect.NewItems32(0, 1);
            weapon.BulletAttributes = bulletAttributes;
            weapon.Cooldown = new Cooldown(1, 15, 3, 60);
            if (owner != null)
            {
                weapon.FireSound = owner.Level.Music.BasicGun;
                weapon.Level = owner.Level;
            }
           
            return weapon;
        }

        private static Weapon NewBazooka(Character owner)
        {
            var bulletAttributes = new BulletAttributes();
            bulletAttributes.RangeInCoord = 7 * Tools.CellSizeInCoord;
            bulletAttributes.Damage = 15;
            bulletAttributes.ExplosionRange = 3 * Tools.QuaterCellSizeInCoord;
            bulletAttributes.SpeedInCoord = 2000;
            bulletAttributes.TargetType = TargetType.Monster;
            Rectangle sourceRectangle = new Rectangle(33, 0, 32, 32);
            bulletAttributes.TextureRect = new TextureRect(TextureRect.SpriteFile.TilesetBullet32, sourceRectangle);
            if (owner != null)
            {
                bulletAttributes.SoundExplosion = owner.Level.Music.BazookaExplode;
            }
            bulletAttributes.GoOverCharacter = true;
            bulletAttributes.SourceCharacter = owner;

            Weapon weapon = new Weapon();
            weapon.Icon = TextureRect.NewItems32(6, 1);
            weapon.BulletAttributes = bulletAttributes;
            weapon.Cooldown = new Cooldown(1, 30);
            weapon.IncantationDuration = 20;
            weapon.DefaultAmmoQuantity = weapon.AmmoQuantity = 30;           
            if (owner != null)
            {
                weapon.Level = owner.Level;
                weapon.FireSound = owner.Level.Music.BazookaFire;
            }           
            weapon.LimitRangeToTargetDistance = true;
            return weapon;
        }

        private static Weapon NewAoeBomb(Character owner)
        {
            //var bulletAttributes = new BulletAttributes();
            //bulletAttributes.WeaponFireType = WeaponFireType.AoeBomb;
            //bulletAttributes.Damage = 30;
            //bulletAttributes.ExplosionRange = 9 * Tools.QuaterCellSizeInCoord;
            //bulletAttributes.TargetType = TargetType.Monster | TargetType.Player;
            //bulletAttributes.TextureRect = Bomb.Image;
            //if (owner != null)
            //{
            //    bulletAttributes.SoundExplosion = owner.Level.Music.BombExplosion;
            //}
            //bulletAttributes.DamageTag = DamageTag.MobBombAoe;     
            //bulletAttributes.SourceCharacter = owner;
            var bulletAttributes = newBombAttributes(owner);
            Weapon weapon = new Weapon();
            weapon.Icon = Bomb.Image;
            weapon.BulletAttributes = bulletAttributes;
            weapon.Cooldown = new Cooldown(1, 30);            
            weapon.DefaultAmmoQuantity = weapon.AmmoQuantity = 15;
            if (owner != null)
            {
                weapon.Level = owner.Level;
                weapon.FireSound = owner.Level.Music.PutBomb;
            
            }
            return weapon;
        }

        private static Weapon NewPotionHeal(Character owner)
        {
            var bulletAttributes = new BulletAttributes();
            bulletAttributes.WeaponFireType = WeaponFireType.PotionHeal;                      
            bulletAttributes.SourceCharacter = owner;

            Weapon weapon = new Weapon();
            bulletAttributes.TextureRect = weapon.Icon = TextureRect.NewItems32(12, 1);
            weapon.BulletAttributes = bulletAttributes;
            weapon.Cooldown = new Cooldown(1, 10);
            weapon.DefaultAmmoQuantity = weapon.AmmoQuantity = 1;
            //weapon.IncantationDuration = 60;
            if (owner != null)
            {
                weapon.Level = owner.Level;
                weapon.FireSound = owner.Level.Music.Drop;
            }
            return weapon;
        }
        private static Weapon NewPotionEnrage(Character owner)
        {
            var bulletAttributes = new BulletAttributes();
            bulletAttributes.WeaponFireType = WeaponFireType.PotionEnrage;
            bulletAttributes.SourceCharacter = owner;

            Weapon weapon = new Weapon();
            bulletAttributes.TextureRect = weapon.Icon = TextureRect.NewItems32(13, 1);
            weapon.BulletAttributes = bulletAttributes;
            weapon.Cooldown = new Cooldown(1, 10);
            weapon.DefaultAmmoQuantity = weapon.AmmoQuantity = 1;
           // weapon.IncantationDuration = 60;
            if (owner != null)
            {
                weapon.Level = owner.Level;
                weapon.FireSound = owner.Level.Music.BossMagic;

            }
            return weapon;
        }
    }
}
