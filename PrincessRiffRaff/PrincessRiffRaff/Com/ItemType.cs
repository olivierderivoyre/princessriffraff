﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace PrincessRiffRaff
{
    public enum ItemType {
                  
        
        DELIMITER_CARPET,
       
		GROUND_WATER,
        GROUND_WATER_LIGHT,
        GROUND_SAND,
        GROUND_EARTH,
		GROUND_GRASS,
        GROUND_EARTH_LIGHT,
		GROUND_BAT,
        GROUND_WOOD,       
        GROUND_WATER_BRIDGE,
        GROUND_SWAMP,
        GROUND_SNOW,
        
        DOOR,
        DELIMITER_CARPET_END,

        DELIMITER_FURNITURE,        
        BLOCK_EARTH,
        BLOCK_BRICK_EARTH,
        BLOCK_STONE,
        BLOCK_WALL_STONE,

        ORE,
        TREE,
        APPLE_TREE_NO_FRUIT,
        APPLE_TREE_WITH_FRUIT,
        DEAD_TREE,
        PALM_TREE,
        FIR_TREE,
       
		WORKSTATION_SMALL_TABLE,
		WORKSTATION_SMALL_TABLE_WATER,
		WORKSTATION_TABLE,
		WORKSTATION_BOMB,
		WORKSTATION_ORE,
        BED,
        COMMODE,				
        DELIMITER_FURNITURE_END,


        DELIMITER_MOB,
        BAT,
        MOB_BAT,
        MOB_WORM,
        MOB_EYE_VERTICAL,
        MOB_EYE_HORIZONTAL,
        MOB_TURTLE_BOMB,
        MOB_BOMB_BEE,
        MOB_RAT,
        MOB_SKELETON_WIZARD,
        MOB_JELLY_SPOT,
        MOB_TORNADO,
        MOB_MOSQUITO,
        DELIMITER_MOB_END,

        ZONE_START,
        ZONE_BOSS,
        ZONE_SAVE,
        ZONE_NEW_HEARTH,
        ZONE_AMMO,
        


        
    };


    public static class ItemTypeHelper
    {
        public static TextureRect GetTextureRect(ItemType itemType)
        {

            switch (itemType)
            {
                ///Ground
                case ItemType.GROUND_EARTH: return TextureRect.NewGround(0, 0);
                case ItemType.GROUND_GRASS: return TextureRect.NewGround(1, 0);
                case ItemType.GROUND_WATER: return TextureRect.NewGround(2, 0);
                case ItemType.GROUND_BAT: return TextureRect.NewGround(3, 0);
                case ItemType.GROUND_WOOD: return TextureRect.NewGround(5, 0);
                case ItemType.GROUND_WATER_BRIDGE: return TextureRect.NewGround(6, 0);
                case ItemType.GROUND_WATER_LIGHT: return TextureRect.NewGround(8, 0);
                case ItemType.GROUND_SAND: return TextureRect.NewGround(9, 0);
                case ItemType.GROUND_SWAMP: return TextureRect.NewGround(12, 0);
                case ItemType.GROUND_SNOW: return TextureRect.NewGround(13, 0);
                case ItemType.BLOCK_EARTH: return TextureRect.NewGround(0, 1);
                case ItemType.TREE: return TextureRect.NewGround(1, 1);
                case ItemType.BLOCK_STONE: return TextureRect.NewGround(2, 1);
                case ItemType.WORKSTATION_TABLE: return TextureRect.NewGround(3, 1);
                case ItemType.GROUND_EARTH_LIGHT: return TextureRect.NewGround(11, 0);
                case ItemType.APPLE_TREE_NO_FRUIT: return TextureRect.NewGround(5, 1);
                case ItemType.APPLE_TREE_WITH_FRUIT: return TextureRect.NewGround(6, 1);
                case ItemType.DEAD_TREE: return TextureRect.NewGround(7, 1);
                case ItemType.PALM_TREE: return TextureRect.NewGround(8, 1);
                case ItemType.FIR_TREE: return TextureRect.NewGround(11, 1);
                case ItemType.BLOCK_BRICK_EARTH: return TextureRect.NewGround(0, 2);
                case ItemType.BLOCK_WALL_STONE: return TextureRect.NewGround(10, 0);                 
                case ItemType.BED: return TextureRect.NewGround(4, 2);
                case ItemType.COMMODE: return TextureRect.NewGround(5, 2);
                case ItemType.DOOR: return TextureRect.NewGround(6, 2);
                case ItemType.WORKSTATION_SMALL_TABLE: return TextureRect.NewGround(7, 2);
                case ItemType.WORKSTATION_SMALL_TABLE_WATER: return TextureRect.NewGround(0, 3);
                case ItemType.ORE: return TextureRect.NewGround(0, 4);
                case ItemType.WORKSTATION_ORE: return TextureRect.NewGround(0, 5);
                ///Mob
                case ItemType.MOB_BAT: return TextureRect.NewMob(2, 0);
                case ItemType.MOB_WORM: return TextureRect.NewMob(4, 0);
                case ItemType.MOB_EYE_HORIZONTAL: return TextureRect.NewMob(1, 1);
                case ItemType.MOB_EYE_VERTICAL: return TextureRect.NewMob(1, 1);
                case ItemType.MOB_TURTLE_BOMB: return TextureRect.NewMob(8, 0);
                case ItemType.MOB_BOMB_BEE: return TextureRect.NewMob(7, 0);
                case ItemType.MOB_RAT: return TextureRect.NewMob(0, 0);
                case ItemType.MOB_SKELETON_WIZARD: return TextureRect.NewMob(4, 1);
                case ItemType.MOB_JELLY_SPOT: return TextureRect.NewMob(6, 1);
                case ItemType.MOB_TORNADO: return TextureRect.NewMob(7, 2);
                case ItemType.MOB_MOSQUITO: return TextureRect.NewMob(8, 2);
                ///Zone
                case ItemType.ZONE_START: return new TextureRect(TextureRect.SpriteFile.Player1, new Rectangle(0, 0, 64, 64));
                case ItemType.ZONE_BOSS: return TextureRect.NewMob(3, 0);
                case ItemType.ZONE_SAVE: return TextureRect.NewItems32(11, 0);
                case ItemType.ZONE_NEW_HEARTH: return TextureRect.NewItems32(5, 6);
                case ItemType.ZONE_AMMO: return TextureRect.NewGround(1, 3);                          
            }

            return null;
        }



    }


}
