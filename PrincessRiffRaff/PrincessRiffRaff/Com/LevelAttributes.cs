﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PrincessRiffRaff
{
    [Serializable]
    public class LevelAttributes
    {
        public string LevelName { get; set; }
        public BossType BossType { get; set; }
        public WeaponType WeaponGun { get; set; }
        public WeaponType WeaponBazooka { get; set; }
        public WeaponType WeaponBomb { get; set; }
        public WeaponType WeaponSpecial { get; set; }

        public WeaponType LevelIsNewWeapon { get; set; }
    }
}
