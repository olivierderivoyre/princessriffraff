﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace PrincessRiffRaff
{
    public static class MobTypeHelper
    {
        public static Mob NewMob(Level level, ItemType itemType)
        {
            if (itemType <= ItemType.DELIMITER_MOB || itemType >= ItemType.DELIMITER_MOB_END)
            {
                throw new ArgumentException("Can not be mob: " + itemType);
            }
            Mob mob = new Mob();
            mob.MaxLife = mob.Life = 8;
            mob.Level = level;
            if (itemType == ItemType.MOB_BAT)
            {
                mob.MobAI = new BatAI();
                mob.Weapon = WeaponTypeHelper.NewMobBasicGun(mob);
            }
            if (itemType == ItemType.MOB_WORM)
            {
                mob.MobAI = new MobGoNearPlayerAI(128);
                mob.Weapon = WeaponTypeHelper.NewMobBasicGun(mob);
                mob.Weapon.BulletAttributes.SpeedInCoord = 300;
            }
            if (itemType == ItemType.MOB_EYE_HORIZONTAL || itemType == ItemType.MOB_EYE_VERTICAL)
            {
                mob.MobAI = new EyeAI(itemType == ItemType.MOB_EYE_HORIZONTAL);
                mob.Weapon = WeaponTypeHelper.NewMobBasicGun(mob);
                mob.Weapon.BulletAttributes.RangeInCoord = 40 * Tools.CellSizeInCoord;                
                mob.CanSleep = mob.IsSpleeping = false;
            }
            if (itemType == ItemType.MOB_TURTLE_BOMB)
            {
                mob.MobAI = new BombTurtleAI();
                mob.MaxLife = mob.Life = 20;
            }
            if (itemType == ItemType.MOB_BOMB_BEE)
            {
                mob.MobAI = new BeeAI();
                mob.CanSleep = mob.IsSpleeping = false;
            }
            if (itemType == ItemType.MOB_RAT)
            {
                mob.MobAI = new MobGoNearPlayerAI(30 + Tools.Rand.Next(30));
                mob.Weapon = WeaponTypeHelper.NewMobBasicGun(mob);
                mob.Weapon.BulletAttributes.SpeedInCoord = 1800;
                mob.Weapon.BulletAttributes.StartSpeedInCoord = 1800;
                mob.Weapon.BulletAttributes.RangeInCoord = 2 * Tools.CellSizeInCoord;
                mob.MaxLife = mob.Life = 30;
            }
            if (itemType == ItemType.MOB_SKELETON_WIZARD)
            {
                mob.MobAI = new SkeletonWizardAI();
                mob.Weapon = WeaponTypeHelper.NewMobBasicGun(mob);
                Rectangle sourceRectangle = new Rectangle(66, 0, 32, 32);
                mob.Weapon.BulletAttributes.TextureRect = new TextureRect(TextureRect.SpriteFile.TilesetBullet32, sourceRectangle);
                mob.Weapon.IncantationDuration = 90;
                mob.MaxLife = mob.Life = 30;
            }
            if (itemType == ItemType.MOB_JELLY_SPOT)
            {
                mob.MobAI = new JustFireAI();
                mob.Weapon = WeaponTypeHelper.NewMobRandomGun(mob);               
            }
            if (itemType == ItemType.MOB_TORNADO)
            {
                mob.MobAI = new TornadoAI();
                mob.Weapon = WeaponTypeHelper.NewMobBasicGun(mob);
                mob.MaxLife = mob.Life = 30;
                mob.CanSleep = mob.IsSpleeping = false;
            }
            if (itemType == ItemType.MOB_MOSQUITO)
            {
                mob.MobAI = new MosquitoAI();
                mob.Weapon = WeaponTypeHelper.NewMobRandomGun(mob);
                mob.MaxLife = mob.Life = 1;
            }
            if (mob.MobAI == null)
            {
                return null;
            }
            mob.MobAI.Level = level;
            mob.MobAI.Mob = mob;            
            mob.Image = ItemTypeHelper.GetTextureRect(itemType);            
            return mob;
        }
    }
}
