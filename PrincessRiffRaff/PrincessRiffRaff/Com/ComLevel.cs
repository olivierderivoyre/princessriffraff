﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PrincessRiffRaff
{
    [Serializable]
    public class ComLevel
    {
        public LevelAttributes LevelAttributes;
        public ItemType?[][] Carpet;
        public ItemType?[][] Furniture;

    }
}
