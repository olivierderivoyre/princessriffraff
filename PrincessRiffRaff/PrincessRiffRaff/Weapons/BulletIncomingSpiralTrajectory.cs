﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PrincessRiffRaff
{
    public class BulletIncomingSpiralTrajectory : BulletTrajectory
    {
        private readonly int minRadiusInCoord;
        private int currentRadius;
        private int currentAngus;
        private int initialAngus;


        public BulletIncomingSpiralTrajectory(IntVector2 initialPosition, int minRadiusInCoord)
        {
            base.InitialPosition = initialPosition;
            this.minRadiusInCoord = minRadiusInCoord;
            this.initialAngus = Tools.Rand.Next(360);
            this.currentRadius = Tools.Rand.Next(8, 12) * Tools.CellSizeInCoord;
        }

        public override bool IsFisnish()
        {
            return this.currentAngus > 60 * 5;
        }

        public override List<IntVector2> Update()
        {
            this.currentAngus++;
            this.currentRadius = Math.Max(this.currentRadius - 400, this.minRadiusInCoord);
            List<IntVector2> r = new List<IntVector2>();
            int radius = (this.initialAngus + this.currentAngus) % 360;
            IntVector2 centeredPosition = new IntVector2(Tools.CosCoord[radius], Tools.SinCoord[radius]) * this.currentRadius / 256;
            r.Add(this.InitialPosition + centeredPosition);
            return r;
        }
    }
}
