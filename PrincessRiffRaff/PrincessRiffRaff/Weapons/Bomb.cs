﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace PrincessRiffRaff
{
    public class Bomb
    {
        public Level level;
        public IntVector2 Position;
        public readonly BulletAttributes attributes;
        public static TextureRect Image = TextureRect.NewGround(2, 2);
        private int countDownTick = 4 * 60;
        private bool cascadeExplode = false;
        public bool IsFinished = false;
        /// <summary>
        /// To avoid that 5 bombs do 5x more damage
        /// </summary>
        public int ExplosionId = -1;

        public Bomb(Level level, IntVector2 Position, BulletAttributes attributes)
        {
            this.level = level;
            this.Position = Position;
            this.attributes = attributes;

        }

        public void Update()
        {
            this.countDownTick--;
            if (this.cascadeExplode || countDownTick <= 0)
            {
                this.Explode();
                this.IsFinished = true;
            }
        }
        public void Draw(Screen screen)
        {
            Color color = Color.White;
            if (this.countDownTick < 60)
            {
                if (((this.countDownTick / 5) % 2) == 0)
                {
                    color = Color.Red;
                }
            }           
            screen.Draw64(Image, this.Position, color);
        }
        protected virtual void Explode()
        {
            if (this.ExplosionId == -1)
            {
                this.ExplosionId = this.level.Tick;
            }
            IntVector2 centerExplosion = this.Position + new IntVector2(1, 1) * 2 * Tools.QuaterCellSizeInCoord;
            ExplosionAnimation explosion = new ExplosionAnimation(this.level, centerExplosion, this.attributes);
            explosion.ExplosionId = this.ExplosionId;
            this.level.Add(explosion);
            this.level.Remove(this);
            IntVector2 topLeft = this.Position - new IntVector2(1, 1) * this.attributes.ExplosionRange;
            foreach (Bomb neighbour in this.level.GetBombInside(topLeft, this.attributes.ExplosionRange * 2 + Tools.CellSizeInCoord))
            {
                neighbour.cascadeExplode = true;
                if (neighbour.ExplosionId == -1)
                {
                    neighbour.ExplosionId = this.ExplosionId;
                }
            }
        }
    }
}
