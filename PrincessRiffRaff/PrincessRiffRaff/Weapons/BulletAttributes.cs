﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Audio;

namespace PrincessRiffRaff
{
    public enum TargetType { Monster = 1, Player = 2 }

    public enum WeaponFireType { Linear, RandomLinear, AoeBomb, PotionHeal, PotionEnrage }

    public enum DamageTag { None = 0, MobBombAoe = 1 };

    public sealed class BulletAttributes
    {
        public WeaponFireType WeaponFireType = WeaponFireType.Linear;
        public TargetType TargetType;
        public TextureRect TextureRect;
        public int Damage;
        public int RangeInCoord;
        public int SpeedInCoord;
        public int StartSpeedInCoord;
        public int ExplosionRange;
        public bool GoOverFurniture = false;
        public bool GoOverCharacter = false;
        public Character SourceCharacter;
        public Player TargetPlayer;
        public DamageTag DamageTag = DamageTag.None;
        public SoundEffectInstance SoundExplosion;

        public BulletAttributes Clone()
        {
            return (BulletAttributes)base.MemberwiseClone();
        }
    }
    
}
