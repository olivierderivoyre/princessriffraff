﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PrincessRiffRaff
{
    public class BulletSpiralTrajectory : BulletTrajectory
    {
        private readonly int maxRadiusInCoord;
        private int currentRadius = 0;
        private int currentAngus;
        private int initialAngus;
        private int tick = 0;

        public BulletSpiralTrajectory(IntVector2 initialPosition, int maxRadiusInCoord, int initialAngus)
        {
            base.InitialPosition = initialPosition;
            this.maxRadiusInCoord = maxRadiusInCoord;
            this.initialAngus = initialAngus;           
        }

        public override bool IsFisnish()
        {
            return this.tick > 60 * 5;
        }

        public override List<IntVector2> Update()
        {
            this.tick++;
            this.currentAngus+=2;
            this.currentRadius = Math.Min(this.currentRadius + 140, this.maxRadiusInCoord);
            List<IntVector2> r = new List<IntVector2>();
            int radius = (this.initialAngus + this.currentAngus) % 360;
            IntVector2 centeredPosition = new IntVector2(Tools.CosCoord[radius], Tools.SinCoord[radius]) * this.currentRadius / 256;
            r.Add(this.InitialPosition + centeredPosition);
            return r;

        }
    }
}
