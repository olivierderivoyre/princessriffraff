﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace PrincessRiffRaff
{
    public class Invocation
    {
        public Weapon weapon;
        public Character sourceCharacter;
        public int Duration;
        private int tick;
        public bool IsInvoking = false;
        public bool IsFinished = false;


        public Invocation(Character source, Weapon weapon)
        {
            this.Duration = source.IsEnraged() ? weapon.IncantationDuration / 2 : weapon.IncantationDuration;
            this.weapon = weapon;
            this.sourceCharacter = source;
        }

        public Invocation(MobAI ai, Weapon weapon)
        {
            this.weapon = weapon;
            this.Duration = weapon.IncantationDuration;
            this.IsInvoking = true;
        }

        public static void PlayerPressing(Player character, Weapon weapon)
        {
            if (character.CurrentInvocation != null)
            {
                if (weapon == null || character.CurrentInvocation.weapon != weapon || character.CurrentInvocation.IsFinished)
                {
                    character.CurrentInvocation.Reset();
                    character.CurrentInvocation = null;
                }
            }
        }

        public void Reset()
        {
            this.tick = 0;
            this.IsInvoking = false;
            this.IsFinished = false;
        }

        public void Invoke()
        {
            Character character = this.sourceCharacter;
            if (!this.IsInvoking)
            {
                this.IsInvoking = true;
                this.IsFinished = false;
                this.tick = 0;
                if (character.CurrentInvocation != null)
                {
                    character.CurrentInvocation.Reset();
                }
                character.CurrentInvocation = this;
            }
            else
            {
                if (character.CurrentInvocation != this)
                {
                    this.Reset();
                    return;
                }
                this.tick++;
                if (this.tick >= this.Duration)
                {
                    this.IsInvoking = false;
                    this.IsFinished = true;
                    this.weapon.InvocationFinished();
                }
            }
        }

        public void AIManagedInvoke()
        {
            this.tick++;
            if (this.tick >= this.Duration)
            {
                this.IsInvoking = false;
                this.IsFinished = true;
            }
        }


        public void Draw(Screen screen, Character character)
        {
            if (this.IsInvoking)
            {
                int size = 24 + (8 * this.tick / this.Duration);
                IntVector2 topLeft = Tools.CenterCharacter(character.Position) - new IntVector2(size, size) / 2 * Tools.PixelSizeInCoord;
                screen.DrawScale(this.weapon.BulletAttributes.TextureRect, topLeft, size, Color.White);
            }
        }

    }
}
