﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PrincessRiffRaff
{
    public abstract class BulletTrajectory
    {
        public IntVector2 InitialPosition;
       
        public abstract bool IsFisnish();
       
        public abstract List<IntVector2> Update();
    }
}
