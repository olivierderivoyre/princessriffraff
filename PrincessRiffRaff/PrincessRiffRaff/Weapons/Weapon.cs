﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;

namespace PrincessRiffRaff
{
    public class Weapon
    {
        public Level Level;
        public TextureRect Icon;
        public Cooldown Cooldown;
        public BulletAttributes BulletAttributes;
        public SoundEffectInstance FireSound;
        public int DefaultAmmoQuantity = -1;
        public int AmmoQuantity = -1;
        public int IncantationDuration = 0;
        public bool LimitRangeToTargetDistance = false;
        private Invocation currentInvocation;
        



        public bool TryActivate()
        {
            Character source = this.BulletAttributes.SourceCharacter;
            if (this.AmmoQuantity == 0)
            {
                return false;
            }
            if (this.BulletAttributes.WeaponFireType == WeaponFireType.AoeBomb)
            {
                if (!this.Level.CanAddBombHere(this.getBombPosition()))
                {
                    return false;
                }
            }
            if (this.IncantationDuration <= 0)
            {
                if (!this.Cooldown.TryStartCooldown(this.Level.Tick, source.IsEnraged()))
                {
                    return false;
                }
                fire();
                return true;
            }
            else
            {
                if (this.Cooldown.AllowAction(this.Level.Tick, source.IsEnraged()))
                {
                    if (this.currentInvocation == null || this.currentInvocation.IsFinished)
                    {
                        this.currentInvocation = new Invocation(source, this);                        
                    }
                    this.currentInvocation.Invoke();
                    return this.currentInvocation.IsFinished;
                }

                return false;
            }
        }

        public void InvocationFinished()
        {
            this.Cooldown.StartCooldown(this.Level.Tick);
            this.fire();
        }

        public void ForceFire()
        {
            this.fire();
        }

        private void fire()
        {
            if (this.AmmoQuantity != -1)
            {
                this.AmmoQuantity--;
            }

            this.BulletAttributes.SourceCharacter.PlaySoundOnScreen(this.FireSound);

            switch (this.BulletAttributes.WeaponFireType)
            {
                case WeaponFireType.Linear: this.fireLinearBullet(); break;
                case WeaponFireType.RandomLinear: this.fireRandomLinearBullets(); break;
                case WeaponFireType.AoeBomb: this.fireBomb(); break;
                case WeaponFireType.PotionHeal: this.firePotionHeal(); break;
                case WeaponFireType.PotionEnrage: this.firePotionEnrage(); break;
                default: throw new NotImplementedException("" + this.BulletAttributes.WeaponFireType);
            }

        }

        private void fireLinearBullet()
        {
            Character source = this.BulletAttributes.SourceCharacter;
            IntVector2 weaponDirection = source.GetWeaponDirection();
            fireLinearBullet(source, weaponDirection);
        }

        private void fireLinearBullet(Character source, IntVector2 weaponDirection)
        {
            int range = this.BulletAttributes.RangeInCoord;
            if (this.LimitRangeToTargetDistance)
            {
                Character target = source.GetTargetCharacter();
                if (target != null)
                {
                    range = Math.Min(range, IntVector2.GetDistance(source.Position, target.Position));
                }
            }
            int randAngus = Tools.RandDiceZeroCentered(this.BulletAttributes.SourceCharacter is Player ? 10 : 25);

            IntVector2 bulletDirection = Weapon.RotateCoord(weaponDirection, randAngus);
            IntVector2 gunPosition = Tools.CenterCharacter(source.Position);
            Bullet bullet = new Bullet(Level, BulletAttributes, new BulletLinearTrajectory(gunPosition, bulletDirection, BulletAttributes, range));
            this.Level.Add(bullet);            
        }
        private void fireRandomLinearBullets()
        {
            Character source = this.BulletAttributes.SourceCharacter;
            IntVector2 weaponDirection = source.GetWeaponDirection();
            int rand = Tools.Rand.Next(10);
            if (rand == 0)
            {
                this.fireLinearBullet(source, weaponDirection);
            }
            else if (rand == 1)
            {
                
                this.fireLinearBullet(source, Weapon.RotateCoord(weaponDirection, 30));
                this.fireLinearBullet(source, Weapon.RotateCoord(weaponDirection, -30));
            }
            else if (rand == 2)
            {
                this.fireLinearBullet(source, Weapon.RotateCoord(weaponDirection, 45));
                this.fireLinearBullet(source, weaponDirection);
                this.fireLinearBullet(source, Weapon.RotateCoord(weaponDirection, -45));
            }
            else if (rand == 3)
            {
                foreach (IntVector2 direction in Tools.CardinalPoints)
                {
                    this.fireLinearBullet(source, direction * 255);
                }                
            }
            else if (rand == 4)
            {
                foreach (IntVector2 direction in Tools.CardinalPoints)
                {
                    this.fireLinearBullet(source, Weapon.RotateCoord(direction * 255, 45));
                }
            }
            else if (rand == 5)
            {
                this.fireLinearBullet(source, Weapon.RotateCoord(weaponDirection, 120));
                this.fireLinearBullet(source, weaponDirection);
                this.fireLinearBullet(source, Weapon.RotateCoord(weaponDirection, -120));
            }
            else if (rand == 6)
            {
                this.fireLinearBullet(source, weaponDirection);
                this.fireLinearBullet(source, Weapon.RotateCoord(weaponDirection, 90));
                this.fireLinearBullet(source, Weapon.RotateCoord(weaponDirection, 180));
                this.fireLinearBullet(source, Weapon.RotateCoord(weaponDirection, 270));
            }
            else if (rand == 7)
            {
                this.fireLinearBullet(source, weaponDirection);
                this.fireLinearBullet(source, Weapon.RotateCoord(weaponDirection, 180));
            }
            else if (rand == 8)
            {
                this.fireLinearBullet(source, Weapon.RotateCoord(weaponDirection, 15));
                this.fireLinearBullet(source, Weapon.RotateCoord(weaponDirection, -15));
                this.fireLinearBullet(source, Weapon.RotateCoord(weaponDirection, 130));
                this.fireLinearBullet(source, Weapon.RotateCoord(weaponDirection, -130));
            }
            else if (rand == 9)
            {
                int a = 0;
                while (a < 360)
                {
                    a += Tools.Rand.Next(30, 90);
                    this.fireLinearBullet(source, Weapon.RotateCoord(new IntVector2(255, 0), a));
                }
            }
        }

        private void fireBomb()
        {
            IntVector2 position = this.getBombPosition();
            if (!this.Level.CanAddBombHere(position))
            {
                return;
            }
            Bomb bomb = new Bomb(this.Level, position, this.BulletAttributes);
            this.Level.Add(bomb);
        }

        private void firePotionHeal()
        {
            foreach (Player p in this.Level.GetAlivePlayers())
            {
                p.Life = Math.Min(p.Life + 6 * Player.QuaterHearthLife, p.MaxLife);
            }
        }
        private void firePotionEnrage()
        {
            foreach (Player p in this.Level.GetAlivePlayers())
            {
                p.EndEnrageTick = this.Level.Tick + 8 * 60;
            }
        }

        private IntVector2 getBombPosition()
        {
            IntVector2 position = Tools.CenterCharacterCell(this.BulletAttributes.SourceCharacter.Position);
            return position;
        }
        public static IntVector2 RotateCoord(IntVector2 d, int angus)
        {
            int positiveAngus = ((angus % 360) + 360) % 360;
            int cos = Tools.CosCoord[positiveAngus];
            int sin = Tools.SinCoord[positiveAngus];
            return new IntVector2(
                 (d.X * cos + d.Y * sin) / 256,
                  (d.Y * cos - d.X * sin) / 256);
        }

        public void ResetOnResurrect()
        {
            if (this.AmmoQuantity != -1)
            {
                this.AmmoQuantity = Math.Max(this.AmmoQuantity, this.DefaultAmmoQuantity);
            }
        }

        public bool CanClearShoot(Character source, Character target)
        {
            if (!this.Cooldown.AllowAction(this.Level.Tick, false))
            {
                return false;
            }
            int distance = IntVector2.GetDistance(source.Position, target.Position);
            if (distance >= this.BulletAttributes.RangeInCoord)
            {
                return false;
            }
            IntVector2 sourcePosition = Tools.CenterCharacter(source.Position);
            IntVector2 target1 = target.Position;
            ///Do a second check to do not waste the first shoot on wall due to the weapon-accuracy
            IntVector2 target2 = target.Position + new IntVector2(1, 1) * Tools.CellSizeInCoord;
            return this.Level.HasClearShoot(sourcePosition, target1, this.BulletAttributes.RangeInCoord)
                && this.Level.HasClearShoot(sourcePosition, target2, this.BulletAttributes.RangeInCoord);
        }
    }
}
