﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;

namespace PrincessRiffRaff
{    
   
    public class Bullet
    {
        public Level level;
        private BulletTrajectory trajectory;
        private bool isFinish = false;
		public readonly BulletAttributes attributes;       
        private IntVector2 Position;
       
        
        public static IntVector2 GetBulletDirection(IntVector2 start, IntVector2 end)
        {
            IntVector2 delta = IntVector2.Delta(start, end);
            int distance = delta.GetDistance();
            if (distance == 0)
            {
                return new IntVector2(1, 1);
            }
            return delta * Tools.PixelSizeInCoord / distance;
        }

        public Bullet(Level level, BulletAttributes bulletAttributes, BulletTrajectory trajectory)
        {
            this.level = level;
			this.attributes = bulletAttributes;
			this.trajectory = trajectory;
            this.Position = trajectory.InitialPosition;          
        }


        public bool IsFisnish()
        {
            return this.isFinish;
        }
        public void ForceFinish()
        {
            this.isFinish = true ;
        }
        public void Update()
        {
            bool bulletStopped = false;
            if (this.trajectory.IsFisnish())
            {
                bulletStopped = true;
            }
            else
            {                
                foreach (IntVector2 bulletPosition in this.trajectory.Update())
                {
                    this.Position = bulletPosition;

                    if (this.level.Land.HasBulletBlock(bulletPosition, this.attributes))
                    {
                        bulletStopped = true;
                    }                   
                    if (!this.attributes.GoOverCharacter)
                    {
                        if ((this.attributes.TargetType & TargetType.Monster) != 0)
                        {
                            if (level.GetVisibleMobsHitBy(bulletPosition, 0).Any(m => m.IsVulnerableTo(this.attributes)))
                            {
                                bulletStopped = true;
                                break;
                            }
                        }
                        if ((this.attributes.TargetType & TargetType.Player) != 0)
                        {
                            if (level.GetFriendlyUnitsHitBy(bulletPosition, 0).Any())
                            {
                                bulletStopped = true;
                                break;
                            }
                        }
                    }
                }
            }


			if (bulletStopped)
			{
				explose();
			}
        }


        private void explose()
		{
			if (this.attributes.ExplosionRange <= 0)
			{
				doDamage(this.level, this.Position, this.attributes, -1);
				
			}
			else
			{
                var explosionAnimation = new ExplosionAnimation(this.level, this.Position, this.attributes);
                this.level.Add(explosionAnimation);
			}
            this.isFinish = true;            
		}

        public static void doDamage(Level level, IntVector2 bulletPosition, BulletAttributes attributes, int explosionId)
		{
			if ((attributes.TargetType & TargetType.Monster) != 0)
			{
                foreach (Mob mob in level.GetVisibleMobsHitBy(bulletPosition, attributes.ExplosionRange))
				{
                    mob.HitBy(attributes, explosionId);
				}
			}
			if ((attributes.TargetType & TargetType.Player) != 0)
			{
                foreach (Character character in level.GetFriendlyUnitsHitBy(bulletPosition, attributes.ExplosionRange))
				{
                    character.HitBy(attributes, explosionId);
				}
			}
		}

		public void Draw(Screen screen)
        {
            IntVector2 topLeft = this.Position - new IntVector2(16, 16) * Tools.PixelSizeInCoord;
            screen.Draw32(this.attributes.TextureRect, topLeft, Color.White);
        }
      
    }
}
