﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using Microsoft.Xna.Framework;

namespace PrincessRiffRaff
{
    public class ExplosionAnimation : Animation
    {
        private const int nbTickPerExplotionFrame = 3;
        private Level level;
        private readonly IntVector2 center;
        private readonly BulletAttributes attributes;
        private readonly int range;
        private TextureRect currentImage;
        public int ExplosionId = -1;

        public ExplosionAnimation(Level level, IntVector2 center, BulletAttributes attributes)
        {
            this.level = level;
            this.center = center;
            this.attributes = attributes;
            this.range = attributes.ExplosionRange;          
        }

        protected override IEnumerable Play()
        {
            if (this.attributes.SoundExplosion != null)
            {
                this.attributes.SoundExplosion.Play();
            }
            for (int frame = 0; frame < 7; frame++)
            {
                this.currentImage = new TextureRect(TextureRect.SpriteFile.TilesetInterface40, new Rectangle(82 + frame * 41, 124, 40, 40));
                if (frame == 2)
                {
                    ///do damage
                    Bullet.doDamage(this.level, this.center, this.attributes, this.ExplosionId);
                }
                for (int i = 0; i < nbTickPerExplotionFrame; i++)
                {
                    yield return null;
                }
            }
            
        }
        public override void Draw(Screen screen)
        {
            if (this.currentImage == null)
            {
                return;///Not yet started
            }
            IntVector2 topLeft = this.center - new IntVector2(range, range);
            screen.DrawScale(this.currentImage, topLeft, range * 2 / Tools.PixelSizeInCoord, Color.White);
        }
    }
}
