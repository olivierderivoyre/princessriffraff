﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PrincessRiffRaff
{
    public class BulletLinearTrajectory : BulletTrajectory
    {
       
        private readonly IntVector2 directionInCoord;
        private int speedInCoord;
        private int maxSpeedInCoord;
        private readonly int rangeInCoord;       
        protected IntVector2 Position;
        private int tick = 0;
       
               
        public BulletLinearTrajectory(IntVector2 initialPosition, IntVector2 directionInCoord, BulletAttributes attributes, int? forcedRange)
        {
            this.InitialPosition = initialPosition;
            this.Position = initialPosition;
            this.directionInCoord = directionInCoord;

            this.maxSpeedInCoord = attributes.SpeedInCoord;
            if (attributes.StartSpeedInCoord > 0)
            {
                this.speedInCoord = attributes.StartSpeedInCoord;                
            }
            else
            {
                this.speedInCoord = this.maxSpeedInCoord;
            }
            this.rangeInCoord = forcedRange ?? attributes.RangeInCoord;
            if (this.speedInCoord <= 0)
            {
                throw new ArgumentException();
            }                        
        }
        public static BulletLinearTrajectory NewToTarget(IntVector2 initialPosition, IntVector2 target, BulletAttributes attributes)
        {
            IntVector2 directionInCoord = Bullet.GetBulletDirection(initialPosition, target);  
            int distance = IntVector2.GetDistance(initialPosition, target);
            return new BulletLinearTrajectory(initialPosition, directionInCoord, attributes, distance);
            
        }
        public override bool IsFisnish()
        {            
            return IntVector2.GetDistance(this.InitialPosition, this.Position) >= this.rangeInCoord;
        }

   
        public override List<IntVector2> Update()
        {
            this.tick++;
            if (this.speedInCoord < this.maxSpeedInCoord)
            {
                int incr = Math.Max(4, (this.maxSpeedInCoord - this.speedInCoord) / 50) + 1;                
                this.speedInCoord = Math.Min(this.speedInCoord + incr, this.maxSpeedInCoord);
            }
            List<IntVector2> r = new List<IntVector2>();
            for (int testedDistance = 0; testedDistance < this.speedInCoord; testedDistance += 8 * Tools.PixelSizeInCoord)
            {
                IntVector2 position = this.Position + this.directionInCoord * testedDistance / 256;
                r.Add(position);
            }
            this.Position = this.Position + this.directionInCoord * this.speedInCoord / 256;
            r.Add(this.Position);

            return r;
        }

    }
}
