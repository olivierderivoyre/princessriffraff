﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PrincessRiffRaff
{
    public class TickLimiter
    {
        private readonly int maxOccuranceDuringTime;
        private readonly int timeRange;
        private int nbOccurance;
        private int lastTick;
        public TickLimiter(int maxOccuranceDuringTime, int timeRange)
        {
            this.maxOccuranceDuringTime = maxOccuranceDuringTime;
            this.timeRange = timeRange;            
        }
       
        public bool AllowAction(int now, bool isEnraged)
        {
            if (this.nbOccurance < this.maxOccuranceDuringTime)
            {
                return true;
            }
            return this.isEllapsed(now, isEnraged);
        }

        private bool isEllapsed(int now, bool isEnraged)
        {
            if (isEnraged)
            {
                return now - this.lastTick >= this.timeRange / 2;
            }
            return now - this.lastTick >= this.timeRange;
        }

        public void StartCooldown(int now, bool isEnraged)
        {
            if (this.isEllapsed(now, isEnraged))
            {
                this.nbOccurance = 1;
                this.lastTick = now;
            }
            else
            {
                this.nbOccurance++;              
            }           
        }

        public void RandomizeStarting(int now)
        {
            this.nbOccurance = this.maxOccuranceDuringTime;
            this.lastTick = now - Tools.Rand.Next(this.timeRange);
        }
    }
}
