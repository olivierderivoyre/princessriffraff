﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PrincessRiffRaff
{
    public class BulletMovingTargetTrajectory : BulletTrajectory
    {
        private readonly Character target;
        protected IntVector2 Position;
        private readonly int speedInCoord;
        private int tick = 0;

        public BulletMovingTargetTrajectory(IntVector2 initialPosition, BulletAttributes attributes, Character target)
        {
            this.Position = base.InitialPosition = initialPosition;
            this.target = target;
            this.speedInCoord = attributes.SpeedInCoord;
        }

        public override bool IsFisnish()
        {
            if (this.target.IsDead)
            {
                return true;
            }
            return this.tick > 60 * 20;
        }

        public override List<IntVector2> Update()
        {
            this.tick++;
            IntVector2 direction = MobAI.GetFlyingDirectionToGo(this.Position, Tools.CenterCharacter(this.target.Position));
            int speed = this.speedInCoord;
            if (direction.X != 0 && direction.Y != 0)
            {
                speed = this.speedInCoord * 100 / 141;///Avoid diag mob go merge with horizontal mobs
            }
            List<IntVector2> r = new List<IntVector2>();
            for (int i = 0; i < speed; i += 256)
            {
                r.Add(this.Position + i * direction);
            }
            this.Position = this.Position + speed * direction;
            r.Add(this.Position);
            return r;

        }
    }
}
