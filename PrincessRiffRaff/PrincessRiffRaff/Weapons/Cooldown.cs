﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PrincessRiffRaff
{
    public class Cooldown
    {
        private List<TickLimiter> timeLimiters = new List<TickLimiter>();

        public Cooldown(int maxOccuranceDuringTime, int timeRange)
            : this(new int[] { maxOccuranceDuringTime, timeRange })
        {
        }
        public Cooldown(params int[] tickLimiterParameters)
        {
            for (int i = 0; i < tickLimiterParameters.Length; i += 2)
            {
                this.Add(new TickLimiter(tickLimiterParameters[i], tickLimiterParameters[i + 1]));
            }
        }

        public void Add(TickLimiter limiter)
        {
            this.timeLimiters.Add(limiter);
        }
        public bool AllowAction(int now, bool isEnraged)
        {
            foreach (TickLimiter limiter in this.timeLimiters)
            {
                if (!limiter.AllowAction(now, isEnraged))
                {
                    return false;
                }
            }
            return true;
        }
        public void StartCooldown(int now, bool isEnraged = false)
        {
            foreach (TickLimiter limiter in this.timeLimiters)
            {
                limiter.StartCooldown(now, isEnraged);
            }
        }

        public bool TryStartCooldown(int now, bool isEnraged)
        {
            if (this.AllowAction(now, isEnraged))
            {
                this.StartCooldown(now, isEnraged);
                return true;
            }
            return false;
        }
        /// <summary>
        /// To have mob not perfectly sync
        /// </summary>       
        public void RandomizeStarting(int now)
        {
            foreach (TickLimiter limiter in this.timeLimiters)
            {
                limiter.RandomizeStarting(now);
            }
        }
    }
}
