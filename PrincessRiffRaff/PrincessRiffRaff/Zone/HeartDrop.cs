﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace PrincessRiffRaff
{
    /// <summary>
    /// Increase max life
    /// </summary>
    public class HeartDrop : ZoneTrigger
    {
        public static TextureRect Image = ItemTypeHelper.GetTextureRect(ItemType.ZONE_NEW_HEARTH);
        public HeartDrop()
        {
           
        }

        public override void Draw(Screen screen)
        {
            screen.Draw64(Image, this.Position, Color.White);
        }
        public override void OnTaken(Player player)
        {
            this.Level.Music.Drop.Play();
            this.Level.CurrentBuffHeart = Math.Min(3, this.Level.CurrentBuffHeart + 1);
            foreach (Player p in new[] { this.Level.player1, this.Level.player2 })
            {
                p.MaxLife = (2 +  this.Level.CurrentBuffHeart) * 4 * Player.QuaterHearthLife;
                p.Life = Math.Min(p.MaxLife, p.Life + 6 * Player.QuaterHearthLife);
            }
            this.Level.Remove(this);
          
        }
    }
}
