﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace PrincessRiffRaff
{
    public class AmmoProvider : ZoneTrigger
    {
        public static TextureRect ImageOn = TextureRect.NewGround(1, 3);
        public static TextureRect ImageOff = TextureRect.NewGround(2, 3);


       
        private bool isOn = true;


        public override void Draw(Screen screen)
        {
            screen.Draw64(this.isOn ? ImageOn : ImageOff, this.Position, Color.White);
        }

        public override void OnTaken(Player player)
        {
            if (this.isOn)
            {
                this.Level.Music.Drop.Play();
                this.isOn = false;
                foreach (Player p in this.Level.GetAlivePlayers())
                {
                    p.Life = Math.Min(p.Life + 4 * Player.QuaterHearthLife, p.MaxLife);
                    foreach (Weapon weapon in p.Weapons.Where(w => w != null))
                    {
                        weapon.AmmoQuantity = weapon.DefaultAmmoQuantity;
                    }
                }
            }
        }
       
    }
}
