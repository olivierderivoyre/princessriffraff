﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace PrincessRiffRaff
{
    public abstract class ZoneTrigger
    {        
        public Level Level;
        public IntVector2 Position;

        public abstract void Draw(Screen screen);
        public abstract void OnTaken(Player player);
       
       


    }
}
