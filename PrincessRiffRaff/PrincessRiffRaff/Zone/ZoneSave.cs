﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PrincessRiffRaff
{
    public class ZoneSave : IUpdatable
    {
        public IntVector2 Position;
        public Level Level;

        public void Update()
        {
            int radius = 1;
            IntVector2 topLeft = this.Position - new IntVector2(radius, radius) * Tools.CellSizeInCoord;
            var alivePlayers = this.Level.GetAlivePlayers();
            int count = alivePlayers.Count(p =>
                p.Position.IsInside(topLeft, 7 * Tools.CellSizeInCoord, (radius + 1 + radius) * Tools.CellSizeInCoord));
            if (count > 0)
            {
                this.Level.PlayerResurectPosition = this.Position;
            }           
        }
    }
}
