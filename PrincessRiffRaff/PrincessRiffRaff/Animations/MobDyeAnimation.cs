﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using Microsoft.Xna.Framework;

namespace PrincessRiffRaff
{
    public class MobDyeAnimation : Animation
    {
        protected readonly Level level;
        protected readonly Mob mob;
        private readonly long score;
        private IntVector2 center;
        protected int size = 64;

        public MobDyeAnimation(Mob mob, long score)
        {
            this.mob = mob;
            this.level = mob.Level;
            this.center = Tools.CenterCharacter(this.mob.Position);
            this.score = score;
        }

        protected override IEnumerable Play()
        {
            while (this.size > 0)
            {
                yield return null;
                this.size -= 2;

            }
            this.showGrave();
        }

        protected void showGrave()
        {
            this.level.Music.MobDying.Play();
            MobGrave grave = new MobGrave(this.center, this.mob.IsBoss);
            if (!grave.IsOverBlock(this.level))
            {
                this.level.Add(grave);
            }
            this.level.Add(new ScoreAnimation(this.center, this.score));
        }

        public override void Draw(Screen g)
        {
            if (this.size > 0)
            {
                g.DrawScale(this.mob.Image, this.center - new IntVector2(1, 1) * size * Tools.PixelSizeInCoord / 2, size, Color.White);
            }
        }
    }
}
