﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

namespace PrincessRiffRaff
{   
    public abstract class Animation
    {
        private CoroutineCpu cpu;
        public bool IsFinished = false;
        public bool IsStarted = false;

        public bool IsActive { get { return this.IsStarted && !this.IsFinished; } }

        public void Update()
        {
            if (this.cpu == null)
            {
                this.IsStarted = true;
                this.cpu = new CoroutineCpu();
                this.cpu.Go(this.Play());
            }
            else
            {
                this.cpu.Update();
            }
            if (this.cpu.IsIdle)
            {
                this.IsFinished = true;
            }
        }

        protected abstract IEnumerable Play();

        public virtual void Draw(Screen g)
        {
        }
        
    }
}
