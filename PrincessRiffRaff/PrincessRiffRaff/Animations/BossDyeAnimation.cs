﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using Microsoft.Xna.Framework;

namespace PrincessRiffRaff
{
    public class BossDyeAnimation : MobDyeAnimation
    {

        public BossDyeAnimation(Mob mob, long score) :
            base(mob, score)
        {
        }

        protected override IEnumerable Play()
        {
            this.level.CleanBullets();
            this.level.Music.BossDying.Play();
            yield return null;
            yield return CoroutineCpu.Current.GoSleep(20);

            while (this.size > 0)
            {
                yield return CoroutineCpu.Current.GoSleep(8);
                this.size -= 2;
            }

            base.showGrave();

            this.level.Music.LevelWin.Play();
            yield return CoroutineCpu.Current.GoSleep(4 * 60);

            this.level.Game.LevelWin();
            this.level.Game.YouHaveWinMenu = new YouHaveWinMenu(this.level);
        }

    }
}
