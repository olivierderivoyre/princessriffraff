﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using Microsoft.Xna.Framework;

namespace PrincessRiffRaff
{
    public class ScoreAnimation : Animation
    {
        private readonly IntVector2 mobPosition;
        private readonly string score;
        private IntVector2? position;

        public ScoreAnimation(IntVector2 mobPosition, long score)
        {
            this.mobPosition = mobPosition;
            this.score = score.ToString();
        }


        protected override IEnumerable Play()
        {
            for (int i = 0; i < 60 * 2; i++)
            {
                this.position = this.mobPosition - new IntVector2(Tools.QuaterCellSizeInCoord, Tools.QuaterCellSizeInCoord *2  + i * 100);
                yield return null;
            }
        }

        public override void Draw(Screen g)
        {
            if (this.position == null)
            {
                return;
            }
            g.DrawString(this.score, this.position.Value, Color.White);
        }
    }
}
