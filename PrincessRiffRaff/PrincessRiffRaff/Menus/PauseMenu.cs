﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using log4net;

namespace PrincessRiffRaff
{
    public class PauseMenu
    {
        private static ILog logger = LogManager.GetLogger(typeof(PauseMenu));
        private Level level;
        private int tick;
        private int selectedMenu = 0;

        public PauseMenu(Level level)
		{
            logger.Info("Pause");
			this.level = level;			
		}


		private Color? getSelectedColor(int menuIndex)
		{
			if (this.selectedMenu == menuIndex)
			{
				return Theme.SelectionColor;
			}
			return null;
		}

        public void Draw(SpriteBatch spriteBatch, Ressource ressource)
        {
            IntVector2 topLeft = new IntVector2(260, 80);
            spriteBatch.Draw(ressource.TilesetInterface40, new Rectangle(topLeft.X, topLeft.Y, 300, 300), new Rectangle(164, 0, 40, 40), Color.White);
            topLeft = new IntVector2(360, 120);
            spriteBatch.DrawString(ressource.FontTitle, "Paused", new Vector2(topLeft.X, topLeft.Y), Theme.TitleColor);

            topLeft = new IntVector2(320, 180);
            IntroMenu.DrawMenu(spriteBatch, ressource, "Resume ", topLeft, getSelectedColor(0));
            IntroMenu.DrawMenu(spriteBatch, ressource, "Quit level", new IntVector2(topLeft.X, topLeft.Y + 64), getSelectedColor(1));
            IntroMenu.DrawMenu(spriteBatch, ressource, "Exit game", new IntVector2(topLeft.X, topLeft.Y + 128), getSelectedColor(2));
        
        }

        public void Update(InputState inputState, InputState previousInputState)
        {
            this.tick++;
            if (this.tick < 60)
            {
                return;
            }
            moveCursor(level.player1.InputController, inputState, previousInputState);
            moveCursor(level.player2.InputController, inputState, previousInputState);
            click(level.player1.InputController, inputState, previousInputState);
            click(level.player2.InputController, inputState, previousInputState);
        }

        private void moveCursor(InputController inputController, InputState inputState, InputState previousInputState)
        {
            if (inputController.HasDirection(previousInputState))
            {
                return;
            }
            IntVector2 move = inputController.GetMoveDirection(inputState);
            if (move == IntVector2.Zero)
            {
                return;
            }
            this.selectedMenu = Tools.InRange(this.selectedMenu + move.Y, 0, 2);
        }

        private void click(InputController inputController, InputState inputState, InputState previousInputState)
        {
            if (inputController.HasAnyActionButtonPressed(inputState) && !inputController.HasAnyActionButtonPressed(previousInputState))
            {
                if (this.selectedMenu == 0)///Resume
                {
                    logger.Info("Resume");
                    this.level.Game.PauseMenu = null;
                }
                else if (this.selectedMenu == 1)
                {
                    logger.Info("exit level");
                    this.level.Game.ShowLevelsMap(null);
                }
                else if (this.selectedMenu == 2)///Exit
                {
                    logger.Info("Game exit");
                    this.level.Game.Exit();
                }
            }
        }


    }
}
