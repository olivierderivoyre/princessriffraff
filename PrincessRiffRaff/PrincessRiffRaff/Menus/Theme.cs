﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace PrincessRiffRaff
{
    public static class Theme
    {

        public static readonly Color TextColor = new Color(245,241,232);
        public static readonly Color TextQuantityColor = new Color(245, 241, 232);
        public static readonly Color TitleColor = new Color(169, 87, 14);//Color.Yellow;
        public static readonly Color ScoreColor = Color.Yellow;
        public static readonly Color SelectionColor = new Color(169,87,14);// new Color(180, 180, 180);


    }
}
