﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System.Collections;

namespace PrincessRiffRaff
{
    public class YouHaveWinMenu
    {
        private Level level;
        private bool isContinueMenuSelected = false;
        private readonly CoroutineCpu showScoreAnnimation = new CoroutineCpu();

        private long player1DisplayedScore = 0;
        private long player2DisplayedScore = 0;

        public YouHaveWinMenu(Level level)
        {
            this.level = level;
            this.showScoreAnnimation = new CoroutineCpu();
            this.showScoreAnnimation.Go(this.showScoreLive());
        }



        public void Draw(SpriteBatch spriteBatch, Ressource ressource)
        {
            IntVector2 topLeft = new IntVector2(100, 40);
            spriteBatch.Draw(ressource.TilesetInterface40, new Rectangle(topLeft.X, topLeft.Y, 600, 370), new Rectangle(165, 1, 38, 38), Color.White);
            topLeft = new IntVector2(320, 60);
            spriteBatch.DrawString(ressource.FontTitle, "You win!", new Vector2(topLeft.X, topLeft.Y), Theme.TitleColor);

            topLeft = new IntVector2(340, 84);
            if (!this.level.player2.NeedToPressStartToPlay)
            {
                drawScore(spriteBatch, ressource, this.level.player1.PlayerStats, this.level.player1, this.player1DisplayedScore, new IntVector2(140, topLeft.Y));
                drawScore(spriteBatch, ressource, this.level.player2.PlayerStats, this.level.player2, this.player2DisplayedScore, new IntVector2(500, topLeft.Y));
            }
            else
            {
                drawScore(spriteBatch, ressource, this.level.player1.PlayerStats, this.level.player1, this.player1DisplayedScore, new IntVector2(200, topLeft.Y));

            }
            topLeft = new IntVector2(360, 360);
            IntroMenu.DrawMenu(spriteBatch, ressource, "Continue", topLeft, this.isContinueMenuSelected ? (Color?)Theme.SelectionColor : null);
        }



        public void Update(InputState inputState, InputState previousInputState)
        {
            if (!this.showScoreAnnimation.IsIdle)
            {                
                this.showScoreAnnimation.Update();
                return;
            }

            click(level.player1.InputController, inputState, previousInputState);
            click(level.player2.InputController, inputState, previousInputState);
        }

        private void click(InputController inputController, InputState inputState, InputState previousInputState)
        {
            if (inputController.HasAnyActionButtonPressed(inputState) && !inputController.HasAnyActionButtonPressed(previousInputState))
            {
                if (this.isContinueMenuSelected)///Back to save point
                {
                    this.level.Game.ShowLevelsMap(null);
                }
            }
        }

        #region Score

        private static long increment(long value, long target)
        {
            long diff = target - value;
            if (diff == 0)
            {
                return 0;
            }
            if (diff < 0)
            {
                return -1 * increment(target, value);
            }
            if (diff < 10)
            {
                return diff;
            }
            if (diff < 100)
            {
                return diff / 2;
            }
            return diff / 8;
            
        }
        private static bool incr(ref long value, long target)
        {
            value += increment(value, target);
            return value != target;
        }
        private IEnumerable showScoreLive()
        {


            long target1 = computeFinalScore(this.level.Score, this.level.player1.PlayerStats);
            long target2 = computeFinalScore(this.level.Score, this.level.player2.PlayerStats);

            yield return null;
            long incr = 1;
            while (incr != 0)
            {
                incr = Math.Max(increment(this.player1DisplayedScore, target1), increment(this.player2DisplayedScore, target2));
                this.player1DisplayedScore = Math.Min(this.player1DisplayedScore + incr, target1);
                this.player2DisplayedScore = Math.Min(this.player2DisplayedScore + incr, target2);

                
                this.level.Music.ScoreCounting.Play();
                
                yield return CoroutineCpu.Current.GoSleep(4);
            }

            yield return CoroutineCpu.Current.GoSleep(30);
            this.isContinueMenuSelected = true;
        }

        private static long computeFinalScore(long levelScore, PlayerStats playerStats)
        {
            long r = levelScore;
            r += playerStats.DamageDone;
            r += playerStats.MobKilled * 100;
            return r;
        }

        private void drawScore(SpriteBatch spriteBatch, Ressource ressource, PlayerStats playerStats, Player player, long finalScore,  IntVector2 topLeft)
        {
            Texture2D spriteTexture = player.getTexture2D(ressource);
            spriteBatch.Draw(spriteTexture,
                new Rectangle(topLeft.X + 64, topLeft.Y, 40, 40),
                new Rectangle(0, 0, 64, 64), Color.White);
            topLeft.Y += 48;
            spriteBatch.DrawString(ressource.Font, "Base score: " + this.level.Score, new Vector2(topLeft.X, topLeft.Y), Theme.TextColor);
            spriteBatch.DrawString(ressource.Font, "Damages done: " + playerStats.DamageDone, new Vector2(topLeft.X, topLeft.Y + 24), Theme.TextColor);

            spriteBatch.DrawString(ressource.Font, "Mobs killed: " + playerStats.MobKilled, new Vector2(topLeft.X, topLeft.Y + 48), Theme.TextColor);
            spriteBatch.DrawString(ressource.Font, "Player deaths: " + playerStats.Death, new Vector2(topLeft.X, topLeft.Y + 72), Theme.TextColor);
            spriteBatch.DrawString(ressource.Font, "Damages taken: " + playerStats.DamageTaken, new Vector2(topLeft.X, topLeft.Y + 96), Theme.TextColor);


            spriteBatch.DrawString(ressource.Font, "Final score: ", new Vector2(topLeft.X, topLeft.Y + 144), Theme.TextColor);
            spriteBatch.DrawString(ressource.FontScore, "" + finalScore, new Vector2(topLeft.X + 20, topLeft.Y + 180), Theme.TextColor);
        }

        #endregion Score

    }
}
