﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace PrincessRiffRaff
{
    /// <summary>
    /// It is the first screen shown when starting the game
    /// </summary>
    public class IntroMenu
    {
        private readonly PrincessRiffRaffGame Game;
        private int selectedMenu = 0;


        public IntroMenu(PrincessRiffRaffGame game)
        {
            this.Game = game;
        }

        private Color? getSelectedColor(int menuIndex)
        {
            if (this.selectedMenu == menuIndex)
            {
                return Theme.SelectionColor;
            }
            return null;
        }

        public static void DrawMenu(SpriteBatch spriteBatch, Ressource ressource, string text, IntVector2 topLeft, Color? focusColor)
        {
            spriteBatch.DrawString(ressource.Font, text, new Vector2(topLeft.X, topLeft.Y), Theme.TextColor);
            if (focusColor != null)
            {
                int textPx = text.Length * 9;
                spriteBatch.Draw(ressource.TilesetInterface40, new Rectangle(topLeft.X - 15, topLeft.Y - 2, 15, 40), new Rectangle(123, 0, 15, 40), focusColor.Value);
                spriteBatch.Draw(ressource.TilesetInterface40, new Rectangle(topLeft.X, topLeft.Y - 2, textPx, 40), new Rectangle(123 + 15, 0, 10, 40), focusColor.Value);
                spriteBatch.Draw(ressource.TilesetInterface40, new Rectangle(topLeft.X + textPx, topLeft.Y - 2, 15, 40), new Rectangle(123 + 25, 0, 15, 40), focusColor.Value);
            }
        }

        public void Draw(SpriteBatch spriteBatch, Ressource ressource)
        {
            //IntVector2 topLeft = new IntVector2(300, 80);
            //spriteBatch.DrawString(ressource.Font, "Princess Riff Raff", new Vector2(topLeft.X, topLeft.Y), Color.Yellow);
            spriteBatch.Draw(ressource.ImgPrincessRiffRaffLogo, new Rectangle(0, 0, 800, 225), Color.White);

            IntVector2 topLeft = new IntVector2(40, 260);
            IntroMenu.DrawMenu(spriteBatch, ressource, "Play ", topLeft, getSelectedColor(0));
            IntroMenu.DrawMenu(spriteBatch, ressource, "My levels", new IntVector2(topLeft.X, topLeft.Y + 32), getSelectedColor(1));
            IntroMenu.DrawMenu(spriteBatch, ressource, "Toggle fullscren", new IntVector2(topLeft.X, topLeft.Y + 64), getSelectedColor(2));
            IntroMenu.DrawMenu(spriteBatch, ressource, "Reset save", new IntVector2(topLeft.X, topLeft.Y + 96), getSelectedColor(3));
            IntroMenu.DrawMenu(spriteBatch, ressource, "Exit", new IntVector2(topLeft.X, topLeft.Y + 128), getSelectedColor(4));

        }

        public void Update(InputState inputState, InputState previousInputState)
        {
            moveCursor(this.Game.Player1InputController, inputState, previousInputState);
            moveCursor(this.Game.Player2InputController, inputState, previousInputState);
            click(this.Game.Player1InputController, inputState, previousInputState);
            click(this.Game.Player2InputController, inputState, previousInputState);
        }

        private void moveCursor(InputController inputController, InputState inputState, InputState previousInputState)
        {
            if (inputController.HasDirection(previousInputState))
            {
                return;
            }
            IntVector2 move = inputController.GetMoveDirection(inputState);
            if (move == IntVector2.Zero)
            {
                return;
            }
            this.selectedMenu = Tools.InRange(this.selectedMenu + move.Y, 0, 4);
        }

        private void click(InputController inputController, InputState inputState, InputState previousInputState)
        {
            if (inputController.HasAnyActionButtonPressed(inputState) && !inputController.HasAnyActionButtonPressed(previousInputState))
            {
                if (this.selectedMenu == 0)///Play
                {
                    this.Game.IntroMenu = null;
                    this.Game.ShowLevelsMap(PrincessRiffRaffGame.GameModeEnum.Campaign);
                }
                else if (this.selectedMenu == 1)
                {
                    this.Game.IntroMenu = null;
                    this.Game.ShowLevelsMap(PrincessRiffRaffGame.GameModeEnum.MyLevel);
                }
                else if (this.selectedMenu == 2)
                {
                    this.Game.ToggleFullScreen();
                }
                else if (this.selectedMenu == 3)///Exit
                {
                    this.Game.ResetProgression();
                    this.selectedMenu = 0;
                }
                else if (this.selectedMenu == 4)///Exit
                {
                    this.Game.Exit();
                }
            }
        }
    }
}
