﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework;

namespace PrincessRiffRaff
{
    public class InterfaceLayer
    {
        private Level level;
      //  private TextureRect ShieldIcon = ItemTypeHelper.GetTextureRect(ItemType.ZONE_SHIELD);

        public InterfaceLayer(Level world)
        {
            this.level = world;
        }

        public void Draw(SpriteBatch spriteBatch, IntVector2 screenSize, Ressource ressource)
        {
            drawInGameUserInterface(spriteBatch, screenSize, ressource);
        }

        private IntVector2 drawInGameUserInterface(SpriteBatch spriteBatch, IntVector2 screenSize, Ressource ressource)
        {
            if (!level.player1.NeedToPressStartToPlay)
            {
                drawActionBar(spriteBatch, ressource, level.player1, 10);
                drawLifeBars(spriteBatch, ressource, level.player1, 10 + 124);
                //drawShields(spriteBatch, ressource, 10 + 130);
                drawHeroIcon(spriteBatch, ressource, level.player1, 10 + 124 + 158);

            }
            else
            {
                drawPressStart(spriteBatch, ressource, 10);
            }


            drawScore(spriteBatch, ressource, screenSize.X / 2 + 50);

            if (!level.player2.NeedToPressStartToPlay)
            {
                int left  = screenSize.X - 332;
                drawHeroIcon(spriteBatch, ressource, level.player2, left);
                drawLifeBars(spriteBatch, ressource, level.player2, left + 46);
               // drawShields(spriteBatch, ressource, screenSize.X - 310 + 50);
                drawActionBar(spriteBatch, ressource, level.player2, left + 46 + 158);
            }
            else
            {
                drawPressStart(spriteBatch, ressource, screenSize.X - 210);
            }

            if (this.level.BossZone != null && this.level.BossZone.Boss != null && !this.level.BossZone.Boss.IsDead)
            {
                drawBossLifeBars(spriteBatch, ressource, screenSize, this.level.BossZone.Boss);
            }
            return screenSize;
        }

        private void drawHeroIcon(SpriteBatch spriteBatch, Ressource ressource, Player player, int left)
        {

            Color color = player.GetLifeRedAlertIndicator();

            spriteBatch.Draw(ressource.TilesetInterface40,
                         new Rectangle(left, 10 + 40, 40, 40),
                         new Rectangle(0, 0, 40, 40), color);

            Texture2D spriteTexture = player.getTexture2D(ressource);
            spriteBatch.Draw(spriteTexture,
                new Rectangle(left + 4, 10 + 40 + 4, 32, 32),
                new Rectangle(0, 0, 64, 64), color);

        }

        private static readonly TextureRect[] HearthIcons = new TextureRect[]{
            TextureRect.NewItems32(1, 6),
            TextureRect.NewItems32(2, 6),
            TextureRect.NewItems32(3, 6),
            TextureRect.NewItems32(4, 6),
            TextureRect.NewItems32(5, 6)
        };
        private static readonly TextureRect DisabledHearthIcon = TextureRect.NewItems32(0, 6);

        private void drawLifeBars(SpriteBatch spriteBatch, Ressource ressource, Player player, int left)
        {

            int life = player.Life / Player.QuaterHearthLife;
            if (life == 0 && player.Life > 0)
            {
                life = 1;
            }
            int nbHearth = player.MaxLife / (4 * Player.QuaterHearthLife);

            for (int i = 0; i < 5; i++)
            {
                TextureRect icon;
                if (i >= nbHearth)
                {
                    icon = DisabledHearthIcon;
                }
                else
                {
                    int partLife = life - (4 * i);
                    if (partLife <= 0)
                    {
                        icon = HearthIcons[0];
                    }
                    else if (partLife >= HearthIcons.Length)
                    {
                        icon = HearthIcons[HearthIcons.Length - 1];
                    }
                    else
                    {
                        icon = HearthIcons[partLife];
                    }
                }
                spriteBatch.Draw(icon.GetTileset(ressource), new Rectangle(left + 30 * i, 10 + 44, 32, 32), icon.Rectangle, Color.White);
            }

            //int maxLifePx = 113;
            //if (!player.IsDead)
            //{
            //    int lifeLogPx = maxLifePx * player.Life / 100;
            //    spriteBatch.Draw(ressource.TilesetInterface40, new Rectangle(left + 4, 10 + 40, lifeLogPx, 40), new Rectangle(123, 40, 40, 40), Color.White);
            //}
            //spriteBatch.Draw(ressource.TilesetInterface40, new Rectangle(left, 10 + 40, 120, 40), new Rectangle(0, 41, 120, 40), Color.White);



        }

        private static void drawItemQuantity(SpriteBatch spriteBatch, Ressource ressource, IntVector2 topLeft, int quantity)
        {
            string txt = quantity.ToString();
            int offset = 0;
            if (txt.Length == 1)
            {
                offset = 15;
            }
            Color color = Theme.TextQuantityColor;
            if (quantity == 0)
            {
                color = Color.Orange;
            }
            spriteBatch.DrawString(ressource.FontTitle, txt, new Vector2(topLeft.X + 5 + offset + 1, topLeft.Y + 15 + 1), Color.Black);
            spriteBatch.DrawString(ressource.FontTitle, txt, new Vector2(topLeft.X + 5 + offset - 1, topLeft.Y + 15 - 1), Color.Black);
            spriteBatch.DrawString(ressource.FontTitle, txt, new Vector2(topLeft.X + 5 + offset, topLeft.Y + 15), color);
        }

        private static void drawWeaponIcon(SpriteBatch spriteBatch, Ressource ressource, IntVector2 topLeft, Weapon weapon, Color? backColor)
        {
            if (backColor == null)
            {
                spriteBatch.Draw(ressource.TilesetInterface40,
                    new Rectangle(topLeft.X, topLeft.Y, 40, 40),
                    new Rectangle(0, 0, 40, 40), Color.White);
            }
            else
            {
                spriteBatch.Draw(ressource.TilesetInterface40,
                    new Rectangle(topLeft.X, topLeft.Y, 40, 40),
                    new Rectangle(0, 82, 40, 40), backColor.Value);
            }
            if (weapon != null)
            {
                TextureRect textureRect = weapon.Icon;
                spriteBatch.Draw(textureRect.GetTileset(ressource),
                     new Rectangle(topLeft.X + 4, topLeft.Y + 4, 32, 32),
                   textureRect.Rectangle, Color.White);
                if (weapon.AmmoQuantity != -1)
                {
                    drawItemQuantity(spriteBatch, ressource, topLeft, weapon.AmmoQuantity);
                }
            }
        }

        private void drawActionBar(SpriteBatch spriteBatch, Ressource ressource, Player player, int left)
        {
            drawWeaponIcon(spriteBatch, ressource, new IntVector2(left, 10 + 40), player.Weapons[2], Color.Blue);
            drawWeaponIcon(spriteBatch, ressource, new IntVector2(left + 40, 10), player.Weapons[3], Color.Yellow);
            drawWeaponIcon(spriteBatch, ressource, new IntVector2(left + 40, 10 + 40 + 40), player.Weapons[0], Color.Green);
            drawWeaponIcon(spriteBatch, ressource, new IntVector2(left + 80, 10 + 40), player.Weapons[1], Color.Red);
        }

        //private void drawShields(SpriteBatch spriteBatch, Ressource ressource, int left)
        //{
        //    if (this.level.CurrentBuffHeart == 0)
        //    {
        //        return;
        //    }
        //    for (int i = 0; i < this.level.CurrentBuffHeart; i++)
        //    {
        //        spriteBatch.Draw(this.ShieldIcon.GetTileset(ressource),
        //            new Rectangle(left + 40 * i, 88, 40, 40),
        //            this.ShieldIcon.Rectangle, Color.White);
        //    }

        //}

        private void drawBossLifeBars(SpriteBatch spriteBatch, Ressource ressource, IntVector2 screenSize, Mob boss)
        {
            spriteBatch.Draw(ressource.TilesetInterface40,
                new Rectangle(40, screenSize.Y - 40, (screenSize.X - 80) * boss.Life / boss.MaxLife, 40),
                new Rectangle(134, 40, 20, 40),
                Color.White);
        }

        private void drawPressStart(SpriteBatch spriteBatch, Ressource ressource, int left)
        {
            spriteBatch.DrawString(ressource.Font, "Press any key to start", new Vector2(left, 10), Theme.TextColor);
        }

        private void drawScore(SpriteBatch spriteBatch, Ressource ressource, int right)
        {
            string text = this.level.Score.ToString();
            if (this.level.Score == 0)
            {
                text = "SCORE";
            }
            int left = right - text.Length * 18;
            var topLeft = new Vector2(left, 12);
            spriteBatch.DrawString(ressource.FontScore, text, topLeft + new Vector2(2, 2), Color.Black);
            spriteBatch.DrawString(ressource.FontScore, text, topLeft, Theme.ScoreColor);
            //spriteBatch.DrawString(ressource.Font, this.world.Score.ToString() , new Vector2(left, 50), Color.White, 0, new Vector2(0,0), 3, SpriteEffects.None, 0);			
        }


    }
}
