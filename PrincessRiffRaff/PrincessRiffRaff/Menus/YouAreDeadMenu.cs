﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace PrincessRiffRaff
{
	public class YouAreDeadMenu
	{
		private Level level;
        private int tick;
		
        public YouAreDeadMenu(Level level)
		{
			this.level = level;			
		}


		public void Draw(SpriteBatch spriteBatch, Ressource ressource)
		{
			IntVector2 topLeft = new IntVector2(260, 80);
			spriteBatch.Draw(ressource.TilesetInterface40, new Rectangle(topLeft.X, topLeft.Y, 300, 160), new Rectangle(164, 0, 40, 40), Color.White);
			topLeft = new IntVector2(340, 100);
			spriteBatch.DrawString(ressource.FontTitle, "You are dead", new Vector2(topLeft.X, topLeft.Y), Theme.TitleColor);
		}

		public void Update(InputState inputState, InputState previousInputState)
		{
            this.tick++;
            if (this.tick < 60 * 3)
            {
                return;
            }
            this.level.Game.YouAreDeadMenu = null;
            this.level.ResurectPlayers();
		}

	
	}
}
