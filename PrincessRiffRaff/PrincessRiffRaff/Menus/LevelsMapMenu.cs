﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace PrincessRiffRaff
{
    public class LevelsMapMenu
    {


        public class CellContent
        {
            public bool IsBackCell;
            public ComLevel comLevel;
            public TextureRect Image;
            public string Text;
        }


        private readonly List<ComLevel> availableComLevels;
        private PrincessRiffRaffGame game;
        private List<CellContent> grid = new List<CellContent>();
        private int selectedLevelIndex = 0;
        private int progression;
        private int CellsPerRow = 3;
        private int cellSize = 64;
      
        public LevelsMapMenu(PrincessRiffRaffGame game, List<ComLevel> availableComLevels, int progression)
        {
            this.game = game;
            this.availableComLevels = availableComLevels;
            this.progression = progression + 1;///+1 due to the back button
            int nbButtons = availableComLevels.Count + 1;
            this.cellSize = 4;
            this.CellsPerRow = 800 / (this.cellSize * 2);
            foreach (int size in new int[] { 8, 16, 24, 32, 48, 64 })
            {
                int newCellPerRow = Math.Max(1, 800 / (size * 2));
                if ((nbButtons / newCellPerRow) * size * 2 < 300)
                {
                    this.cellSize = size;
                    this.CellsPerRow = newCellPerRow;
                }
            }
            fillGrid();
            this.selectedLevelIndex = Math.Min(this.grid.Count - 1, this.progression);
        }
        private static TextureRect UnsolvedLevelImg = new TextureRect(TextureRect.SpriteFile.TilesetInterface40, new Rectangle(165, 41, 40, 40));
        private static TextureRect DisabledLevelImg = new TextureRect(TextureRect.SpriteFile.TilesetInterface40, new Rectangle(246, 41, 40, 40));
        
        private void fillGrid()
        {
            this.grid.Clear();

            CellContent backcell = new CellContent();
            backcell.IsBackCell = true;
            backcell.Image = new TextureRect(TextureRect.SpriteFile.TilesetInterface40, new Rectangle(288, 0, 40, 40));
            backcell.Text = "Back to main menu";
            this.grid.Add(backcell);

            for (int i = 0; i < this.availableComLevels.Count; i++)
            {
                ComLevel comLevel = this.availableComLevels[i];
                int levelIndex = i + 1;///+1 due to the backCell
                CellContent cell = new CellContent();
                cell.comLevel = comLevel;
                cell.Text = "" + comLevel.LevelAttributes.LevelName;
                if (levelIndex <= this.progression)
                {
                    if (comLevel.LevelAttributes.LevelIsNewWeapon != WeaponType.None && comLevel.LevelAttributes.LevelIsNewWeapon != WeaponType.Default)
                    {
                        Weapon weapon = WeaponTypeHelper.NewWeapon(null, comLevel.LevelAttributes.LevelIsNewWeapon, WeaponType.BasicGun);
                        if (levelIndex == this.progression)
                        {
                            cell.Image = TextureRect.NewItems32(13, 0);
                            cell.Text = "Click to open";
                        }
                        else
                        {
                            cell.Image = weapon.Icon;
                        }
                    }
                    else
                    {
                        Mob boss = BossTypeHelper.CreateBoss(null, comLevel.LevelAttributes.BossType);
                        if (boss != null && boss.Image != null)
                        {
                            if (levelIndex != progression)
                            {
                                cell.Image = boss.Image;
                            }
                            else
                            {
                                cell.Image = LevelsMapMenu.UnsolvedLevelImg;
                                cell.Text = "Level unlocked, click to start";
                            }
                        }
                    }
                }
                else
                {
                    cell.Image = LevelsMapMenu.DisabledLevelImg;
                }
                this.grid.Add(cell);

            }
        }

        public static void DrawRoundRectangle(SpriteBatch spriteBatch, Ressource ressource, Rectangle rect, Color color)
        {
            spriteBatch.Draw(ressource.TilesetInterface40, new Rectangle(rect.X, rect.Y, 15, 15), new Rectangle(41, 0, 15, 15), color);
            spriteBatch.Draw(ressource.TilesetInterface40, new Rectangle(rect.X + 15, rect.Y, rect.Width - 30, 15), new Rectangle(41 + 15, 0, 10, 15), color);
            spriteBatch.Draw(ressource.TilesetInterface40, new Rectangle(rect.Right - 15, rect.Y, 15, 15), new Rectangle(41 + 25, 0, 15, 15), color);
            spriteBatch.Draw(ressource.TilesetInterface40, new Rectangle(rect.Right - 15, rect.Y + 15, 15, rect.Height - 30), new Rectangle(41 + 25, 15, 15, 10), color);
            spriteBatch.Draw(ressource.TilesetInterface40, new Rectangle(rect.Right - 15, rect.Bottom - 15, 15, 15), new Rectangle(41 + 25, 25, 15, 15), color);
            spriteBatch.Draw(ressource.TilesetInterface40, new Rectangle(rect.X + 15, rect.Bottom - 15, rect.Width - 30, 15), new Rectangle(41 + 15, 25, 10, 15), color);
            spriteBatch.Draw(ressource.TilesetInterface40, new Rectangle(rect.X, rect.Bottom - 15, 15, 15), new Rectangle(41, 25, 15, 15), color);
            spriteBatch.Draw(ressource.TilesetInterface40, new Rectangle(rect.X, rect.Y + 15, 15, rect.Width - 30), new Rectangle(41, 15, 15, 10), color);
        }



        public void Draw(SpriteBatch spriteBatch, Ressource ressource)
        {
            IntVector2 topLeft = new IntVector2(300, 20);
            spriteBatch.DrawString(ressource.Font, "Choose your level", new Vector2(topLeft.X, topLeft.Y), Theme.TextColor);

            for (int levelIndex = 0; levelIndex < this.grid.Count; levelIndex++)
            {
                CellContent cell = this.grid[levelIndex];
                int x = levelIndex % CellsPerRow;
                int y = levelIndex / CellsPerRow;

                topLeft = new IntVector2(40 + cellSize * 2 * x, 60 + cellSize * 2 * y);

                spriteBatch.Draw(cell.Image.GetTileset(ressource), new Rectangle(topLeft.X, topLeft.Y, cellSize, cellSize), cell.Image.Rectangle, Color.White);
                if (levelIndex == this.selectedLevelIndex)
                {
                    DrawRoundRectangle(spriteBatch, ressource, new Rectangle(topLeft.X - 5, topLeft.Y - 5, cellSize + 10, cellSize + 10), Theme.SelectionColor);
                }

            }

            CellContent selectedCell = this.grid[this.selectedLevelIndex];
            string text = selectedCell.Text;
            topLeft = new IntVector2(50, 402);
            spriteBatch.DrawString(ressource.Font, text, new Vector2(topLeft.X, topLeft.Y), Theme.TextColor);
        }

        public void Update(InputState inputState, InputState previousInputState)
        {
            moveCursor(game.Player1InputController, inputState, previousInputState);
            moveCursor(game.Player2InputController, inputState, previousInputState);
            click(game.Player1InputController, inputState, previousInputState);
            click(game.Player2InputController, inputState, previousInputState);
        }

        private void moveCursor(InputController inputController, InputState inputState, InputState previousInputState)
        {
            if (inputController.HasDirection(previousInputState))
            {
                return;
            }
            IntVector2 move = inputController.GetMoveDirection(inputState);
            if (move == IntVector2.Zero)
            {
                return;
            }
            int newIndex = this.selectedLevelIndex;
            if (move.X > 0)
            {
                newIndex++;
            }
            else if (move.X < 0)
            {
                newIndex--;
            }
            else if (move.Y > 0)
            {
                newIndex += CellsPerRow;
            }
            else if (move.Y < 0)
            {
                newIndex -= CellsPerRow;
            }
            if (newIndex >= 0 && newIndex <= this.progression && newIndex < this.grid.Count)
            {
                this.selectedLevelIndex = newIndex;
            }
        }

        private void click(InputController inputController, InputState inputState, InputState previousInputState)
        {
            if (inputController.HasAnyActionButtonPressed(inputState) && !inputController.HasAnyActionButtonPressed(previousInputState))
            {
                CellContent cell = this.grid[this.selectedLevelIndex];
                if (cell.IsBackCell)
                {
                    this.game.IntroMenu = new IntroMenu(this.game);

                }
                else if (cell.comLevel.LevelAttributes.LevelIsNewWeapon == WeaponType.None || cell.comLevel.LevelAttributes.LevelIsNewWeapon == WeaponType.Default)
                {
                    game.Music.MenuClick.Play();
                    game.LevelsMapMenu = null;
                    game.StartLevel(cell.comLevel, this.selectedLevelIndex);
                }
                else
                {
                    if (this.selectedLevelIndex == this.progression)
                    {
                        game.Music.LevelWin.Play();
                        this.progression++;
                        this.selectedLevelIndex = Math.Min(this.selectedLevelIndex + 1, this.grid.Count - 1);
                        fillGrid();
                    }
                }
            }
        }
    }
}
