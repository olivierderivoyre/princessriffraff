﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using PrincessRiffRaff;
using System.IO;
using System.Xml.Serialization;
using System.Diagnostics;
using System.ComponentModel.Design;
using log4net;

namespace MapEditor
{
	public partial class MapEditorForm : Form
	{

        private static ILog logger = LogManager.GetLogger(typeof(MapEditorForm));
        private Dictionary<ItemType, Bitmap> items = new Dictionary<ItemType, Bitmap>();
		private DataTable table;
        private ComLevel comLevel;
        private  Bitmap tilesetGround64 ;
        private  Bitmap tilesetMob64 ;
        private  Bitmap tilesetPlayer1;
        private  Bitmap tilesetItems32;

     

        public MapEditorForm()
        {
            InitializeComponent();
           
            this.tilesetGround64 = (Bitmap)Image.FromFile(@".\Content\Tileset\TilesetGround64.png");
            this.tilesetMob64 = (Bitmap)Image.FromFile(@".\Content\Tileset\TilesetMob64.png");
            this.tilesetPlayer1 = (Bitmap)Image.FromFile(@".\Content\Sprite\harajuku4.png");
            this.tilesetItems32 = (Bitmap)Image.FromFile(@".\Content\Tileset\TilesetItems32.png");
          
            foreach (ItemType itemType in Enum.GetValues(typeof(ItemType)))
            {
                if (itemType == PrincessRiffRaff.ItemType.DELIMITER_CARPET_END ||
                    itemType == PrincessRiffRaff.ItemType.DELIMITER_FURNITURE_END ||
                    itemType == PrincessRiffRaff.ItemType.DELIMITER_MOB_END)
                {
                    this.toolStripItems.Items.Add(new ToolStripSeparator());
                }
                Bitmap img = this.getImage(itemType);
                if (img == null)
                {
                    continue;
                }
                this.items.Add(itemType, img);                
                ToolStripButton button = new ToolStripButton();
                button.Text = itemType.ToString();
                button.Image = this.items[itemType];
                button.DisplayStyle = ToolStripItemDisplayStyle.Image;
                ItemType currentItem = itemType;///Create specific delegate
                if (itemType > ItemType.DELIMITER_CARPET && itemType < ItemType.DELIMITER_CARPET_END)
                {                   
                    button.Click += delegate { this.itemCarpetButton_Click(currentItem); };                    
                }
                else
                {                    
                    button.Click += delegate { this.itemFurnitureButton_Click(currentItem); };                   
                }
                this.toolStripItems.Items.Add(button);
            }

            newMap();
        }


        #region draw map in grid

        private Bitmap getImage(ItemType itemType)
        {
             TextureRect textureRect = ItemTypeHelper.GetTextureRect(itemType);
             if (textureRect != null)
             {
                 Bitmap tile = null;
                 if (textureRect.spriteFile == TextureRect.SpriteFile.TilesetGround)
                 {
                     tile = tilesetGround64;
                 }
                 if (textureRect.spriteFile == TextureRect.SpriteFile.TilesetMob64)
                 {
                     tile = tilesetMob64;
                 }
                 if (textureRect.spriteFile == TextureRect.SpriteFile.Player1)
                 {
                     tile = tilesetPlayer1;
                 }
                 if (textureRect.spriteFile == TextureRect.SpriteFile.TilesetItems32)
                 {
                     tile = tilesetItems32;
                 }
                 if (tile != null)
                 {
                     Bitmap img = Crop(tile, new Rectangle(textureRect.Rectangle.X, textureRect.Rectangle.Y, textureRect.Rectangle.Width, textureRect.Rectangle.Height));
                     return img;
                 }
             }
             return null;
        }
        /// <summary>
		/// http://stackoverflow.com/questions/734930/how-to-crop-an-image-using-c
		/// </summary>
		private static Bitmap Crop(Image source, Rectangle cropRect)
		{
			Bitmap target = new Bitmap(cropRect.Width, cropRect.Height);
			using (Graphics g = Graphics.FromImage(target))
			{
				g.DrawImage(source, new Rectangle(0, 0, target.Width, target.Height),
								 cropRect,
								 GraphicsUnit.Pixel);
			}
			return target;
		}

		private void panel1_Paint(object sender, PaintEventArgs e)
		{
			e.Graphics.DrawImage(this.items.Values.First(), e.ClipRectangle);
		}

        private void gridView_CellPainting(object sender, DataGridViewCellPaintingEventArgs e)
        {
            if (e.RowIndex == -1 || e.ColumnIndex == -1)
            {
                return;
            }
            e.Handled = true;
            Tuple<ItemType?, ItemType?> ground = new Tuple<ItemType?, ItemType?>(null, null);
            DataRow row = this.table.Rows[e.RowIndex];
            if (!row.IsNull(e.ColumnIndex))
            {
                ground = (Tuple<ItemType?, ItemType?>)row[e.ColumnIndex];
            }
            e.Graphics.FillRectangle(new SolidBrush(Color.White), e.CellBounds);
            if (ground.Item1 != null)
            {
                if (this.items.ContainsKey(ground.Item1.Value))
                {
                    e.Graphics.DrawImage(this.items[ground.Item1.Value], e.CellBounds);
                }
                else
                {
                    e.Graphics.DrawString("?", this.Font, new SolidBrush(Color.Black), e.CellBounds);
                }
            }
            if (ground.Item2 != null)
            {
                if (this.items.ContainsKey(ground.Item2.Value))
                {
                    e.Graphics.DrawImage(this.items[ground.Item2.Value], e.CellBounds);
                }
                else
                {
                    e.Graphics.DrawString("?", this.Font, new SolidBrush(Color.Black), e.CellBounds);
                }
            }

            if ((e.State & DataGridViewElementStates.Selected) != 0)
            {
                e.Graphics.DrawRectangle(new Pen(new SolidBrush(Color.Blue)),
                    new Rectangle(e.CellBounds.X, e.CellBounds.Y, e.CellBounds.Width - 1, e.CellBounds.Height - 1));
            }
        }
        private static bool hasValue(object dataRowCellValue)
        {
            if (dataRowCellValue == DBNull.Value)
            {
                return false;
            }
            Tuple<ItemType?, ItemType?> ground = (Tuple<ItemType?, ItemType?>)dataRowCellValue;
            return ground.Item1 != null || ground.Item2 != null;
        }

        private void showMap()
        {
            this.propertyGrid.SelectedObject = this.comLevel.LevelAttributes;
            int width = this.comLevel.Carpet.Length;
            int height = this.comLevel.Carpet[0].Length;
            this.table = new DataTable();
            Enumerable.Range(0, width).ToList().ForEach(i => table.Columns.Add("c" + i, typeof(Tuple<ItemType?, ItemType?>)));
            Enumerable.Range(0, height).ToList().ForEach(i => table.Rows.Add(table.NewRow()));
            for (int j = 0; j < height; j++)
            {
                for (int i = 0; i < width; i++)
                {
                    this.table.Rows[j][i] = new Tuple<ItemType?, ItemType?>(
                        this.comLevel.Carpet[i][j],
                         this.comLevel.Furniture[i][j]);
                }
            }
            this.gridView.DataSource = this.table;
            foreach (DataGridViewColumn column in this.gridView.Columns)
            {
                column.Width = 24;
            }
        }
        #endregion draw map in grid


        private void newMap()
        {
            this.comLevel = new ComLevel();
            comLevel.Carpet = newArray(80, 20);
            comLevel.Furniture = newArray(80, 20);
            comLevel.LevelAttributes = new PrincessRiffRaff.LevelAttributes();
            this.showMap();
        }

        private void enlargeButton_Click(object sender, EventArgs e)
        {
            this.writeTableToItemMap();

            int width = this.comLevel.Carpet.Length;
            int height = this.comLevel.Carpet[0].Length;
            int added = 24;
            ItemType?[][] newCarpet = newArray(this.comLevel.Carpet.Length + 2 * added, this.comLevel.Carpet[0].Length + 2 * added);
            ItemType?[][] newFurniture = newArray(this.comLevel.Carpet.Length + 2 * added, this.comLevel.Carpet[0].Length + 2 * added);
           
            this.table = new DataTable();
            Enumerable.Range(0, width).ToList().ForEach(i => table.Columns.Add("c" + i, typeof(Tuple<ItemType?, ItemType?>)));
            Enumerable.Range(0, height).ToList().ForEach(i => table.Rows.Add(table.NewRow()));
            for (int j = 0; j < height; j++)
            {
                for (int i = 0; i < width; i++)
                {
                    newCarpet[i + added][j + added] = this.comLevel.Carpet[i][j];
                    newFurniture[i + added][j + added] = this.comLevel.Furniture[i][j];
                }
            }
            this.comLevel.Carpet = newCarpet;
            this.comLevel.Furniture = newFurniture;
            this.showMap();
        }
        


        private ItemType?[][] newArray(int width, int height)
        {
            ItemType?[][] t = new ItemType?[width][];
            for (int i = 0; i < width; i++)
            {
                t[i] = new ItemType?[height];
            }

            return t;
        }

		private void writeTableToItemMap()
		{
			int rowStart = 0;
			int rowEnd = this.table.Rows.Count - 1;
			int columnStart = 0;
			int columnEnd = this.table.Columns.Count - 1;
			for (; rowStart < table.Rows.Count; rowStart++)
			{
				DataRow row = this.table.Rows[rowStart];
				if (table.Columns.Cast<DataColumn>().Any(c => hasValue(row[c])))
				{
					break;
				}
			}
			for (; rowEnd >= 0; rowEnd--)
			{
				DataRow row = table.Rows[rowEnd];
				if (table.Columns.Cast<DataColumn>().Any(c => hasValue(row[c])))
				{
					break;
				}
			}
			for (; columnStart < this.table.Columns.Count; columnStart++)
			{
				DataColumn column = this.table.Columns[columnStart];
				if (table.Rows.Cast<DataRow>().Any(r => hasValue(r[column])))
				{
					break;
				}
			}
			for (; columnEnd >= 0; columnEnd--)
			{
				DataColumn column = table.Columns[columnEnd];
				if (table.Rows.Cast<DataRow>().Any(r => hasValue(r[column])))
				{
					break;
				}
			}
            int width = columnEnd + 1 - columnStart;
            int height = rowEnd + 1 - rowStart;
            if (width <= 0 || height <= 0)
            {
                throw new Exception("Empty map.");
            }
            this.comLevel.Carpet = newArray(width, height);
            this.comLevel.Furniture = newArray(width, height);
            for (int j = 0; j < height; j++)
			{
                for (int i = 0; i < width; i++)
				{
					object cellValue = this.table.Rows[rowStart + j][columnStart + i];
					if (cellValue != DBNull.Value)					
					{
                        Tuple<ItemType?, ItemType?> ground = (Tuple<ItemType?, ItemType?>)cellValue;
                        this.comLevel.Carpet[i][j] = ground.Item1;
                        this.comLevel.Furniture[i][j] = ground.Item2;										
					}
				}
			}
            this.showMap();
           
		}

		private void newButton_Click(object sender, EventArgs e)
		{
			newMap();			
		}

        private void itemCarpetButton_Click(ItemType? item)
        {
            foreach (DataGridViewCell cell in this.gridView.SelectedCells)
            {
                Tuple<ItemType?, ItemType?> ground = new Tuple<ItemType?, ItemType?>(null, null);
                DataRow row = this.table.Rows[cell.RowIndex];
                if (!row.IsNull(cell.ColumnIndex))
                {
                    ground = (Tuple<ItemType?, ItemType?>)row[cell.ColumnIndex];
                }
                row[cell.ColumnIndex] = new Tuple<ItemType?, ItemType?>(item, ground.Item2);

            }
        }
        private void itemFurnitureButton_Click(ItemType? item)
        {
            foreach (DataGridViewCell cell in this.gridView.SelectedCells)
            {
                Tuple<ItemType?, ItemType?> ground = new Tuple<ItemType?, ItemType?>(null, null);
                DataRow row = this.table.Rows[cell.RowIndex];
                if (!row.IsNull(cell.ColumnIndex))
                {
                    ground = (Tuple<ItemType?, ItemType?>)row[cell.ColumnIndex];
                }
                row[cell.ColumnIndex] = new Tuple<ItemType?, ItemType?>(ground.Item1, item);

            }
        }
        		
		private void cleanCarpetButton_Click(object sender, EventArgs e)
		{
            this.itemCarpetButton_Click(null);
		}
        private void cleanFurnitureButton_Click(object sender, EventArgs e)
        {
            this.itemFurnitureButton_Click(null);
        }
		private string filename;
        private XmlSerializer serializer = new XmlSerializer(typeof(ComLevel));
		private void save()
		{
            logger.Info("save " + this.filename);
            this.writeTableToItemMap();
			File.Delete(this.filename);
			using (var stream = File.OpenWrite(this.filename))
			{
				serializer.Serialize(stream, this.comLevel);
			}			
		}

		private void saveAs()
		{
            string dir = Tools.GetPlayerLevelDirectory();
            if (!Directory.Exists(dir))
            {
                Directory.CreateDirectory(dir);
            }
			SaveFileDialog saveFileDialog = new SaveFileDialog();
			saveFileDialog.Filter = ".xml|*.xml";
            saveFileDialog.InitialDirectory = dir;
			var r = saveFileDialog.ShowDialog();
			if (r != System.Windows.Forms.DialogResult.OK)
			{
				return;
			}
			this.filename = saveFileDialog.FileName;
			save();
            this.registerRecent();
		}

		
		private void SaveAsButton_Click(object sender, EventArgs e)
		{
            logger.Info("SaveAsButton_Click");
			saveAs();
		}

		private void saveButton_Click(object sender, EventArgs e)
		{
            logger.Info("saveButton_Click");
            saveOrSaveAs();
		}

        private void saveOrSaveAs()
        {
            if (string.IsNullOrEmpty(this.filename))
            {
                saveAs();
            }
            else
            {
                save();
            }
        }

		private void loadButton_Click(object sender, EventArgs e)
		{
            logger.Info("loadButton_Click");
            string dir = Tools.GetPlayerLevelDirectory();
            if (!Directory.Exists(dir))
            {
                Directory.CreateDirectory(dir);
            }
			OpenFileDialog fileDialog = new OpenFileDialog();
			fileDialog.Filter = ".xml|*.xml";
            fileDialog.InitialDirectory = dir;
			var r = fileDialog.ShowDialog();
			if (r != System.Windows.Forms.DialogResult.OK)
			{
				return;
			}           
            open(fileDialog.FileName);
		}

        private void open(string file)
        {
            logger.Info("open " + file);
            this.filename = file;

            using (var stream = File.OpenRead(this.filename))
            {
                this.comLevel = (ComLevel)serializer.Deserialize(stream);
            }
            if (comLevel.LevelAttributes == null)
            {
                comLevel.LevelAttributes = new LevelAttributes();
            }
            this.showMap();
            this.registerRecent();
        }

		private void gridView_CurrentCellChanged(object sender, EventArgs e)
		{			
			DataGridViewCell cell = this.gridView.CurrentCell;
			if (cell != null)
			{
				this.Text = cell.ColumnIndex + " x " + cell.RowIndex + ": " + cell.Value;
			}
			else
			{
				this.Text = "Map editor";
			}
		}



        private void runButton_Click(object sender, EventArgs e)
        {
            logger.Info("runButton_Click");
            this.saveOrSaveAs();
            string exe = Path.Combine(Path.GetDirectoryName(typeof(MapEditorForm).Assembly.Location), @".\PrincessRiffRaff.exe");
            string arg = "-level \"" + this.filename + "\"";
            if (!File.Exists(exe))
            {
                throw new Exception("Exe not found: " + exe);
            }
            Process process = Process.Start(exe, arg);
           
        }

        private void MapEditorForm_Load(object sender, EventArgs e)
        {
            logger.Info("MapEditorForm_Load");
            if (MapEditor.Properties.Settings.Default.RecentFiles != null)
            {
                foreach (string file in MapEditor.Properties.Settings.Default.RecentFiles)
                {
                    ToolStripMenuItem menu = new ToolStripMenuItem();
                    menu.Text = file;
                    menu.Tag = file;
                    menu.Click += recentMenu_Click;
                    this.loadRecentMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] { menu });
                }
            }
        }

        private void recentMenu_Click(object sender, EventArgs e)
        {
            logger.Info("recentMenu_Click");
            string file = (string)((ToolStripMenuItem)sender).Tag;
            this.open(file);
        }

        private void registerRecent()
        {
            if (MapEditor.Properties.Settings.Default.RecentFiles == null)
            {
                MapEditor.Properties.Settings.Default.RecentFiles = new System.Collections.Specialized.StringCollection();
            }
            MapEditor.Properties.Settings.Default.RecentFiles.Remove(this.filename);

            MapEditor.Properties.Settings.Default.RecentFiles.Insert(0, this.filename);

            while (MapEditor.Properties.Settings.Default.RecentFiles.Count > 5)
            {
                MapEditor.Properties.Settings.Default.RecentFiles.RemoveAt(MapEditor.Properties.Settings.Default.RecentFiles.Count - 1);
            }
            MapEditor.Properties.Settings.Default.Save();
        }

	}

		
}
