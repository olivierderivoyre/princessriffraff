﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using System.Diagnostics;
using log4net;

namespace MapEditor
{
	static class Program
	{
		private static ILog logger = LogManager.GetLogger(typeof(Program));
		
		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main()
		{
            try
            {
				log4net.Config.XmlConfigurator.Configure();
				logger.Info("");
				logger.Info("Start Editor");
				logger.Info("");

                Application.ThreadException += new System.Threading.ThreadExceptionEventHandler(Application_ThreadException);
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                var form = new MapEditorForm();
                // form.ClientSize = new System.Drawing.Size(1280, 720);
                Application.Run(form);

				logger.Info("");
				logger.Info("End Editor");
            }
            catch (Exception ex)
            {
				logger.Error(ex);
                MessageBox.Show(ex.ToString());
            }
		}

		static void Application_ThreadException(object sender, System.Threading.ThreadExceptionEventArgs e)
		{		
			logger.Error(e.Exception);
			MessageBox.Show(e.Exception.Message);
		}
	}
}
